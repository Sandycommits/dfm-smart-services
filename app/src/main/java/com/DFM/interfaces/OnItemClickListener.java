package com.DFM.interfaces;

/**
 * Created by Sandeep on 24/10/17.
 */

public interface OnItemClickListener {

    void onItemClick(int position);
}
