package com.DFM.interfaces;

import com.DFM.dto.AccountRecord;

/**
 * Created by Sandeep on 04/12/17.
 */

public interface OnAccountItemClickListener {

    void onAccountItemClick(AccountRecord record);
}
