package com.DFM.interfaces;

/**
 * Created by macadmin on 1/31/18.
 */

public interface LoadMoreListener {
    void onLoadMore();
}
