package com.DFM.interfaces;

import com.DFM.dto.BrokerStatement;

/**
 * Created by macadmin on 1/14/18.
 */

public interface OnGlobalItemClickListener {

    void onTradeItemClick(BrokerStatement statement);
}
