package com.DFM.interfaces;

import com.DFM.dto.Icon;

/**
 * Created by piuser on 8/28/2017.
 */

public interface OnGridItemTouchListener {
    void onGridItemTouch(Icon icon);
}
