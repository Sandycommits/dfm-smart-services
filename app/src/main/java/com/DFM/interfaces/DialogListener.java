package com.DFM.interfaces;

/**
 * Created by macadmin on 1/14/18.
 */

public interface DialogListener {

    void onConfirmation(boolean isConfirmed);
}
