package com.DFM.interfaces;

import com.DFM.dto.Broker;

/**
 * Created by Sandeep on 29/10/17.
 */

public interface OnBrokerSelectionListener {

    void onBrokerSelected(Broker broker);
}
