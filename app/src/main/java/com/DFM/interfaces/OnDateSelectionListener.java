package com.DFM.interfaces;

/**
 * Created by Sandeep on 19/11/17.
 */

public interface OnDateSelectionListener {

    void onDateSelected(String date);
}
