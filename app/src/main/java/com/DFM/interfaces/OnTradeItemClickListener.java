package com.DFM.interfaces;

import com.DFM.dto.TradeRecord;

/**
 * Created by Sandeep on 29/11/17.
 */

public interface OnTradeItemClickListener {
    void onTradeItemClick(TradeRecord record);
}
