package com.DFM.interfaces;

/**
 * Created by Sandeep on 16/11/17.
 */

public interface OnDialogItemClickListener {

    void onDialogItem(String selected);
}
