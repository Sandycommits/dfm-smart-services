package com.DFM;

import android.app.Application;
import android.content.Context;

/**
 * Created by macadmin on 2/8/18.
 */

public class MyApplication extends Application {

    private static Context mContext;

    public static Context getContext() {
        return mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }
}
