package com.DFM;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.DFM.fonts.HelveticaNeueButton;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.languageSwitch)
    HelveticaNeueButton languageSwitch;
    private Configuration config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.settings));

        config = getResources().getConfiguration();
        if (config.locale.getLanguage().equalsIgnoreCase("en")) {
            languageSwitch.setText(getString(R.string.arabic));
        } else {
            languageSwitch.setText(getString(R.string.english));
        }
    }

    @OnClick({R.id.languageSwitch})
    public void onViewClicks(View view) {
        String currentLanguage = languageSwitch.getText().toString();
        if (currentLanguage.equalsIgnoreCase(getString(R.string.english))) {
            languageSwitch.setText(getString(R.string.arabic));
            changeLanguage();
        } else {
            languageSwitch.setText(getString(R.string.english));
            changeLanguage();
        }
    }

    private void changeLanguage() {
        Locale current = config.locale;
        Locale locale = new Locale(current.getLanguage().equalsIgnoreCase("en") ? "ar" : "en");
        config.setLocale(locale);
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        startActivity(new Intent(this, SettingsActivity.class));
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
//            onBackPressed();
            startActivity(new Intent(this, DashboardActivity.class));
            this.finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, DashboardActivity.class));
        this.finish();
    }
}
