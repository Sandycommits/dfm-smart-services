package com.DFM;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.DFM.dto.Table;
import com.DFM.fonts.HelveticaNeueBoldTextView;
import com.DFM.fonts.HelveticaNeueEditText;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.PreferenceConnector;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nin)
    HelveticaNeueEditText nin;
    @BindView(R.id.email)
    HelveticaNeueEditText email;
    @BindView(R.id.mobile)
    HelveticaNeueEditText mobile;
    @BindView(R.id.address)
    HelveticaNeueEditText address;
    @BindView(R.id.city)
    HelveticaNeueEditText city;
    @BindView(R.id.country)
    HelveticaNeueEditText country;
    @BindView(R.id.postBox)
    HelveticaNeueEditText postBox;
    @BindView(R.id.name)
    HelveticaNeueBoldTextView name;
    @BindView(R.id.circleView)
    HelveticaNeueBoldTextView circleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.my_profile));
        Table table = (Table) CommonMethods.retrieveObject(this, PreferenceConnector.PROFILE, new Table());
        loadData(table);
    }

    @Override
    public void onViewClicks(View view) {

    }

    private void loadData(Table table) {
        if (table != null) {
            email.setText(table.getEmail());
            nin.setText(table.getNIN());
            mobile.setText(table.getPHONE2());
            address.setText(table.getADDRESS_1());
            city.setText(table.getCITY());
            country.setText(table.getCOUNTRY());
            postBox.setText(table.getPOSTAL());
            String userName = table.getENNAME();
            name.setText(userName);
            String[] literals = userName.split(" ");
            if (literals != null && literals.length >= 2)
                circleView.setText(literals[0].substring(0, 1) + literals[1].substring(0, 1));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
