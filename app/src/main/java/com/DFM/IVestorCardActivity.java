package com.DFM;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.DFM.dto.Attachment;
import com.DFM.dto.Body;
import com.DFM.dto.CaseData;
import com.DFM.dto.CaseInfoResponse;
import com.DFM.dto.ComboBox;
import com.DFM.dto.EpayResponse;
import com.DFM.dto.FormInfoResponse;
import com.DFM.dto.Header;
import com.DFM.dto.IVestorCardRequest;
import com.DFM.dto.IVestorCardResponse;
import com.DFM.dto.Login;
import com.DFM.dto.NIN;
import com.DFM.dto.PayementInfo;
import com.DFM.dto.UserCode;
import com.DFM.fonts.HelveticaNeueBoldTextView;
import com.DFM.fonts.HelveticaNeueEditText;
import com.DFM.interfaces.OnDialogItemClickListener;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.Constants;
import com.DFM.utils.PreferenceConnector;
import com.Wsdl2Code.WebServices.MiscFunctions.IWsdl2CodeEvents;
import com.Wsdl2Code.WebServices.MiscFunctions.MiscFunctions;
import com.Wsdl2Code.WebServices.ePay5.ePay5;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.mapper.MapperWrapper;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IVestorCardActivity extends BaseActivity {

    private static final int EMIRATES_ID = 1;
    private static final int PASSPORT = 2;
    private static final int PERMISSION_CODE = 1;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.selectNin)
    HelveticaNeueEditText selectNin;
    @BindView(R.id.selectService)
    HelveticaNeueEditText selectService;
    @BindView(R.id.caption)
    HelveticaNeueBoldTextView caption;
    @BindView(R.id.emiratesID)
    ImageView emiratesID;
    @BindView(R.id.passport)
    ImageView passport;
    @BindView(R.id.idClose)
    ImageView idClose;
    @BindView(R.id.passportClose)
    ImageView passportClose;
    @BindView(R.id.submit)
    Button submit;
    private UserCode userCode;
    private String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    private List<Attachment> attachments;
    private int userID;
    private String[] services;
    private Body formData;
    private List<ComboBox> options;
    private Login login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ivestor_card);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.iVESTOR_card_services));

        login = (Login) CommonMethods.retrieveObject(this, PreferenceConnector.LOGIN, new Login());
        userCode = (UserCode) CommonMethods.retrieveObject(this, PreferenceConnector.USER_CODES, new UserCode());
        userID = Integer.parseInt(login.getData().getUserID());
        attachments = new ArrayList<>();
//        services = getResources().getStringArray(R.array.service_array);
        options = new ArrayList<>();
        loadComboService();
//        loadData();
    }

    private void loadComboService() {
        if (CommonMethods.isOnline(this)) {
            MiscFunctions miscFunctions = new MiscFunctions(new IWsdl2CodeEvents() {
                @Override
                public void Wsdl2CodeStartedRequest() {

                }

                @Override
                public void Wsdl2CodeFinished(String methodName, Object data) {
                    dismissProgressDialog();
                    String response = data.toString();
                    DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                    try {
                        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
                        Document doc = docBuilder.parse(new InputSource(new StringReader(response)));
                        NodeList nodeList = doc.getElementsByTagName("Data").item(0).getChildNodes();
                        int length = nodeList.getLength();
                        for (int i = 0; i < length; i = i + 2) {
                            Node optionNode = nodeList.item(i).getFirstChild();
                            Node valueNode = nodeList.item(i + 1).getFirstChild();
                            ComboBox comboBox = new ComboBox();
                            if (optionNode != null) {
                                comboBox.setOption(optionNode.getNodeValue());
                            } else {
                                comboBox.setOption("");
                            }
                            if (valueNode != null) {
                                comboBox.setValue(valueNode.getNodeValue());
                            } else {
                                comboBox.setValue("");
                            }
                            options.add(comboBox);
                            loadServices();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    loadData();
                }

                @Override
                public void Wsdl2CodeFinishedWithException(Exception ex) {
                    dismissProgressDialog();
                }

                @Override
                public void Wsdl2CodeEndedRequest() {

                }
            });
            showProgressDialog();
            try {
                miscFunctions.ComboListAsync("ivestorcardservice", language);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }
    }

    private void loadServices() {
        services = new String[options.size()];
        for (int i = 0; i < options.size(); i++) {
            ComboBox comboBox = options.get(i);
            services[i] = comboBox.getOption();
        }
    }

    @Override
    @OnClick({R.id.selectNin, R.id.selectService, R.id.emiratesID, R.id.passport, R.id.submit, R.id.idClose, R.id.passportClose})
    public void onViewClicks(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.selectNin:
                String[] ninStrings = CommonMethods.getNinArray(userCode.getNinList());
                int selectedPosition = CommonMethods.getStringPosition(ninStrings, selectNin.getText().toString());
                CommonMethods.showListDialog(this, ninStrings, new OnDialogItemClickListener() {
                    @Override
                    public void onDialogItem(String selected) {
                        selectNin.setText(selected);
                    }
                }, selectedPosition);
                break;
            case R.id.selectService:
                int servPosition = CommonMethods.getStringPosition(services, selectService.getText().toString());
                CommonMethods.showListDialog(this, services, new OnDialogItemClickListener() {
                    @Override
                    public void onDialogItem(String selected) {
                        selectService.setText(selected);
                        caption.setText(getCaption(selected));
                    }
                }, servPosition);
                break;
            case R.id.emiratesID:
                if (CommonMethods.hasPermissions(this, permissions)) {
                    EasyImage.openChooserWithGallery(this, "Select option", EMIRATES_ID);
                } else {
                    ActivityCompat.requestPermissions(this, permissions, EMIRATES_ID);
                }
                break;
            case R.id.passport:
                if (CommonMethods.hasPermissions(this, permissions)) {
                    EasyImage.openChooserWithGallery(this, "Select option", PASSPORT);
                } else {
                    ActivityCompat.requestPermissions(this, permissions, PASSPORT);
                }
                break;
            case R.id.submit:
                if (formData != null) {
                    updateIvestorCardForm();
                } else {
                    submitIvestorCardForm();
                }
                break;
            case R.id.idClose:
                idClose.setVisibility(View.GONE);
                emiratesID.setImageResource(R.drawable.ic_clip);
                removeAttachment(0);
                break;
            case R.id.passportClose:
                passportClose.setVisibility(View.GONE);
                passport.setImageResource(R.drawable.ic_clip);
                removeAttachment(1);
                break;
            default:
                break;
        }
    }

    private void updateIvestorCardForm() {

        IVestorCardRequest request = new IVestorCardRequest();
        Header header = new Header();
        header.setUserID(userID);
        header.setRequestID((int) Math.random());
        header.setSessionID("");
        header.setHasAttachments(true);
        header.setRefNo(formData.getRefNo());
        header.setFormOperation(2);

        CaseData caseData = new CaseData();
        caseData.setCaseID(formData.getCrmTrackingNumber());
        caseData.setUserID(userID + "");
        caseData.setCaseSubject("");
        request.setAttachments(attachments);
        request.setBody(formData);
        request.setCaseData(caseData);
        request.setHeader(header);
        if (CommonMethods.isOnline(this)) {
            showProgressDialog();
            apiService.iVestorCardForm(request).enqueue(new Callback<IVestorCardResponse>() {
                @Override
                public void onResponse(Call<IVestorCardResponse> call, Response<IVestorCardResponse> response) {
                    dismissProgressDialog();
                    IVestorCardResponse cardResponse = response.body();
                    FormInfoResponse formInfo = cardResponse.getFormInfoResponse();
                    PayementInfo payementInfo = formInfo.getPayementInfo();
                    if (payementInfo != null && payementInfo.getPaymentAmount() > 0) {

                    } else {
                        CaseInfoResponse caseInfo = cardResponse.getCaseInfoResponse();
                        Intent intent = new Intent(IVestorCardActivity.this, TrackRequestActivity.class);
                        startActivity(intent);
                        intent.putExtra("reference", caseInfo.getTrackingNumber());
                        IVestorCardActivity.this.finish();
                    }
                }

                @Override
                public void onFailure(Call<IVestorCardResponse> call, Throwable t) {
                    dismissProgressDialog();
                }
            });
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }
    }

    private String getCaption(String selected) {
        for (ComboBox comboBox : options) {
            if (selected.equalsIgnoreCase(comboBox.getOption())) {
                return comboBox.getValue();
            }
        }
        return "";
    }

    private void submitIvestorCardForm() {

        IVestorCardRequest request = new IVestorCardRequest();
        NIN root = CommonMethods.getRootNin(userCode);
        Header header = new Header();
        header.setUserID(userID);
        header.setRequestID((int) Math.random());
        header.setSessionID("");
        header.setHasAttachments(true);
        header.setRefNo("");
        header.setFormOperation(1);
        Body body = new Body();
        body.setRefNo("");
        body.setUserID(userID);
        body.setCrmTrackingNumber("");
        body.setReqID((int) Math.random());
        body.setAppDate(CommonMethods.getCurrentDateOnly());
        String selected = selectNin.getText().toString();
        if (selected.contains("-")) {
            String[] investor = selected.split("-");
            body.setInvestorName(investor[1]);
            body.setInvestorNumber(investor[0]);
        } else {
            body.setInvestorName(root.getAccountName());
            body.setInvestorNumber(root.getNIN());
        }
        body.setServiceType(getServiceType(selectService.getText().toString()));
        body.setComments("");
        CaseData caseData = new CaseData();
        caseData.setCaseID("");
        caseData.setUserID(userID + "");
        caseData.setCaseSubject("");
        request.setAttachments(attachments);
        request.setBody(body);
        request.setCaseData(caseData);
        request.setHeader(header);
        if (CommonMethods.isOnline(this)) {
            showProgressDialog();
            apiService.iVestorCardForm(request).enqueue(new Callback<IVestorCardResponse>() {
                @Override
                public void onResponse(Call<IVestorCardResponse> call, Response<IVestorCardResponse> response) {
                    dismissProgressDialog();
                    IVestorCardResponse cardResponse = response.body();
                    FormInfoResponse formInfo = cardResponse.getFormInfoResponse();
                    PayementInfo payementInfo = formInfo.getPayementInfo();
                    if (payementInfo != null && payementInfo.getPaymentAmount() > 0) {
                        String text = "CUSTID=" + userID + "&UserName=" + login.getUserName() + "&USERID=" + userID + "&ServiceCode=MOB&PaymentAmount=" + payementInfo.getPaymentAmount() + "&isCaseCreation=true&RefNo=" + formInfo.getReferenceNumber() + "&TRName=iVESTOR Card Request Form&SavedFormUrl=" + formInfo.getUrl() + "?CUSTID=" + userID + "&Lang=en&OtpRequired=false";
                        ePay5 ePay5 = new ePay5(new com.Wsdl2Code.WebServices.ePay5.IWsdl2CodeEvents() {
                            @Override
                            public void Wsdl2CodeStartedRequest() {

                            }

                            @Override
                            public void Wsdl2CodeFinished(String methodName, Object data) {
                                dismissProgressDialog();
                                XStream stream = new XStream(new DomDriver()) {
                                    // to enable ignoring of unknown elements
                                    @Override
                                    protected MapperWrapper wrapMapper(MapperWrapper next) {
                                        return new MapperWrapper(next) {
                                            @Override
                                            public boolean shouldSerializeMember(Class definedIn, String fieldName) {
                                                if (definedIn == Object.class) {
                                                    try {
                                                        return this.realClass(fieldName) != null;
                                                    } catch (Exception e) {
                                                        return false;
                                                    }
                                                }
                                                return super.shouldSerializeMember(definedIn, fieldName);
                                            }
                                        };
                                    }
                                };
                                stream.processAnnotations(EpayResponse.class);
                                EpayResponse epayResponse = (EpayResponse) stream.fromXML(data.toString());
                                if (login.getStatus().equalsIgnoreCase("valid")) {
                                    String encoded = Constants.EPAY_URL + epayResponse.getData();
                                /*Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(encoded));
                                startActivity(intent);*/

                                    Intent intent = new Intent(IVestorCardActivity.this, PaymentActivity.class);
                                    intent.putExtra("payment", encoded);
                                    startActivity(intent);
                                    IVestorCardActivity.this.finish();
                                } else {
                                    CommonMethods.displayOKDialogue(IVestorCardActivity.this, getString(R.string.something_wrong));
                                }
                            }

                            @Override
                            public void Wsdl2CodeFinishedWithException(Exception ex) {
                                dismissProgressDialog();
                            }

                            @Override
                            public void Wsdl2CodeEndedRequest() {

                            }
                        });
                        try {
                            ePay5.ePay5EncryptionNewAsync(login.getData().getSession(), text);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        CaseInfoResponse caseInfo = cardResponse.getCaseInfoResponse();
                        Intent intent = new Intent(IVestorCardActivity.this, TrackRequestActivity.class);
                        intent.putExtra("reference", caseInfo.getTrackingNumber());
                        startActivity(intent);
                        IVestorCardActivity.this.finish();
                    }
                }

                @Override
                public void onFailure(Call<IVestorCardResponse> call, Throwable t) {
                    dismissProgressDialog();
                }
            });
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == PERMISSION_CODE) {
                EasyImage.openChooserWithGallery(this, "Select option", EMIRATES_ID);
            } else if (requestCode == PASSPORT) {
                EasyImage.openChooserWithGallery(this, "Select option", PASSPORT);
            }
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new EasyImage.Callbacks() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource imageSource, int i) {
                CommonMethods.displayOKDialogue(IVestorCardActivity.this, getString(R.string.something_wrong));
            }

            @Override
            public void onImagePicked(File file, EasyImage.ImageSource imageSource, int i) {
                String filePath = file.getPath();
                Bitmap originalBitmap = BitmapFactory.decodeFile(filePath);

                Bitmap bitmap = CommonMethods.getRotatedBitMap(originalBitmap, filePath);


                Attachment attachment = new Attachment();
                if (i == EMIRATES_ID) {
                    removeAttachment(0);
                    idClose.setVisibility(View.VISIBLE);
                    emiratesID.setImageBitmap(bitmap);
                    attachment.setDocumentName("Emirates ID");
                    attachment.setPosition(0);
                } else if (i == PASSPORT) {
                    removeAttachment(1);
                    passportClose.setVisibility(View.VISIBLE);
                    attachment.setDocumentName("Passport");
                    passport.setImageBitmap(bitmap);
                    attachment.setPosition(1);
                }
                attachment.setFileBytes(CommonMethods.bitmapToBase64(bitmap));
                attachment.setFileName(file.getName());
                attachment.setFileType("jpeg");
                attachments.add(attachment);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource imageSource, int i) {

            }
        });
    }

    private void loadData() {

        Body body = (Body) getIntent().getSerializableExtra("isEdit");
        if (body != null) {
            IVestorCardRequest request = new IVestorCardRequest();

            Header header = new Header();
            header.setUserID(userID);
            header.setRefNo(body.getRefNo());
            header.setFormOperation(4);
            Body innerBody = new Body();
            innerBody.setRefNo(body.getRefNo());
            innerBody.setUserID(userID);
            innerBody.setCrmTrackingNumber(body.getCrmTrackingNumber());
            innerBody.setAppDate(CommonMethods.getIVestorDateFormat(body.getAppDate()));
            CaseData caseData = new CaseData();
            caseData.setCaseID(body.getCrmTrackingNumber());
            caseData.setUserID(userID + "");
            request.setBody(innerBody);
            request.setCaseData(caseData);
            request.setHeader(header);
            if (CommonMethods.isOnline(this)) {
                showProgressDialog();
                apiService.iVestorCardForm(request).enqueue(new Callback<IVestorCardResponse>() {
                    @Override
                    public void onResponse(Call<IVestorCardResponse> call, Response<IVestorCardResponse> response) {
                        dismissProgressDialog();
                        IVestorCardResponse iVestorCardResponse = response.body();
                        FormInfoResponse formResponse = iVestorCardResponse.getFormInfoResponse();
                        formData = formResponse.getFormData();
                        selectNin.setText(formData.getInvestorNumber() + "-" + formData.getInvestorName());
                        int serviceType = formData.getServiceType();
                        if (serviceType > 0) {
                            String type = services[serviceType - 1];
                            selectService.setText(type);
                            caption.setText(getCaption(type));
                        }
                    }

                    @Override
                    public void onFailure(Call<IVestorCardResponse> call, Throwable t) {
                        dismissProgressDialog();
                    }
                });
            } else {
                CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
            }
            selectNin.setClickable(false);
            selectService.setClickable(false);
            selectNin.setBackgroundResource(R.drawable.disabled_edittext);
            selectService.setBackgroundResource(R.drawable.disabled_edittext);
        } else {
            userCode = (UserCode) CommonMethods.retrieveObject(this, PreferenceConnector.USER_CODES, new UserCode());
            NIN root = CommonMethods.getRootNin(userCode);
            selectNin.setText(root.getNIN() + "-" + root.getAccountName());
            if (options != null && options.size() > 0) {
                ComboBox comboBox = options.get(0);
                selectService.setText(comboBox.getOption());
                caption.setText(comboBox.getValue());
            }
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public int getServiceType(String selected) {
        for (int i = 0; i < options.size(); i++) {
            ComboBox comboBox = options.get(i);
            if (selected.equalsIgnoreCase(comboBox.getOption())) {
                return i + 1;
            }
        }
        return 0;
    }

    private void removeAttachment(int position) {

        for (int i = 0; i < attachments.size(); i++) {
            Attachment attachment = attachments.get(i);
            if (position == attachment.getPosition())
                attachments.remove(attachment);
        }
    }
}
