package com.DFM.expandable;

import com.DFM.dto.Dividend;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by Sandeep on 23/11/17.
 */

public class DividendHeader extends ExpandableGroup {

    DecimalFormat formatter;
    private List<Dividend> items;

    public DividendHeader(String title, List<Dividend> items) {
        super(title, items);
        this.items = items;

        NumberFormat nFormatter = NumberFormat.getNumberInstance(Locale.US);
        formatter = (DecimalFormat) nFormatter;
        formatter.applyPattern("#,###,###.##");
        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);
    }

    public String getTotalDividend() {
        double count = 0;
        String currency = items.get(0).getCURRENCY();
        for (Dividend dividend : items) {
            double amount = Double.parseDouble(dividend.getCASHDIVIDEND());
            count = count + amount;
        }
        return currency + " " + formatter.format(count);
    }
}
