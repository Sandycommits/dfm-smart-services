package com.DFM.expandable;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.DFM.R;
import com.DFM.dto.TradeRecord;
import com.DFM.interfaces.OnTradeItemClickListener;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by Sandeep on 28/11/17.
 */

public class TradeAdapter extends ExpandableRecyclerViewAdapter<TradeHeaderViewHolder, TradeChildViewHolder> {

    private LayoutInflater inflater;
    private OnTradeItemClickListener listener;


    public TradeAdapter(List<? extends ExpandableGroup> groups, Context context) {
        super(groups);
        this.inflater = LayoutInflater.from(context);
        listener = (OnTradeItemClickListener) context;
    }

    @Override
    public TradeHeaderViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.trade_header_item, parent, false);
        return new TradeHeaderViewHolder(view);
    }

    @Override
    public TradeChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.trade_child_item, parent, false);
        return new TradeChildViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(TradeChildViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        final TradeRecord record = (TradeRecord) group.getItems().get(childIndex);
        holder.onBind(record);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onTradeItemClick(record);
            }
        });
    }

    @Override
    public void onBindGroupViewHolder(final TradeHeaderViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.onBind(group, flatPosition);
        if (flatPosition == 0)
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    holder.itemView.performClick();
                }
            }, 200);
    }
}
