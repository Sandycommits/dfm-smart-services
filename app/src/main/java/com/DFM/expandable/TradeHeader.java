package com.DFM.expandable;

import com.DFM.dto.TradeRecord;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by Sandeep on 28/11/17.
 */

public class TradeHeader extends ExpandableGroup {

    DecimalFormat formatter;
    private List<TradeRecord> items;
    private TradeRecord record;

    public TradeHeader(TradeRecord record, List<TradeRecord> items) {
        super(record.getSymbol(), items);
        this.items = items;
        this.record = record;

        NumberFormat nFormatter = NumberFormat.getNumberInstance(Locale.US);
        formatter = (DecimalFormat) nFormatter;
        formatter.applyPattern("#,###,###.##");
        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);
    }

    public String getTotalBuyValue() {
        return formatter.format(Double.parseDouble(record.getTotalBuyValue()));
    }

    public String getTotalSellValue() {
        return formatter.format(Double.parseDouble(record.getTotalSellValue()));
    }

    public String getTotalAmount() {
        return formatter.format(record.getTotalAmount());
    }

/*    public String getTotalDividend() {
        double count = 0;
        String currency = items.get(0).getCURRENCY();
        for (Dividend dividend : items) {
            double amount = Double.parseDouble(dividend.getCASHDIVIDEND());
            count = count + amount;
        }
        return currency + " " + formatter.format(count);
    }*/
}
