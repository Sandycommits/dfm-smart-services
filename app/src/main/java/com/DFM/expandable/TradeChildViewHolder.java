package com.DFM.expandable;

import android.view.View;

import com.DFM.R;
import com.DFM.dto.TradeRecord;
import com.DFM.fonts.HelveticaNeueTextView;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Sandeep on 28/11/17.
 */

public class TradeChildViewHolder extends ChildViewHolder {

    HelveticaNeueTextView broker, totalAmount;
    DecimalFormat formatter;

    public TradeChildViewHolder(View itemView) {
        super(itemView);
        broker = itemView.findViewById(R.id.broker);
        totalAmount = itemView.findViewById(R.id.totalAmount);

        NumberFormat nFormatter = NumberFormat.getNumberInstance(Locale.US);
        formatter = (DecimalFormat) nFormatter;
        formatter.applyPattern("#,###,###.##");
        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);
    }

    public void onBind(TradeRecord record) {
        broker.setText(record.getBrokerCode());
        totalAmount.setText(formatter.format(record.getTotalAmount()));
    }
}
