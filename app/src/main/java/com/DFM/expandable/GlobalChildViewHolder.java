package com.DFM.expandable;

import android.view.View;

import com.DFM.R;
import com.DFM.dto.BrokerStatement;
import com.DFM.fonts.HelveticaNeueTextView;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by macadmin on 1/14/18.
 */

public class GlobalChildViewHolder extends ChildViewHolder {

    HelveticaNeueTextView childName, childAccount, childShares, childAvailable;
    DecimalFormat formatter;
    DecimalFormat valueFormatter;

    public GlobalChildViewHolder(View itemView) {
        super(itemView);
        childName = itemView.findViewById(R.id.childName);
        childAccount = itemView.findViewById(R.id.childAccountNumber);
        childAvailable = itemView.findViewById(R.id.childAvailable);
        childShares = itemView.findViewById(R.id.childShares);

        NumberFormat nFormatter = NumberFormat.getNumberInstance(Locale.US);
        formatter = (DecimalFormat) nFormatter;
        formatter.applyPattern("#,###,###");

        NumberFormat vFormatter = NumberFormat.getNumberInstance(Locale.US);
        valueFormatter = (DecimalFormat) vFormatter;
        valueFormatter.applyPattern("#,###,###.##");
        valueFormatter.setMaximumFractionDigits(2);
        valueFormatter.setMinimumFractionDigits(2);
    }

    public void onBind(BrokerStatement statement) {
        childName.setText(statement.getBrokerCode());
        childAccount.setText(statement.getAccount() + "");
        double price = statement.getPrice();
        long currentBalance = statement.getCurrentBalance();
        childShares.setText(valueFormatter.format(price * currentBalance));
        childAvailable.setText(formatter.format(statement.getAvailable()));
    }
}
