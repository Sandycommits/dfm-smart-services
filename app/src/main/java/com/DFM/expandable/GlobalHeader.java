package com.DFM.expandable;

import com.DFM.dto.BrokerStatement;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by macadmin on 1/14/18.
 */

public class GlobalHeader extends ExpandableGroup {

    DecimalFormat formatter;
    DecimalFormat valueFormatter;
    private List<BrokerStatement> items;

    public GlobalHeader(String title, List<BrokerStatement> items) {
        super(title, items);
        this.items = items;

        NumberFormat nFormatter = NumberFormat.getNumberInstance(Locale.US);
        formatter = (DecimalFormat) nFormatter;
        formatter.applyPattern("#,###,###");


        NumberFormat vFormatter = NumberFormat.getNumberInstance(Locale.US);
        valueFormatter = (DecimalFormat) vFormatter;
        valueFormatter.applyPattern("#,###,###.##");
        valueFormatter.setMaximumFractionDigits(2);
        valueFormatter.setMinimumFractionDigits(2);
    }

    public String getTotalValue() {
        double totalValue = 0;
        for (BrokerStatement statement : items) {
            totalValue += statement.getCurrentBalance() * statement.getPrice();
        }
        return valueFormatter.format(totalValue);
    }

    public String getTotalShares() {
        double totalShares = 0;
        for (BrokerStatement statement : items) {
            totalShares += statement.getAvailable();
        }
        return formatter.format(totalShares);
    }
}
