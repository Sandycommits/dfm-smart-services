package com.DFM.expandable;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.DFM.R;
import com.DFM.dto.BrokerStatement;
import com.DFM.interfaces.OnGlobalItemClickListener;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by macadmin on 1/14/18.
 */

public class GlobalAdapter extends ExpandableRecyclerViewAdapter<GlobalHeaderViewHolder, GlobalChildViewHolder> {

    private LayoutInflater inflater;
    private OnGlobalItemClickListener listener;

    public GlobalAdapter(List<? extends ExpandableGroup> groups, Context context) {
        super(groups);
        this.inflater = LayoutInflater.from(context);
        listener = (OnGlobalItemClickListener) context;
    }

    @Override
    public GlobalHeaderViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.global_header_item, parent, false);
        return new GlobalHeaderViewHolder(view);
    }

    @Override
    public GlobalChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.global_child_item, parent, false);
        return new GlobalChildViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(GlobalChildViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        final BrokerStatement statement = (BrokerStatement) group.getItems().get(childIndex);
        holder.onBind(statement);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onTradeItemClick(statement);
            }
        });
    }

    @Override
    public void onBindGroupViewHolder(final GlobalHeaderViewHolder holder, final int flatPosition, ExpandableGroup group) {
        holder.onBind(group, flatPosition);
        if (flatPosition == 0)
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    holder.itemView.performClick();
                }
            }, 200);
    }
}
