package com.DFM.expandable;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.DFM.R;
import com.DFM.dto.AccountRecord;
import com.DFM.interfaces.OnAccountItemClickListener;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by Sandeep on 04/12/17.
 */

public class AccountAdapter extends ExpandableRecyclerViewAdapter<AccountHeaderViewHolder, AccountChildViewHolder> {

    private LayoutInflater inflater;
    private OnAccountItemClickListener listener;


    public AccountAdapter(List<? extends ExpandableGroup> groups, Context context) {
        super(groups);
        this.inflater = LayoutInflater.from(context);
        listener = (OnAccountItemClickListener) context;
    }

    @Override
    public AccountHeaderViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.account_header_item, parent, false);
        return new AccountHeaderViewHolder(view);
    }

    @Override
    public AccountChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.account_child_item, parent, false);
        return new AccountChildViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(AccountChildViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        final AccountRecord record = (AccountRecord) group.getItems().get(childIndex);
        holder.onBind(record);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onAccountItemClick(record);
            }
        });
    }

    @Override
    public void onBindGroupViewHolder(final AccountHeaderViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.onBind(group, flatPosition);
        if (flatPosition == 0)
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    holder.itemView.performClick();
                }
            }, 200);
    }
}
