package com.DFM.expandable;

import com.DFM.dto.AccountRecord;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by Sandeep on 04/12/17.
 */

public class AccountHeader extends ExpandableGroup {

    private List<AccountRecord> items;
    private AccountRecord record;

    public AccountHeader(AccountRecord record, List<AccountRecord> items) {
        super(record.getSymbol(), items);
        this.items = items;
        this.record = record;
    }

    public String getTotalIn() {
        return record.getInVolume();
    }

    public String getTotalOut() {
        return record.getOutVolume();
    }

    public double getTotalBalance() {
        return record.getTotalBalance();
    }
}
