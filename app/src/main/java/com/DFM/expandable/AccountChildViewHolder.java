package com.DFM.expandable;

import android.view.View;

import com.DFM.R;
import com.DFM.dto.AccountRecord;
import com.DFM.fonts.HelveticaNeueTextView;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Sandeep on 04/12/17.
 */

public class AccountChildViewHolder extends ChildViewHolder {

    HelveticaNeueTextView broker, inVolume, outVolume, totalBalance;
    DecimalFormat formatter;

    public AccountChildViewHolder(View itemView) {
        super(itemView);
        broker = itemView.findViewById(R.id.broker);
        inVolume = itemView.findViewById(R.id.inValue);
        outVolume = itemView.findViewById(R.id.outValue);
        totalBalance = itemView.findViewById(R.id.totalBalance);

        NumberFormat nFormatter = NumberFormat.getNumberInstance(Locale.US);
        formatter = (DecimalFormat) nFormatter;
        formatter.applyPattern("#,###,###.##");
    }

    public void onBind(AccountRecord record) {
        broker.setText(record.getBrokerCode());
        if (record.getInVolume().isEmpty()) {
            inVolume.setText("0");
        } else {
            inVolume.setText(formatter.format(Double.parseDouble(record.getInVolume())));
        }
        if (record.getOutVolume().isEmpty()) {
            outVolume.setText("0");
        } else {
            outVolume.setText(formatter.format(Double.parseDouble(record.getOutVolume())));
        }
        totalBalance.setText(formatter.format(record.getTotalBalance()));
    }

}
