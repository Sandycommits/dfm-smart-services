package com.DFM.expandable;

import android.animation.ObjectAnimator;
import android.view.View;
import android.widget.ImageView;

import com.DFM.R;
import com.DFM.fonts.HelveticaNeueBoldTextView;
import com.DFM.fonts.HelveticaNeueTextView;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

/**
 * Created by Sandeep on 23/11/17.
 */

public class HeaderViewHolder extends GroupViewHolder {

    private HelveticaNeueBoldTextView title;
    private HelveticaNeueTextView headerValue;
    private ImageView expandIcon;
    private int rotationAngle = 0;

    public HeaderViewHolder(View itemView) {
        super(itemView);
        title = itemView.findViewById(R.id.headerName);
        headerValue = itemView.findViewById(R.id.headerValue);
        expandIcon = itemView.findViewById(R.id.expandIcon);
    }

    public void onBind(ExpandableGroup group, int position) {
        title.setText(group.getTitle());
        DividendHeader dividendHeader = (DividendHeader) group;
        headerValue.setText(dividendHeader.getTotalDividend());
    }

    @Override
    public void expand() {
        rotationAngle = 0;
        ObjectAnimator anim = ObjectAnimator.ofFloat(expandIcon, "rotation", rotationAngle, rotationAngle + 180);
        anim.setDuration(500);
        anim.start();
    }

    @Override
    public void collapse() {
        rotationAngle = 180;
        ObjectAnimator anim = ObjectAnimator.ofFloat(expandIcon, "rotation", rotationAngle, rotationAngle + 180);
        anim.setDuration(500);
        anim.start();
    }
}
