package com.DFM.expandable;

import android.animation.ObjectAnimator;
import android.view.View;
import android.widget.ImageView;

import com.DFM.R;
import com.DFM.fonts.HelveticaNeueBoldTextView;
import com.DFM.fonts.HelveticaNeueTextView;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

/**
 * Created by Sandeep on 28/11/17.
 */

public class TradeHeaderViewHolder extends GroupViewHolder {

    private HelveticaNeueBoldTextView company;
    private HelveticaNeueTextView buyValue, sellValue, totalAmount;
    private ImageView expandIcon;
    private int rotationAngle = 0;

    public TradeHeaderViewHolder(View itemView) {
        super(itemView);
        company = itemView.findViewById(R.id.company);
        buyValue = itemView.findViewById(R.id.buyValue);
        sellValue = itemView.findViewById(R.id.sellValue);
        totalAmount = itemView.findViewById(R.id.totalAmount);
        expandIcon = itemView.findViewById(R.id.expandIcon);
    }

    public void onBind(ExpandableGroup group, int position) {
        company.setText(group.getTitle());
        TradeHeader tradeHeader = (TradeHeader) group;
        buyValue.setText(tradeHeader.getTotalBuyValue());
        sellValue.setText(tradeHeader.getTotalSellValue());
        totalAmount.setText(tradeHeader.getTotalAmount());
    }

    @Override
    public void expand() {
        rotationAngle = 0;
        ObjectAnimator anim = ObjectAnimator.ofFloat(expandIcon, "rotation", rotationAngle, rotationAngle + 180);
        anim.setDuration(500);
        anim.start();
    }

    @Override
    public void collapse() {
        rotationAngle = 180;
        ObjectAnimator anim = ObjectAnimator.ofFloat(expandIcon, "rotation", rotationAngle, rotationAngle + 180);
        anim.setDuration(500);
        anim.start();
    }
}
