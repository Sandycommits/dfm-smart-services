package com.DFM.expandable;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.DFM.R;
import com.DFM.dto.Dividend;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by Sandeep on 23/11/17.
 */

public class ExpandableAdapter extends ExpandableRecyclerViewAdapter<HeaderViewHolder, DividendViewHolder> {


    private LayoutInflater inflater;
    private Context context;

    public ExpandableAdapter(List<? extends ExpandableGroup> groups, Context context) {
        super(groups);
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public HeaderViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.expand_header_item, parent, false);
        return new HeaderViewHolder(view);
    }

    @Override
    public DividendViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.expand_child_item, parent, false);
        return new DividendViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(DividendViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        Dividend dividend = (Dividend) group.getItems().get(childIndex);
        holder.onBind(dividend);
    }

    @Override
    public void onBindGroupViewHolder(final HeaderViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.onBind(group, flatPosition);
        if (flatPosition == 0)
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    holder.itemView.performClick();
                }
            }, 200);
    }
}
