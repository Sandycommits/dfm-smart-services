package com.DFM.expandable;

import android.view.View;

import com.DFM.R;
import com.DFM.dto.Dividend;
import com.DFM.fonts.HelveticaNeueTextView;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Sandeep on 23/11/17.
 */

public class DividendViewHolder extends ChildViewHolder {

    HelveticaNeueTextView notes, dividend, shares, broker;
    DecimalFormat formatter;
    DecimalFormat sharesFormatter;

    public DividendViewHolder(View itemView) {
        super(itemView);
        notes = itemView.findViewById(R.id.notes);
        dividend = itemView.findViewById(R.id.dividend);
        shares = itemView.findViewById(R.id.shares);
        broker = itemView.findViewById(R.id.broker);

        NumberFormat nFormatter = NumberFormat.getNumberInstance(Locale.US);
        formatter = (DecimalFormat) nFormatter;
        formatter.applyPattern("#,###,###.##");
        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);

        NumberFormat sFormatter = NumberFormat.getNumberInstance(Locale.US);
        sharesFormatter = (DecimalFormat) sFormatter;
        sharesFormatter.applyPattern("#,###,###");
    }

    public void onBind(Dividend dividend) {
        notes.setText(dividend.getNOTES());
        double dividendValue = Double.parseDouble(dividend.getCASHDIVIDEND());
        long totalShares = Long.parseLong(dividend.getBASEDVOLUME());
        this.dividend.setText(formatter.format(dividendValue));
        shares.setText(sharesFormatter.format(totalShares));
        broker.setText(dividend.getBROKERNAME_EN());
    }
}
