package com.DFM.expandable;

import android.animation.ObjectAnimator;
import android.view.View;
import android.widget.ImageView;

import com.DFM.R;
import com.DFM.fonts.HelveticaNeueBoldTextView;
import com.DFM.fonts.HelveticaNeueTextView;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Sandeep on 04/12/17.
 */

public class AccountHeaderViewHolder extends GroupViewHolder {

    DecimalFormat formatter;
    private HelveticaNeueBoldTextView company;
    private HelveticaNeueTextView inVolume, outVolume, totalBalance;
    private ImageView expandIcon;
    private int rotationAngle = 0;

    public AccountHeaderViewHolder(View itemView) {
        super(itemView);
        company = itemView.findViewById(R.id.company);
        inVolume = itemView.findViewById(R.id.inValue);
        outVolume = itemView.findViewById(R.id.outValue);
        totalBalance = itemView.findViewById(R.id.totalBalance);
        expandIcon = itemView.findViewById(R.id.expandIcon);


        NumberFormat nFormatter = NumberFormat.getNumberInstance(Locale.US);
        formatter = (DecimalFormat) nFormatter;
        formatter.applyPattern("#,###,###.##");
    }

    public void onBind(ExpandableGroup group, int position) {
        company.setText(group.getTitle());
        AccountHeader accountHeader = (AccountHeader) group;
        if (accountHeader.getTotalIn().isEmpty()) {
            inVolume.setText("0");
        } else {
            inVolume.setText(formatter.format(Double.parseDouble(accountHeader.getTotalIn())));
        }
        if (accountHeader.getTotalOut().isEmpty()) {
            outVolume.setText("0");
        } else {
            outVolume.setText(formatter.format(Double.parseDouble(accountHeader.getTotalOut())));
        }
        totalBalance.setText(formatter.format(accountHeader.getTotalBalance()));
    }

    @Override
    public void expand() {
        rotationAngle = 0;
        ObjectAnimator anim = ObjectAnimator.ofFloat(expandIcon, "rotation", rotationAngle, rotationAngle + 180);
        anim.setDuration(500);
        anim.start();
    }

    @Override
    public void collapse() {
        rotationAngle = 180;
        ObjectAnimator anim = ObjectAnimator.ofFloat(expandIcon, "rotation", rotationAngle, rotationAngle + 180);
        anim.setDuration(500);
        anim.start();
    }
}
