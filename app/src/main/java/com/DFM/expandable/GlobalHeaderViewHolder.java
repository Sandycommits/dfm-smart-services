package com.DFM.expandable;

import android.animation.ObjectAnimator;
import android.view.View;
import android.widget.ImageView;

import com.DFM.R;
import com.DFM.fonts.HelveticaNeueBoldTextView;
import com.DFM.fonts.HelveticaNeueTextView;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by macadmin on 1/14/18.
 */

public class GlobalHeaderViewHolder extends GroupViewHolder {

    DecimalFormat formatter;
    private HelveticaNeueBoldTextView headerName;
    private HelveticaNeueTextView headerShares, headerValue;
    private ImageView expandIcon;
    private int rotationAngle = 0;

    public GlobalHeaderViewHolder(View itemView) {
        super(itemView);
        headerName = itemView.findViewById(R.id.headerName);
        headerShares = itemView.findViewById(R.id.headerShares);
        headerValue = itemView.findViewById(R.id.headerValue);
        expandIcon = itemView.findViewById(R.id.expandIcon);
        NumberFormat nFormatter = NumberFormat.getNumberInstance(Locale.US);
        formatter = (DecimalFormat) nFormatter;
        formatter.applyPattern("#,###,###.##");
        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);
    }

    public void onBind(ExpandableGroup group, int position) {
        headerName.setText(group.getTitle());
        GlobalHeader globalHeader = (GlobalHeader) group;
        headerShares.setText(globalHeader.getTotalShares());
        headerValue.setText(globalHeader.getTotalValue());
    }

    @Override
    public void expand() {
        rotationAngle = 0;
        ObjectAnimator anim = ObjectAnimator.ofFloat(expandIcon, "rotation", rotationAngle, rotationAngle + 180);
        anim.setDuration(500);
        anim.start();
    }

    @Override
    public void collapse() {
        rotationAngle = 180;
        ObjectAnimator anim = ObjectAnimator.ofFloat(expandIcon, "rotation", rotationAngle, rotationAngle + 180);
        anim.setDuration(500);
        anim.start();
    }
}
