package com.DFM.retrofit;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Sandeep on 5/25/2017.
 */

public class APIClient {

    //    UAT
//    public static final String BASE_URL = "http://esrvuat.dfm.ae:8010/eServicesWebAPI/Api/";
    //    Production
    public static final String BASE_URL = "https://service.dfm.ae/esrvWebAPI/Api/";
    private static Retrofit retrofit = null;

    private APIClient() {

    }

    public static Retrofit getClientInstance() {

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;
    }
}
