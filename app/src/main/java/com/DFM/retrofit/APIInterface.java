package com.DFM.retrofit;

import com.DFM.dto.Disclosures;
import com.DFM.dto.Fee;
import com.DFM.dto.IVestorCardRequest;
import com.DFM.dto.IVestorCardResponse;
import com.DFM.dto.RegistrationResponse;
import com.DFM.dto.TrackRequestResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Sandeep on 26/06/17.
 */

public interface APIInterface {

    /**
     * Login request
     *
     * @param body
     * @return
     */
    @Headers("Content-Type: application/json")
    @POST("Registration")
    Call<RegistrationResponse> registration(@Body HashMap<String, Object> body);

    @GET("sso/source")
    Call<Disclosures> getDisclosures(@Query("p_url") String p_url, @Query("lang") String language, @Query("announcement_type") String announcement, @Query("take") String take, @Query("h7_datetime_format") String dateFormat, @Query("type") String type, @Query("cms_resources") String resources, @Query("skip") String skip);

    @GET("sso/source?p_url=directfn2c8475_efsah&lang=ar&announcement_type=Disclosure&take=50&h7_datetime_format=MMM%20dd,%20yyyy%20hh:mm%20tt&type=json&cms_resources=true")
    Call<Disclosures> getDisclosuresAR();

    @Headers("Content-Type: application/json")
    @POST("RequestIVestorCardService")
    Call<IVestorCardResponse> iVestorCardForm(@Body IVestorCardRequest request);

    @Headers("Content-Type: application/json")
    @POST("UserCases")
    Call<TrackRequestResponse> trackRequest(@Body HashMap<String, Object> body);

    @Headers("Content-Type: application/json")
    @POST("RequestCashDividends")
    Call<IVestorCardResponse> requestCashDividends(@Body IVestorCardRequest request);

    @Headers("Content-Type: application/json")
    @POST("FormsFees")
    Call<Fee> getFormsFees(@Body HashMap<String, Object> body);

    @Headers("Content-Type: application/json")
    @POST("RequestFamilyTransferNew")
    Call<IVestorCardResponse> requestFamilyTransfer(@Body IVestorCardRequest request);

}
