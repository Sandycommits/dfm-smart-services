package com.DFM;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.DFM.adapters.SlideAdapter;
import com.DFM.interfaces.OnItemClickListener;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.Constants;
import com.yarolegovich.discretescrollview.DiscreteScrollView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GuidesActivity extends BaseActivity implements OnItemClickListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.slider)
    DiscreteScrollView slider;
    private String[] menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guides);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.guides));
        loadSlides();
    }

    @Override
    public void onViewClicks(View view) {

    }

    private void loadSlides() {
        menu = getResources().getStringArray(R.array.pdf_documents);
        SlideAdapter adapter = new SlideAdapter(this, menu, language);
        slider.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onItemClick(int position) {
        if (CommonMethods.isOnline(this)) {
            Uri uri;
            switch (position) {
                case 0:
                    uri = language.equalsIgnoreCase("en") ? Uri.parse(Constants.pdf_1) : Uri.parse(Constants.pdf_1a);
                    break;
                case 1:
                    uri = language.equalsIgnoreCase("en") ? Uri.parse(Constants.pdf_2) : Uri.parse(Constants.pdf_2a);
                    break;
                case 2:
                    uri = language.equalsIgnoreCase("en") ? Uri.parse(Constants.pdf_3) : Uri.parse(Constants.pdf_3a);
                    break;
                case 3:
                    uri = language.equalsIgnoreCase("en") ? Uri.parse(Constants.pdf_4) : Uri.parse(Constants.pdf_4a);
                    break;
                case 4:
                    uri = language.equalsIgnoreCase("en") ? Uri.parse(Constants.pdf_5) : Uri.parse(Constants.pdf_5a);
                    break;
                case 5:
                    uri = language.equalsIgnoreCase("en") ? Uri.parse(Constants.pdf_8) : Uri.parse(Constants.pdf_8a);
                    break;
                case 6:
                    uri = language.equalsIgnoreCase("en") ? Uri.parse(Constants.pdf_9) : Uri.parse(Constants.pdf_9a);
                    break;
                case 7:
                    uri = language.equalsIgnoreCase("en") ? Uri.parse(Constants.pdf_6) : Uri.parse(Constants.pdf_6a);
                    break;
                case 8:
                    uri = language.equalsIgnoreCase("en") ? Uri.parse(Constants.pdf_10) : Uri.parse(Constants.pdf_10a);
                    break;
                case 9:
                    uri = language.equalsIgnoreCase("en") ? Uri.parse(Constants.pdf_7) : Uri.parse(Constants.pdf_7a);
                    break;
                case 10:
                    uri = Uri.parse(Constants.pdf_11);
                    break;
                default:
                    uri = Uri.parse("");
                    break;
            }

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(browserIntent);
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }
    }
}
