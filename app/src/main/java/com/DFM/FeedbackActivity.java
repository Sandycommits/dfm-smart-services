package com.DFM;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.DFM.dto.Table;
import com.DFM.fonts.HelveticaNeueButton;
import com.DFM.fonts.HelveticaNeueEditText;
import com.DFM.interfaces.OnDialogItemClickListener;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.PreferenceConnector;
import com.Wsdl2Code.WebServices.Enquiry.Enquiry;
import com.Wsdl2Code.WebServices.Enquiry.IWsdl2CodeEvents;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FeedbackActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.subject)
    HelveticaNeueEditText subject;
    @BindView(R.id.submit)
    HelveticaNeueButton submit;
    @BindView(R.id.email)
    HelveticaNeueEditText email;
    @BindView(R.id.mobile)
    HelveticaNeueEditText mobile;
    @BindView(R.id.message)
    HelveticaNeueEditText message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.feedback));
        loadData();
    }

    private void loadData() {
        Table table = (Table) CommonMethods.retrieveObject(this, PreferenceConnector.PROFILE, new Table());
        email.setText(table.getEmail());
        mobile.setText(table.getPHONE2());
    }

    private ArrayList<String> createArrays() {
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < 20; i++) {
            list.add("item" + i);
        }
        return list;
    }

    @Override
    @OnClick({R.id.subject, R.id.submit})
    public void onViewClicks(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.subject:
                String[] subjects = getResources().getStringArray(R.array.subjects);
                int selectedPosition = CommonMethods.getStringPosition(subjects, subject.getText().toString());
                CommonMethods.showListDialog(this, subjects, new OnDialogItemClickListener() {
                    @Override
                    public void onDialogItem(String selected) {
                        subject.setText(selected);
                    }
                }, selectedPosition);
                break;
            case R.id.submit:
                if (CommonMethods.isOnline(this)) {
                    sendFeedback();
                } else {
                    CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
                }
                break;
            default:
                break;
        }
    }

    private void sendFeedback() {

        boolean isValid = CommonMethods.validateEditTextFields(new EditText[]{subject, email, mobile, message});
        if (isValid) {
            String emailString = email.getText().toString();
            String subjectString = subject.getText().toString();
            String phoneString = mobile.getText().toString();
            String messageString = message.getText().toString();
            if (Patterns.EMAIL_ADDRESS.matcher(emailString).matches()) {
                Enquiry enquiry = new Enquiry(new IWsdl2CodeEvents() {
                    @Override
                    public void Wsdl2CodeStartedRequest() {

                    }

                    @Override
                    public void Wsdl2CodeFinished(String methodName, Object Data) {
                        dismissProgressDialog();
                        CommonMethods.displayOKDialogue(FeedbackActivity.this, getString(R.string.your_online_enquiry));
                    }

                    @Override
                    public void Wsdl2CodeFinishedWithException(Exception ex) {
                        dismissProgressDialog();
                    }

                    @Override
                    public void Wsdl2CodeEndedRequest() {

                    }
                });
                showProgressDialog();
                try {
                    enquiry.FeedbackAsync(subjectString, emailString, messageString, phoneString);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                CommonMethods.displayOKDialogue(this, getString(R.string.email_validation));
            }
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.all_fields));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
