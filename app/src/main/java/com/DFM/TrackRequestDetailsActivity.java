package com.DFM;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.DFM.dto.Body;
import com.DFM.dto.Case;
import com.DFM.fonts.HelveticaNeueButton;
import com.DFM.fonts.HelveticaNeueEditText;
import com.DFM.utils.CommonMethods;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TrackRequestDetailsActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.caseSubject)
    HelveticaNeueEditText caseSubject;
    @BindView(R.id.caseStatus)
    HelveticaNeueEditText caseStatus;
    @BindView(R.id.statusReason)
    HelveticaNeueEditText statusReason;
    @BindView(R.id.caseRemarks)
    HelveticaNeueEditText caseRemarks;
    @BindView(R.id.caseRequestDate)
    HelveticaNeueEditText caseRequestDate;
    @BindView(R.id.expectedResolutionDate)
    HelveticaNeueEditText expectedResolutionDate;
    @BindView(R.id.trackingNumber)
    HelveticaNeueEditText trackingNumber;
    @BindView(R.id.edit)
    HelveticaNeueButton edit;

    private Case userCase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_request_details);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.details));

        loadData();
    }

    private void loadData() {

        userCase = (Case) getIntent().getSerializableExtra("track_data");
        caseSubject.setText(userCase.getRequestCaseSubject());
        String status = userCase.getRequestStatus();
        if (status.equalsIgnoreCase(getString(R.string.incomplete))) {
            edit.setVisibility(View.VISIBLE);
        }
        caseStatus.setText(userCase.getRequestStatus());
        statusReason.setText(userCase.getRequestCaseReason());
        caseRemarks.setText(userCase.getRequestCaseRemarks());
        caseRequestDate.setText(userCase.getRequestDate());
        expectedResolutionDate.setText(CommonMethods.getResolutionDateFormat(userCase.getRequestExpectedDate().split(" ")[0]));
        trackingNumber.setText(userCase.getTrackingNumber());
    }

    @Override
    @OnClick(R.id.edit)
    public void onViewClicks(View view) {

        Body body = new Body();
        body.setRefNo(userCase.getReferenceNumber());
        body.setCrmTrackingNumber(userCase.getTrackingNumber());
        if (userCase.getRequestCaseSubject().equalsIgnoreCase(getString(R.string.iVESTOR_form))) {
            Intent intent = new Intent(this, IVestorCardActivity.class);
            body.setAppDate(userCase.getRequestDate().split(" ")[0]);
            intent.putExtra("isEdit", body);
            startActivity(intent);
        } else if (userCase.getRequestCaseSubject().equalsIgnoreCase(getString(R.string.dividend_form))) {
            Intent intent = new Intent(this, CashDividendsActivity.class);
            body.setApplicationDate(userCase.getRequestDate().split(" ")[0]);
            intent.putExtra("isEdit", body);
            startActivity(intent);
        } else if (userCase.getRequestCaseSubject().equalsIgnoreCase(getString(R.string.transfers_form))) {
            Intent intent = new Intent(this, FamilyTransfersActivity.class);
            body.setApplicationDate(userCase.getRequestDate().split(" ")[0]);
            intent.putExtra("isEdit", body);
            startActivity(intent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
