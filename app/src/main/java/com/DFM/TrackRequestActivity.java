package com.DFM;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;

import com.DFM.adapters.TrackRequestAdapter;
import com.DFM.dto.Case;
import com.DFM.dto.Login;
import com.DFM.dto.TrackRequestResponse;
import com.DFM.fonts.HelveticaNeueEditText;
import com.DFM.interfaces.OnItemClickListener;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.PreferenceConnector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrackRequestActivity extends BaseActivity implements OnItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.search)
    HelveticaNeueEditText search;
    @BindView(R.id.trackRecycler)
    RecyclerView trackRecycler;

    private List<Case> cases;
    private TrackRequestAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_request);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.track_request));

        getCases();
        String reference = getIntent().getStringExtra("reference");
        if (reference != null) {
            CommonMethods.displayOKDialogue(this, getString(R.string.your_request) + " " + reference);
        }
    }

    private void getCases() {

        Login login = (Login) CommonMethods.retrieveObject(this, PreferenceConnector.LOGIN, new Login());
        HashMap<String, Object> map = new HashMap<>();
        map.put("UserID", login.getData().getUserID());
        map.put("PageIndex", 1);
        map.put("PageSize", 99);
        map.put("Session", "");
        showProgressDialog();
        apiService.trackRequest(map).enqueue(new Callback<TrackRequestResponse>() {
            @Override
            public void onResponse(Call<TrackRequestResponse> call, Response<TrackRequestResponse> response) {
                dismissProgressDialog();
                cases = response.body().getCases();
                loadData();
            }

            @Override
            public void onFailure(Call<TrackRequestResponse> call, Throwable t) {
                dismissProgressDialog();
            }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filter(editable.toString());
            }
        });
    }

    private void filter(String text) {
        List<Case> temp = new ArrayList();
        String filteredString;
        for (Case userCase : cases) {
            filteredString = userCase.getRequestName() + userCase.getTrackingNumber();
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (filteredString.toLowerCase().contains(text.toLowerCase())) {
                temp.add(userCase);
            }
        }
        //update recyclerview
        adapter.updateList(temp);
    }

    private void loadData() {
        adapter = new TrackRequestAdapter(this, cases, language);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        trackRecycler.setHasFixedSize(true);
        trackRecycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        trackRecycler.setLayoutManager(layoutManager);
        trackRecycler.setAdapter(adapter);
    }

    @Override
    public void onViewClicks(View view) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(this, TrackRequestDetailsActivity.class);
        intent.putExtra("track_data", adapter.getItem(position));
        startActivity(intent);
    }
}
