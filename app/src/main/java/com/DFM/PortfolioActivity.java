package com.DFM;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.DFM.adapters.PortfolioAdapter;
import com.DFM.dto.BrokerStatement;
import com.DFM.dto.GlobalAccount;
import com.DFM.dto.Login;
import com.DFM.dto.NIN;
import com.DFM.dto.UserCode;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.PreferenceConnector;
import com.Wsdl2Code.WebServices.Enquiry.Enquiry;
import com.Wsdl2Code.WebServices.Enquiry.IWsdl2CodeEvents;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PortfolioActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.portfolioRecycler)
    RecyclerView recyclerView;
    //    @BindView(R.id.playAll)
//    HelveticaNeueButton playAll;
    DecimalFormat formatter;
    private ArrayList<BrokerStatement> companies = new ArrayList<>();
    private PortfolioAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_portfolio);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.portfolio));

        NumberFormat nFormatter = NumberFormat.getNumberInstance(Locale.US);
        formatter = (DecimalFormat) nFormatter;
        formatter.applyPattern("#,###,###.##");
        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);

        callService();
    }

    @Override
    public void onViewClicks(View view) {

    }

    /*@Override
    @OnClick({R.id.playAll})
    public void onViewClicks(View view) {

        if (playAll.getText().toString().equalsIgnoreCase(getString(R.string.play_all))) {
            playAll.setText(getString(R.string.stop_all));
            adapter.playAll();
        } else {
            playAll.setText(getString(R.string.play_all));
            stopSpeaker();
        }
    }*/

    private void stopSpeaker() {
        if (adapter != null) {
            if (adapter.tts.isSpeaking())
                adapter.tts.stop();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void callService() {
        Enquiry enquiry = new Enquiry(new IWsdl2CodeEvents() {
            @Override
            public void Wsdl2CodeStartedRequest() {

            }

            @Override
            public void Wsdl2CodeFinished(String methodName, Object data) {
                dismissProgressDialog();
//                XStream xStream = new XStream(new DomDriver());
                xStream.processAnnotations(Login.class);
                Login login = (Login) xStream.fromXML(data.toString());
                if (login.getStatus().equalsIgnoreCase("valid")) {
                    GlobalAccount globalAccount = login.getData().getGlobalAccount();
                    companies = globalAccount.getBrokers();
                    if (companies != null) {
                        loadRecycler();
                    } else {
                        loadRecycler();
                    }
                }
            }

            @Override
            public void Wsdl2CodeFinishedWithException(Exception ex) {
                dismissProgressDialog();
            }

            @Override
            public void Wsdl2CodeEndedRequest() {

            }
        });
        showProgressDialog();
        Login login = (Login) CommonMethods.retrieveObject(this, PreferenceConnector.LOGIN, new Login());

        UserCode userCode = (UserCode) CommonMethods.retrieveObject(this, PreferenceConnector.USER_CODES, new UserCode());
        NIN root = CommonMethods.getRootNin(userCode);
        String nin = root.getNIN();
        String date = CommonMethods.getCurrentDateOnly();
        try {
            enquiry.GlobalAccountBalanceAsync(login.getData().getSession(), nin, date, "0");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadRecycler() {
        if (companies != null) {
            adapter = new PortfolioAdapter(this, companies);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter);
            ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        stopSpeaker();
    }
}
