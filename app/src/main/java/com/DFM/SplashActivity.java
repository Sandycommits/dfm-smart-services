package com.DFM;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.DFM.utils.PreferenceConnector;

public class SplashActivity extends AppCompatActivity {

    private Handler handler = new Handler();
    private int SPLASH_TIMEOUT = 2000;

    /**
     * Background thread runnable for splash time out.
     */
    private Runnable splashRunnable = new Runnable() {
        @Override
        public void run() {
            boolean isAgree = PreferenceConnector.readBoolean(SplashActivity.this, PreferenceConnector.TERMS, false);
            if (isAgree) {
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
                intent = null;
            } else {
                Intent intent = new Intent(SplashActivity.this, TermsActivity.class);
                startActivity(intent);
                intent = null;
            }
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        handler.postDelayed(splashRunnable, SPLASH_TIMEOUT);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (handler != null) {
            handler.removeCallbacks(splashRunnable);
            handler = null;
        }
    }

}
