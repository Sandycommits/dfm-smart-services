package com.DFM;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;

import com.DFM.dto.Attachment;
import com.DFM.dto.Body;
import com.DFM.dto.CaseData;
import com.DFM.dto.CaseInfoResponse;
import com.DFM.dto.ComboBox;
import com.DFM.dto.EpayResponse;
import com.DFM.dto.FormInfoResponse;
import com.DFM.dto.Header;
import com.DFM.dto.IVestorCardRequest;
import com.DFM.dto.IVestorCardResponse;
import com.DFM.dto.Login;
import com.DFM.dto.NIN;
import com.DFM.dto.PayementInfo;
import com.DFM.dto.UserCode;
import com.DFM.fonts.HelveticaNeueBoldTextView;
import com.DFM.fonts.HelveticaNeueEditText;
import com.DFM.interfaces.OnDialogItemClickListener;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.Constants;
import com.DFM.utils.PreferenceConnector;
import com.Wsdl2Code.WebServices.GeneralResource.GeneralResource;
import com.Wsdl2Code.WebServices.MiscFunctions.IWsdl2CodeEvents;
import com.Wsdl2Code.WebServices.MiscFunctions.MiscFunctions;
import com.Wsdl2Code.WebServices.ePay5.ePay5;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.mapper.MapperWrapper;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CashDividendsActivity extends BaseActivity {

    private static final int EMIRATES_ID = 1;
    private static final int PASSPORT = 2;
    private static final int STATEMENT = 3;
    private static final int PERMISSION_CODE = 1;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.selectNin)
    HelveticaNeueEditText selectNin;
    @BindView(R.id.selectService)
    HelveticaNeueEditText selectService;
    @BindView(R.id.caption)
    HelveticaNeueBoldTextView caption;
    @BindView(R.id.bankRow)
    TableRow bankRow;
    @BindView(R.id.lastLine)
    View lastLine;
    @BindView(R.id.attachmentsLayout)
    LinearLayout attachmentsLayout;
    @BindView(R.id.transferLayout)
    LinearLayout transferLayout;

    @BindView(R.id.emiratesID)
    ImageView emiratesID;
    @BindView(R.id.passport)
    ImageView passport;
    @BindView(R.id.bankStatement)
    ImageView bankStatement;
    @BindView(R.id.idClose)
    ImageView idClose;
    @BindView(R.id.passportClose)
    ImageView passportClose;
    @BindView(R.id.bankClose)
    ImageView bankClose;
    @BindView(R.id.submit)
    Button submit;

    @BindView(R.id.bankName)
    HelveticaNeueEditText bankName;
    @BindView(R.id.accountNumber)
    HelveticaNeueEditText accountNumber;
    @BindView(R.id.accountNumberOther)
    HelveticaNeueEditText accountNumberOther;
    @BindView(R.id.iban)
    HelveticaNeueEditText iban;
    @BindView(R.id.swift)
    HelveticaNeueEditText swift;
    @BindView(R.id.country)
    HelveticaNeueEditText country;
    @BindView(R.id.city)
    HelveticaNeueEditText city;
    @BindView(R.id.branchName)
    HelveticaNeueEditText branchName;


    private String[] permissions = new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA};
    private List<Attachment> attachments;
    private int userID;

    private String[] services;
    private UserCode userCode;
    private Body formData;
    private List<ComboBox> options;
    private Login login;

    private String[] countries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_dividends);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.cash_dividend_form));


        login = (Login) CommonMethods.retrieveObject(this, PreferenceConnector.LOGIN, new Login());
//        services = getResources().getStringArray(R.array.dividend_service_array);
        userCode = (UserCode) CommonMethods.retrieveObject(this, PreferenceConnector.USER_CODES, new UserCode());
        userID = Integer.parseInt(login.getData().getUserID());
        attachments = new ArrayList<>();
//        loadData();

        options = new ArrayList<>();
        loadComboService();
    }

    private void loadComboService() {

        MiscFunctions miscFunctions = new MiscFunctions(new IWsdl2CodeEvents() {
            @Override
            public void Wsdl2CodeStartedRequest() {

            }

            @Override
            public void Wsdl2CodeFinished(String methodName, Object data) {
                dismissProgressDialog();
                String response = data.toString();
                response = response.replaceAll("&", "&amp;");
                DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                try {
                    DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
                    Document doc = docBuilder.parse(new InputSource(new StringReader(response)));
                    NodeList nodeList = doc.getElementsByTagName("Data").item(0).getChildNodes();
                    int length = nodeList.getLength();
                    for (int i = 0; i < length; i = i + 2) {
                        Node optionNode = nodeList.item(i).getFirstChild();
                        Node valueNode = nodeList.item(i + 1).getFirstChild();
                        ComboBox comboBox = new ComboBox();
                        if (optionNode != null) {
                            comboBox.setOption(optionNode.getNodeValue());
                        } else {
                            comboBox.setOption("");
                        }
                        if (valueNode != null) {
                            comboBox.setValue(valueNode.getNodeValue());
                        } else {
                            comboBox.setValue("");
                        }
                        options.add(comboBox);
                        loadServices();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                loadData();
            }

            @Override
            public void Wsdl2CodeFinishedWithException(Exception ex) {
                dismissProgressDialog();
            }

            @Override
            public void Wsdl2CodeEndedRequest() {

            }
        });
        showProgressDialog();
        try {
            miscFunctions.ComboListAsync("cashdividends", language);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadServices() {
        services = new String[options.size()];
        for (int i = 0; i < options.size(); i++) {
            ComboBox comboBox = options.get(i);
            services[i] = comboBox.getOption();
        }
    }

    private void loadData() {

        Body body = (Body) getIntent().getSerializableExtra("isEdit");
        if (body != null) {
            IVestorCardRequest request = new IVestorCardRequest();

            Header header = new Header();
            header.setUserID(userID);
            header.setRefNo(body.getRefNo());
            header.setFormOperation(4);
            Body innerBody = new Body();
            innerBody.setRefNo(body.getRefNo());
            innerBody.setUserID(userID);
            innerBody.setCrmTrackingNumber(body.getCrmTrackingNumber());
            innerBody.setApplicationDate(CommonMethods.getIVestorDateFormat(body.getApplicationDate()));
            CaseData caseData = new CaseData();
            caseData.setCaseID(body.getCrmTrackingNumber());
            caseData.setUserID(userID + "");
            request.setBody(innerBody);
            request.setCaseData(caseData);
            request.setHeader(header);
            if (CommonMethods.isOnline(this)) {
                showProgressDialog();
                apiService.requestCashDividends(request).enqueue(new Callback<IVestorCardResponse>() {
                    @Override
                    public void onResponse(Call<IVestorCardResponse> call, Response<IVestorCardResponse> response) {
                        dismissProgressDialog();
                        IVestorCardResponse iVestorCardResponse = response.body();
                        FormInfoResponse formResponse = iVestorCardResponse.getFormInfoResponse();
                        formData = formResponse.getFormData();
                        selectNin.setText(formData.getInvestorNumber() + "-" + formData.getInvestorName());
                        String serviceType = formData.getDividendsVia();
                        if (serviceType.equalsIgnoreCase("1")) {
                            selectService.setText(services[0]);
                            caption.setText(getCaption(services[0]));
                            transferLayout.setVisibility(View.VISIBLE);
                            bankRow.setVisibility(View.VISIBLE);
                            lastLine.setVisibility(View.VISIBLE);
                            bankName.setText(formData.getBankName());
                            accountNumber.setText(formData.getAccountNumberUAE());
                            accountNumberOther.setText(formData.getAccountNumber());
                            iban.setText(formData.getIbanNumber());
                            swift.setText(formData.getSwiftId());
                            country.setText(formData.getCountry());
                            city.setText(formData.getCity());
                            branchName.setText(formData.getBranchName());

                            //Disabling text fields
                            bankName.setEnabled(false);
                            bankName.setBackgroundResource(R.drawable.disabled_edittext);
                            accountNumber.setEnabled(false);
                            accountNumber.setBackgroundResource(R.drawable.disabled_edittext);
                            accountNumberOther.setEnabled(false);
                            accountNumberOther.setBackgroundResource(R.drawable.disabled_edittext);
                            iban.setEnabled(false);
                            iban.setBackgroundResource(R.drawable.disabled_edittext);
                            swift.setEnabled(false);
                            swift.setBackgroundResource(R.drawable.disabled_edittext);
                            country.setEnabled(false);
                            country.setBackgroundResource(R.drawable.disabled_edittext);
                            country.setClickable(false);
                            city.setEnabled(false);
                            city.setBackgroundResource(R.drawable.disabled_edittext);
                            branchName.setEnabled(false);
                            branchName.setBackgroundResource(R.drawable.disabled_edittext);
                        } else {
                            selectService.setText(services[1]);
                            caption.setText(getCaption(services[1]));
                        }
                    }

                    @Override
                    public void onFailure(Call<IVestorCardResponse> call, Throwable t) {
                        dismissProgressDialog();
                    }
                });
            } else {
                CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
            }
            selectNin.setClickable(false);
            selectNin.setBackgroundResource(R.drawable.disabled_edittext);
            selectService.setClickable(false);
            selectService.setBackgroundResource(R.drawable.disabled_edittext);
        } else {
            userCode = (UserCode) CommonMethods.retrieveObject(this, PreferenceConnector.USER_CODES, new UserCode());
            NIN root = CommonMethods.getRootNin(userCode);
            selectNin.setText(root.getNIN() + "-" + root.getAccountName());
            if (options != null && options.size() > 1) {
                ComboBox comboBox = options.get(1);
                selectService.setText(comboBox.getOption());
                caption.setText(getCaption(comboBox.getOption()));
            }
        }
    }

    @Override
    @OnClick({R.id.selectNin, R.id.selectService, R.id.country, R.id.emiratesID, R.id.passport, R.id.bankStatement, R.id.submit, R.id.idClose, R.id.passportClose, R.id.bankClose})
    public void onViewClicks(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.selectNin:
                String[] ninStrings = CommonMethods.getNinArray(userCode.getNinList());
                int selectedPosition = CommonMethods.getStringPosition(ninStrings, selectNin.getText().toString());
                CommonMethods.showListDialog(this, ninStrings, new OnDialogItemClickListener() {
                    @Override
                    public void onDialogItem(String selected) {
                        selectNin.setText(selected);
                    }
                }, selectedPosition);
                break;
            case R.id.selectService:
                int servPosition = CommonMethods.getStringPosition(services, selectService.getText().toString());
                CommonMethods.showListDialog(this, services, new OnDialogItemClickListener() {
                    @Override
                    public void onDialogItem(String selected) {
                        selectService.setText(selected);
                        caption.setText(getCaption(selected));
                        if (selected.equalsIgnoreCase(options.get(0).getOption())) {
                            transferLayout.setVisibility(View.VISIBLE);
                            bankRow.setVisibility(View.VISIBLE);
                            lastLine.setVisibility(View.VISIBLE);
                            GeneralResource resources = new GeneralResource(new com.Wsdl2Code.WebServices.GeneralResource.IWsdl2CodeEvents() {
                                @Override
                                public void Wsdl2CodeStartedRequest() {

                                }

                                @Override
                                public void Wsdl2CodeFinished(String methodName, Object data) {
                                    dismissProgressDialog();
                                    String response = data.toString();
                                    if (language.equalsIgnoreCase("ar")) {
                                        countries = response.split(",");
                                    } else {
                                        countries = response.split(", ");
                                    }
                                }

                                @Override
                                public void Wsdl2CodeFinishedWithException(Exception ex) {
                                    dismissProgressDialog();
                                }

                                @Override
                                public void Wsdl2CodeEndedRequest() {

                                }
                            }, language);
                            try {
                                showProgressDialog();
                                resources.GetResourceAsync();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            transferLayout.setVisibility(View.GONE);
                            bankRow.setVisibility(View.GONE);
                            lastLine.setVisibility(View.GONE);
                            attachmentsLayout.setVisibility(View.VISIBLE);
                        }
                    }
                }, servPosition);
                break;
            case R.id.country:
                int countryPosition = CommonMethods.getStringPosition(countries, country.getText().toString());
                CommonMethods.showListDialog(this, countries, new OnDialogItemClickListener() {
                    @Override
                    public void onDialogItem(String selected) {
                        country.setText(selected);
                    }
                }, countryPosition);
                break;
            case R.id.emiratesID:
                if (CommonMethods.hasPermissions(this, permissions)) {
                    EasyImage.openChooserWithGallery(this, "Select option", EMIRATES_ID);
                } else {
                    ActivityCompat.requestPermissions(this, permissions, EMIRATES_ID);
                }
                break;
            case R.id.passport:
                if (CommonMethods.hasPermissions(this, permissions)) {
                    EasyImage.openChooserWithGallery(this, "Select option", PASSPORT);
                } else {
                    ActivityCompat.requestPermissions(this, permissions, PASSPORT);
                }
                break;
            case R.id.bankStatement:
                if (CommonMethods.hasPermissions(this, permissions)) {
                    EasyImage.openChooserWithGallery(this, "Select option", STATEMENT);
                } else {
                    ActivityCompat.requestPermissions(this, permissions, STATEMENT);
                }
                break;
            case R.id.submit:
                Body body = new Body();
                NIN root = CommonMethods.getRootNin(userCode);
                body.setApplicationDate(CommonMethods.getCurrentDateOnly());
                body.setCrmTrackingNumber("");
                body.setUserID(userID);
                String selected = selectNin.getText().toString();
                if (selected.contains("-")) {
                    String[] investor = selected.split("-");
                    body.setInvestorName(investor[1]);
                    body.setInvestorNumber(investor[0]);
                } else {
                    body.setInvestorName(root.getAccountName());
                    body.setInvestorNumber(root.getNIN());
                }
                String service = selectService.getText().toString();
                if (options.size() > 0) {
                    if (service.equalsIgnoreCase(options.get(0).getOption())) {
                        if (accountNumber.getText().toString().length() > 0) {
                            String aedCheck = CommonMethods.validateTextFields(new EditText[]{bankName, iban, country, city, branchName});
                            if (aedCheck.equals("")) {
                                body.setDividendsVia(1 + "");
                                body.setBankName(bankName.getText().toString());
                                body.setAccountNumberUAE(accountNumber.getText().toString());
                                body.setAccountNumber(accountNumberOther.getText().toString());
                                body.setIbanNumber(iban.getText().toString());
                                body.setSwiftId(swift.getText().toString());
                                body.setCountry(country.getText().toString());
                                body.setCity(city.getText().toString());
                                body.setBranchName(branchName.getText().toString());
                                if (bankClose.getVisibility() == View.VISIBLE) {
                                    if (formData != null) {
                                        updateForm(formData);
                                    } else {
                                        submitForm(body);
                                    }
                                } else {
                                    CommonMethods.displayOKDialogue(CashDividendsActivity.this, getString(R.string.please_attach_stmt));
                                }
                            } else {
                                CommonMethods.displayOKDialogue(this, getString(R.string.please_fill) + " " + aedCheck);
                            }
                        } else if (accountNumberOther.getText().toString().length() > 0) {
                            String otherCheck = CommonMethods.validateTextFields(new EditText[]{bankName, swift, country, city, branchName});
                            if (otherCheck.equals("")) {
                                body.setDividendsVia(1 + "");
                                body.setBankName(bankName.getText().toString());
                                body.setAccountNumberUAE(accountNumber.getText().toString());
                                body.setAccountNumber(accountNumberOther.getText().toString());
                                body.setIbanNumber(iban.getText().toString());
                                body.setSwiftId(swift.getText().toString());
                                body.setCountry(country.getText().toString());
                                body.setCity(city.getText().toString());
                                body.setBranchName(branchName.getText().toString());
                                if (bankClose.getVisibility() == View.VISIBLE) {
                                    if (formData != null) {
                                        updateForm(formData);
                                    } else {
                                        submitForm(body);
                                    }
                                } else {
                                    CommonMethods.displayOKDialogue(CashDividendsActivity.this, getString(R.string.please_attach_stmt));
                                }
                            } else {
                                CommonMethods.displayOKDialogue(this, getString(R.string.please_fill) + " " + otherCheck);
                            }
                        } else {
                            CommonMethods.displayOKDialogue(this, getString(R.string.please_fill) + " " + getString(R.string.account_no_en));
                        }
                        /*String value = CommonMethods.validateTextFields(new EditText[]{bankName, iban, country, city, branchName});
                        if (value.equals("")) {

                            if (accountNumber.getText().toString().length() > 0 || accountNumberOther.getText().toString().length() > 0) {
                                body.setDividendsVia(1 + "");
                                body.setBankName(bankName.getText().toString());
                                body.setAccountNumberUAE(accountNumber.getText().toString());
                                body.setAccountNumber(accountNumberOther.getText().toString());
                                body.setIbanNumber(iban.getText().toString());
                                body.setSwiftId(swift.getText().toString());
                                body.setCountry(country.getText().toString());
                                body.setCity(city.getText().toString());
                                body.setBranchName(branchName.getText().toString());

//                        submitForm(body);
                                if (formData != null) {
                                    updateForm(formData);
                                } else {
                                    submitForm(body);
                                }
                            } else {
                                CommonMethods.displayOKDialogue(this, getString(R.string.please_fill) + " " + accountNumber.getHint().toString());
                            }
                        } else {
                            CommonMethods.displayOKDialogue(this, getString(R.string.please_fill) + " " + value);
                        }*/
                    } else {
                        body.setDividendsVia(getServiceType(service) + "");
                        if (formData != null) {
                            updateForm(formData);
                        } else {
                            submitForm(body);
                        }
                    }
                }
                break;
            case R.id.idClose:
                idClose.setVisibility(View.GONE);
                emiratesID.setImageResource(R.drawable.ic_clip);
                removeAttachment(0);
                break;
            case R.id.passportClose:
                passportClose.setVisibility(View.GONE);
                passport.setImageResource(R.drawable.ic_clip);
                removeAttachment(1);
                break;
            case R.id.bankClose:
                bankClose.setVisibility(View.GONE);
                bankStatement.setImageResource(R.drawable.ic_clip);
                removeAttachment(2);
                break;
            default:
                break;
        }
    }

    public int getServiceType(String selected) {
        for (int i = 0; i < options.size(); i++) {
            ComboBox comboBox = options.get(i);
            if (selected.equalsIgnoreCase(comboBox.getOption())) {
                return i + 1;
            }
        }
        return 0;
    }

    private String getCaption(String selected) {
        for (ComboBox comboBox : options) {
            if (selected.equalsIgnoreCase(comboBox.getOption())) {
                return comboBox.getValue();
            }
        }
        return "";
    }

    private void updateForm(Body body) {

        IVestorCardRequest request = new IVestorCardRequest();
        Header header = new Header();
        header.setUserID(userID);
        header.setRequestID((int) Math.random());
        header.setSessionID("");
        header.setHasAttachments(true);
        header.setRefNo(body.getRefNo());
        header.setFormOperation(2);

        CaseData caseData = new CaseData();
        caseData.setCaseID(body.getCrmTrackingNumber());
        caseData.setUserID(userID + "");
        caseData.setCaseSubject("");
        request.setAttachments(attachments);
        request.setBody(body);
        request.setCaseData(caseData);
        request.setHeader(header);
        if (CommonMethods.isOnline(this)) {
            showProgressDialog();
            apiService.requestCashDividends(request).enqueue(new Callback<IVestorCardResponse>() {
                @Override
                public void onResponse(Call<IVestorCardResponse> call, Response<IVestorCardResponse> response) {
                    dismissProgressDialog();
                    IVestorCardResponse cardResponse = response.body();
                    FormInfoResponse formInfo = cardResponse.getFormInfoResponse();
                    PayementInfo payementInfo = formInfo.getPayementInfo();
                    if (payementInfo != null && payementInfo.getPaymentAmount() > 0) {

                    } else {
                        CaseInfoResponse caseInfo = cardResponse.getCaseInfoResponse();
                        Intent intent = new Intent(CashDividendsActivity.this, TrackRequestActivity.class);
                        intent.putExtra("reference", caseInfo.getTrackingNumber());
                        startActivity(intent);
                        CashDividendsActivity.this.finish();
                    }
                }

                @Override
                public void onFailure(Call<IVestorCardResponse> call, Throwable t) {
                    dismissProgressDialog();
                }
            });
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == PERMISSION_CODE) {
                EasyImage.openChooserWithGallery(this, "Select option", EMIRATES_ID);
            } else if (requestCode == PASSPORT) {
                EasyImage.openChooserWithGallery(this, "Select option", PASSPORT);
            } else if (requestCode == STATEMENT) {
                EasyImage.openChooserWithGallery(this, "Select option", STATEMENT);
            }
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new EasyImage.Callbacks() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource imageSource, int i) {
                CommonMethods.displayOKDialogue(CashDividendsActivity.this, getString(R.string.something_wrong));
            }

            @Override
            public void onImagePicked(File file, EasyImage.ImageSource imageSource, int i) {
                String filePath = file.getPath();
                Bitmap originalBitmap = BitmapFactory.decodeFile(filePath);

                Bitmap bitmap = CommonMethods.getRotatedBitMap(originalBitmap, filePath);


                Attachment attachment = new Attachment();
                if (i == EMIRATES_ID) {
                    removeAttachment(0);
                    idClose.setVisibility(View.VISIBLE);
                    emiratesID.setImageBitmap(bitmap);
                    attachment.setDocumentName("Emirates ID");
                    attachment.setPosition(0);
                } else if (i == PASSPORT) {
                    removeAttachment(1);
                    passportClose.setVisibility(View.VISIBLE);
                    passport.setImageBitmap(bitmap);
                    attachment.setDocumentName("Passport");
                    attachment.setPosition(1);
                } else if (i == STATEMENT) {
                    removeAttachment(2);
                    bankClose.setVisibility(View.VISIBLE);
                    bankStatement.setImageBitmap(bitmap);
                    attachment.setDocumentName("Bank Statement");
                    attachment.setPosition(2);
                }
                attachment.setFileBytes(CommonMethods.bitmapToBase64(bitmap));
                attachment.setFileName(file.getName());
                attachment.setFileType("jpeg");
                attachments.add(attachment);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource imageSource, int i) {

            }
        });
    }

    private void submitForm(Body body) {

        IVestorCardRequest request = new IVestorCardRequest();
        Header header = new Header();
        header.setUserID(userID);
        header.setRequestID((int) Math.random());
        header.setSessionID("");
        header.setHasAttachments(true);
        header.setRefNo("");
        header.setFormOperation(1);

        CaseData caseData = new CaseData();
        caseData.setCaseID("");
        caseData.setUserID(userID + "");
        caseData.setCaseSubject("");
        request.setAttachments(attachments);
        request.setBody(body);
        request.setCaseData(caseData);
        request.setHeader(header);
        if (CommonMethods.isOnline(this)) {
            showProgressDialog();
            apiService.requestCashDividends(request).enqueue(new Callback<IVestorCardResponse>() {
                @Override
                public void onResponse(Call<IVestorCardResponse> call, Response<IVestorCardResponse> response) {
                    dismissProgressDialog();
                    IVestorCardResponse cardResponse = response.body();
                    FormInfoResponse formInfo = cardResponse.getFormInfoResponse();
                    PayementInfo payementInfo = cardResponse.getFormInfoResponse().getPayementInfo();
                    if (payementInfo != null && payementInfo.getPaymentAmount() > 0) {
                        String text = "CUSTID=" + userID + "&UserName=" + login.getUserName() + "&USERID=" + userID + "&ServiceCode=MOB&PaymentAmount=" + payementInfo.getPaymentAmount() + "&isCaseCreation=true&RefNo=" + formInfo.getReferenceNumber() + "&TRName=Cash Dividends Request Form&SavedFormUrl=" + formInfo.getUrl() + "?CUSTID=" + userID + "&Lang=en&OtpRequired=false";
                        ePay5 ePay5 = new ePay5(new com.Wsdl2Code.WebServices.ePay5.IWsdl2CodeEvents() {
                            @Override
                            public void Wsdl2CodeStartedRequest() {

                            }

                            @Override
                            public void Wsdl2CodeFinished(String methodName, Object data) {
                                dismissProgressDialog();
                                XStream stream = new XStream(new DomDriver()) {
                                    // to enable ignoring of unknown elements
                                    @Override
                                    protected MapperWrapper wrapMapper(MapperWrapper next) {
                                        return new MapperWrapper(next) {
                                            @Override
                                            public boolean shouldSerializeMember(Class definedIn, String fieldName) {
                                                if (definedIn == Object.class) {
                                                    try {
                                                        return this.realClass(fieldName) != null;
                                                    } catch (Exception e) {
                                                        return false;
                                                    }
                                                }
                                                return super.shouldSerializeMember(definedIn, fieldName);
                                            }
                                        };
                                    }
                                };
                                stream.processAnnotations(EpayResponse.class);
                                EpayResponse epayResponse = (EpayResponse) stream.fromXML(data.toString());
                                if (login.getStatus().equalsIgnoreCase("valid")) {
                                    String encoded = Constants.EPAY_URL + epayResponse.getData();
                                /*Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(encoded));
                                startActivity(intent);*/

                                    Intent intent = new Intent(CashDividendsActivity.this, PaymentActivity.class);
                                    intent.putExtra("payment", encoded);
                                    startActivity(intent);
                                    CashDividendsActivity.this.finish();
                                } else {
                                    CommonMethods.displayOKDialogue(CashDividendsActivity.this, getString(R.string.something_wrong));
                                }
                            }

                            @Override
                            public void Wsdl2CodeFinishedWithException(Exception ex) {
                                dismissProgressDialog();
                            }

                            @Override
                            public void Wsdl2CodeEndedRequest() {

                            }
                        });
                        try {
                            ePay5.ePay5EncryptionNewAsync(login.getData().getSession(), text);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        CaseInfoResponse caseInfo = cardResponse.getCaseInfoResponse();
                        Intent intent = new Intent(CashDividendsActivity.this, TrackRequestActivity.class);
                        intent.putExtra("reference", caseInfo.getTrackingNumber());
                        startActivity(intent);
                        CashDividendsActivity.this.finish();
                    }
                }

                @Override
                public void onFailure(Call<IVestorCardResponse> call, Throwable t) {
                    dismissProgressDialog();
                }
            });
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void removeAttachment(int position) {

        for (int i = 0; i < attachments.size(); i++) {
            Attachment attachment = attachments.get(i);
            if (position == attachment.getPosition())
                attachments.remove(attachment);
        }
    }
}
