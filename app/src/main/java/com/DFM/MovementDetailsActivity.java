package com.DFM;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.DFM.dto.MovementRecord;
import com.DFM.fonts.HelveticaNeueEditText;
import com.DFM.utils.CommonMethods;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovementDetailsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.company)
    HelveticaNeueEditText company;
    @BindView(R.id.transaction)
    HelveticaNeueEditText transaction;
    @BindView(R.id.from)
    HelveticaNeueEditText from;
    @BindView(R.id.to)
    HelveticaNeueEditText to;
    @BindView(R.id.reference)
    HelveticaNeueEditText reference;
    @BindView(R.id.qty)
    HelveticaNeueEditText quantity;
    @BindView(R.id.origin)
    HelveticaNeueEditText origin;
    @BindView(R.id.transactionDate)
    HelveticaNeueEditText transactionDate;
    DecimalFormat formatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movement_details);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.details));

        NumberFormat nFormatter = NumberFormat.getNumberInstance(Locale.US);
        formatter = (DecimalFormat) nFormatter;
        formatter.applyPattern("#,###,###");

        loadData();
    }

    private void loadData() {
        MovementRecord record = (MovementRecord) getIntent().getSerializableExtra("movement");
        company.setText(record.getCompany());
        transaction.setText(record.getTransaction());
        transactionDate.setText(CommonMethods.getDateInFormat(record.getDate()));
        from.setText(record.getFromName());
        to.setText(record.getToName());
        reference.setText(record.getReference() + "\n" + record.getToReference());
        quantity.setText(formatter.format(record.getQuantity()));
        origin.setText(record.getOrigin());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
