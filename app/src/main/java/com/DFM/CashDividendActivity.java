package com.DFM;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.DFM.dto.CashDividend;
import com.DFM.dto.Company;
import com.DFM.dto.Dividend;
import com.DFM.dto.Login;
import com.DFM.dto.NIN;
import com.DFM.dto.UserCode;
import com.DFM.expandable.DividendHeader;
import com.DFM.expandable.ExpandableAdapter;
import com.DFM.fonts.HelveticaNeueEditText;
import com.DFM.fonts.HelveticaNeueTextView;
import com.DFM.interfaces.OnDateSelectionListener;
import com.DFM.interfaces.OnDialogItemClickListener;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.PreferenceConnector;
import com.Wsdl2Code.WebServices.Enquiry.Enquiry;
import com.Wsdl2Code.WebServices.Enquiry.IWsdl2CodeEvents;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CashDividendActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    DecimalFormat formatter;
    @BindView(R.id.selectNin)
    HelveticaNeueEditText selectNin;
    @BindView(R.id.startDate)
    HelveticaNeueTextView startDate;
    @BindView(R.id.endDate)
    HelveticaNeueTextView endDate;
    @BindView(R.id.selectCompany)
    HelveticaNeueEditText selectCompany;
    @BindView(R.id.dividendRecycler)
    RecyclerView dividendRecycler;
    @BindView(R.id.print)
    ImageView print;
    @BindView(R.id.rootView)
    View rootView;
    int PERMISSION_ID = 1;
    @BindView(R.id.noData)
    RelativeLayout noData;
    private UserCode userCode;
    private HashMap<String, ArrayList<Company>> companyMap = new HashMap<>();
    private String[] permissions = new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_dividend);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.cash_dividend_stmt));
        print.setVisibility(View.VISIBLE);

        NumberFormat nFormatter = NumberFormat.getNumberInstance(Locale.US);
        formatter = (DecimalFormat) nFormatter;
        formatter.applyPattern("#,###,###.##");
        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);

        endDate.setText(CommonMethods.getCurrentDateDisplay());
        startDate.setText(CommonMethods.getPreviousMonthDateDisplay());
        userCode = (UserCode) CommonMethods.retrieveObject(this, PreferenceConnector.USER_CODES, new UserCode());
        NIN root = CommonMethods.getRootNin(userCode);
        selectNin.setText(root.getNIN() + "-" + root.getAccountName());
        companyMap = CommonMethods.retrieveMapObject(this, PreferenceConnector.COMPANY_MAP);
        callService();

    }

    @Override
    @OnClick({R.id.selectNin, R.id.startDate, R.id.endDate, R.id.selectCompany, R.id.print})
    public void onViewClicks(View view) {
        int id = view.getId();
        String nin = CommonMethods.getInitialLiteral(selectNin.getText().toString());
        switch (id) {
            case R.id.selectNin:
                String[] ninStrings = CommonMethods.getNinArray(userCode.getNinList());
                int selectedPosition = CommonMethods.getStringPosition(ninStrings, selectNin.getText().toString());
                CommonMethods.showListDialog(this, ninStrings, new OnDialogItemClickListener() {
                    @Override
                    public void onDialogItem(String selected) {
                        selectNin.setText(selected);
                        selectCompany.setText(getString(R.string.all));
                        callService();
                    }
                }, selectedPosition);
                break;
            case R.id.startDate:
                CommonMethods.displayDatePickerDialog(this, new OnDateSelectionListener() {
                    @Override
                    public void onDateSelected(String date) {
                        startDate.setText(date);
                        if (!selectCompany.getText().toString().equalsIgnoreCase(getString(R.string.all))) {
                            companyService();
                        } else {
                            callService();
                        }
                    }
                }, false);
                break;
            case R.id.endDate:
                CommonMethods.displayDatePickerDialog(this, new OnDateSelectionListener() {
                    @Override
                    public void onDateSelected(String date) {
                        endDate.setText(date);
                        if (!selectCompany.getText().toString().equalsIgnoreCase(getString(R.string.all))) {
                            companyService();
                        } else {
                            callService();
                        }
                    }
                }, true);
                break;
            case R.id.selectCompany:
                String[] companyArray = CommonMethods.getCompanyArray(companyMap.get(nin), language);
                if (companyArray.length > 0) {
                    int compPosition = CommonMethods.getStringPosition(companyArray, selectCompany.getText().toString());
                    CommonMethods.showListDialog(this, companyArray, new OnDialogItemClickListener() {
                        @Override
                        public void onDialogItem(String selected) {
                            selectCompany.setText(selected);
                            if (!selected.equalsIgnoreCase(getString(R.string.all))) {
                                companyService();
                            } else {
                                callService();
                            }
                        }
                    }, compPosition);
                }
                break;
            case R.id.print:
                if (CommonMethods.hasPermissions(this, permissions)) {
                    Bitmap bitmap = CommonMethods.getScreenShot(rootView);
                    CommonMethods.store(bitmap, "dfm_screenshot.jpeg", this);
                } else {
                    ActivityCompat.requestPermissions(this, permissions, PERMISSION_ID);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            Bitmap bitmap = CommonMethods.getScreenShot(rootView);
            CommonMethods.store(bitmap, "dfm_screenshot.jpeg", this);
        }
    }

    private void callService() {
        if (CommonMethods.isOnline(this)) {
            Enquiry enquiry = new Enquiry(new IWsdl2CodeEvents() {
                @Override
                public void Wsdl2CodeStartedRequest() {

                }

                @Override
                public void Wsdl2CodeFinished(String methodName, Object data) {
                    dismissProgressDialog();
//                    XStream xStream = new XStream(new DomDriver());
                    xStream.processAnnotations(Login.class);
                    Login login = (Login) xStream.fromXML(data.toString());
                    CashDividend cashDividend = login.getData().getCashDividend();
                    if (cashDividend != null) {
                        ArrayList<Dividend> records = login.getData().getCashDividend().getRecords();
                        if (records != null) {
                            noData.setVisibility(View.GONE);
                            bindData(records);
                        } else {
                            bindData(new ArrayList<Dividend>());
                            noData.setVisibility(View.VISIBLE);
                        }
                    } else {
                        CommonMethods.displayOKDialogue(CashDividendActivity.this, getString(R.string.session_expired));
                    }
                }

                @Override
                public void Wsdl2CodeFinishedWithException(Exception ex) {
                    dismissProgressDialog();
                }

                @Override
                public void Wsdl2CodeEndedRequest() {

                }
            });
            Login login = (Login) CommonMethods.retrieveObject(this, PreferenceConnector.LOGIN, new Login());
            String nin = CommonMethods.getInitialLiteral(selectNin.getText().toString());
            String fromDate = CommonMethods.getServiceDateFormat(startDate.getText().toString());
            String toDate = CommonMethods.getServiceDateFormat(endDate.getText().toString());
            showProgressDialog();
            try {
                enquiry.GetCashDividendOverviewAsync(login.getData().getSession(), nin, fromDate, toDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }
    }

    private void bindData(ArrayList<Dividend> records) {

        LinkedHashSet<String> symbols = new LinkedHashSet<>();
        for (Dividend dividend : records) {
            symbols.add(dividend.getSYMBOL().trim());
        }

        List<DividendHeader> categories = new ArrayList<>();
        for (String symbol : symbols) {
            ArrayList<Dividend> tempList = new ArrayList<>();
            for (Dividend dividend : records) {
                if (symbol.equalsIgnoreCase(dividend.getSYMBOL().trim())) {
                    tempList.add(dividend);
                }
            }
            categories.add(new DividendHeader(symbol, tempList));
//            tempList.clear();
        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        //instantiate your adapter with the list of genres
        ExpandableAdapter adapter = new ExpandableAdapter(categories, this);
        dividendRecycler.setLayoutManager(layoutManager);
        dividendRecycler.setAdapter(adapter);
    }

    private void companyService() {
        if (CommonMethods.isOnline(this)) {
            Enquiry enquiry = new Enquiry(new IWsdl2CodeEvents() {
                @Override
                public void Wsdl2CodeStartedRequest() {

                }

                @Override
                public void Wsdl2CodeFinished(String methodName, Object data) {
                    dismissProgressDialog();
//                    XStream xStream = new XStream(new DomDriver());
                    xStream.processAnnotations(Login.class);
                    Login login = (Login) xStream.fromXML(data.toString());
                    ArrayList<Dividend> records = login.getData().getDividendByCompany().getRecords();
                    if (records != null) {
                        noData.setVisibility(View.GONE);
                        bindData(records);
                    } else {
                        bindData(new ArrayList<Dividend>());
                        noData.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void Wsdl2CodeFinishedWithException(Exception ex) {
                    dismissProgressDialog();
                }

                @Override
                public void Wsdl2CodeEndedRequest() {

                }
            });
            Login login = (Login) CommonMethods.retrieveObject(this, PreferenceConnector.LOGIN, new Login());
            String nin = CommonMethods.getInitialLiteral(selectNin.getText().toString());
            String fromDate = CommonMethods.getServiceDateFormat(startDate.getText().toString());
            String toDate = CommonMethods.getServiceDateFormat(endDate.getText().toString());
            String company = CommonMethods.getInitialLiteral(selectCompany.getText().toString());
            showProgressDialog();
            try {
                enquiry.GetCashDividendByCompanyAsync(login.getData().getSession(), nin, fromDate, toDate, company);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
