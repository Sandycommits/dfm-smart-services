package com.DFM.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.DFM.R;

/**
 * Created by Sandeep on 22/10/17.
 */

public class NavigationAdapter extends ArrayAdapter<String> {

    private int layoutId;
    private Context context;
    private int[] drawables = {R.drawable.ic_profile_menu, R.drawable.ic_menu_mail, R.drawable.ic_menu_location, R.drawable.ic_my_statement, R.drawable.ic_cash_dividend, R.drawable.ic_menu_call, R.drawable.ic_menu_web, R.drawable.ic_settings, R.drawable.ic_logout_white};

    public NavigationAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull String[] objects) {
        super(context, resource, objects);
        this.layoutId = resource;
        this.context = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        NavigationAdapter.ViewHolder viewHolder = null;
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layoutId, null);
            viewHolder = new NavigationAdapter.ViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.icon);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (NavigationAdapter.ViewHolder) convertView.getTag();
        }

        final String title = getItem(position);
        viewHolder.title.setText(title);
        viewHolder.icon.setImageResource(drawables[position]);
        return convertView;

    }

    private class ViewHolder {

        TextView title;
        ImageView icon;
    }
}
