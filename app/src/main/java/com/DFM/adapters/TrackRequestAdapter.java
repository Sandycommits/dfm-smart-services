package com.DFM.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.DFM.R;
import com.DFM.dto.Case;
import com.DFM.fonts.HelveticaNeueBoldTextView;
import com.DFM.fonts.HelveticaNeueTextView;
import com.DFM.interfaces.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macadmin on 12/14/17.
 */

public class TrackRequestAdapter extends RecyclerView.Adapter<TrackRequestAdapter.ViewHolder> {

    private List<Case> cases = new ArrayList<>();
    private LayoutInflater mInflater;
    private OnItemClickListener mClickListener;
    private Context context;
    private String language;

    public TrackRequestAdapter(Context context, List<Case> cases, String language) {
        this.mInflater = LayoutInflater.from(context);
        this.cases = cases;
        mClickListener = (OnItemClickListener) context;
        this.context = context;
        this.language = language;
    }

    @Override
    public TrackRequestAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.track_request_item, parent, false);
        return new TrackRequestAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TrackRequestAdapter.ViewHolder holder, int position) {

        Case userCase = cases.get(position);
        String name = userCase.getRequestName();
        if (name.equalsIgnoreCase("transfers")) {
            holder.headline.setText(context.getString(R.string.family_transfer_form));
        } else if (name.equalsIgnoreCase("cash dividends request form")) {
            holder.headline.setText(context.getString(R.string.cash_dividend_form));
        } else if (name.equalsIgnoreCase("iVESTOR Card Request Form")) {
            holder.headline.setText(context.getString(R.string.iVESTOR_card_form));
        }
        holder.trackingNumber.setText(userCase.getTrackingNumber());
        holder.date.setText(userCase.getRequestDate());
        if (language.equalsIgnoreCase("ar")) {
            holder.sideArrow.setImageResource(R.drawable.ic_side_arrow_rtl);
        }
        if (position % 2 == 1) {
            holder.itemView.setBackgroundColor(Color.WHITE);
        } else {
            holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.row_one));
        }
    }

    public void updateList(List<Case> list) {
        cases = list;
        notifyDataSetChanged();
    }

    public Case getItem(int position) {
        return cases.get(position);
    }

    @Override
    public int getItemCount() {
        return cases.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        HelveticaNeueTextView headline;
        HelveticaNeueBoldTextView trackingNumber, date;
        ImageView sideArrow;

        ViewHolder(View itemView) {
            super(itemView);
            headline = itemView.findViewById(R.id.headline);
            trackingNumber = itemView.findViewById(R.id.trackingNumber);
            date = itemView.findViewById(R.id.date);
            sideArrow = itemView.findViewById(R.id.sideArrow);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onItemClick(getAdapterPosition());
        }
    }
}
