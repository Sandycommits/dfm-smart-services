package com.DFM.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.DFM.R;
import com.DFM.fonts.HelveticaNeueBoldTextView;
import com.DFM.fonts.HelveticaNeueTextView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by Sandeep on 25/10/17.
 */

public class CustomBubbleAdapter implements GoogleMap.InfoWindowAdapter {

    private Context context;
    private String title, web;
    private LayoutInflater inflater;

    public CustomBubbleAdapter(Context context, String title, String web) {
        this.context = context;
        this.title = title;
        this.web = web;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        View v = inflater.inflate(R.layout.info_window, null);
        if (marker != null) {
            HelveticaNeueBoldTextView bubbleTitle = v.findViewById(R.id.bubbleTitle);
            bubbleTitle.setText(title);
            HelveticaNeueTextView bubbleWeb = v.findViewById(R.id.bubbleWeb);
            bubbleWeb.setText(web);
        }
        return (v);
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }
}
