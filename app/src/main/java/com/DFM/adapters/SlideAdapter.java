package com.DFM.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.DFM.R;
import com.DFM.fonts.HelveticaNeueTextView;
import com.DFM.interfaces.OnItemClickListener;

/**
 * Created by Sandeep on 23/10/17.
 */

public class SlideAdapter extends RecyclerView.Adapter<SlideAdapter.ViewHolder> {

    private String[] menu;
    private int[] drawables = {R.drawable.pdf_1_en, R.drawable.pdf_2_en, R.drawable.pdf_3_en, R.drawable.pdf_4_en, R.drawable.pdf_5_en, R.drawable.pdf_8_en, R.drawable.pdf_9_ar, R.drawable.pdf_6_en, R.drawable.pdf_10, R.drawable.pdf_7_en, R.drawable.pdf_11_en};
    private int[] drawablesAR = {R.drawable.pdf_1_ar, R.drawable.pdf_2_ar, R.drawable.pdf_3_ar, R.drawable.pdf_4_ar, R.drawable.pdf_5_ar, R.drawable.pdf_8_ar, R.drawable.pdf_9_ar, R.drawable.pdf_6_ar, R.drawable.pdf_10_ar, R.drawable.pdf_7_ar, R.drawable.pdf_11_en};
    private OnItemClickListener listener;
    private String language;

    public SlideAdapter(OnItemClickListener listener, String[] menu, String language) {
        this.menu = menu;
        this.listener = listener;
        this.language = language;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_pdf_card, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        /*Glide.with(holder.itemView.getContext())
                .load(data.get(position).getImage())
                .into(holder.image);*/

        if (language.equalsIgnoreCase("ar")) {
            holder.image.setBackgroundResource(drawablesAR[position]);
        } else {
            holder.image.setBackgroundResource(drawables[position]);
        }
        holder.title.setText(menu[position]);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return menu.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;
        private HelveticaNeueTextView title;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            title = (HelveticaNeueTextView) itemView.findViewById(R.id.title);
        }
    }
}
