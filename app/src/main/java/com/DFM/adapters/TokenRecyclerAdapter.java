package com.DFM.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.DFM.R;
import com.DFM.fonts.HelveticaNeueTextView;
import com.DFM.interfaces.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandeep on 09/11/17.
 */

public class TokenRecyclerAdapter extends RecyclerView.Adapter<TokenRecyclerAdapter.ViewHolder> {

    private List<String> categories = new ArrayList<>();
    private LayoutInflater mInflater;
    private OnItemClickListener mClickListener;
    private Context context;

    public TokenRecyclerAdapter(Context context, List<String> categories) {
        this.mInflater = LayoutInflater.from(context);
        this.categories = categories;
        mClickListener = (OnItemClickListener) context;
        this.context = context;

    }

    @Override
    public TokenRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.token_item, parent, false);
        return new TokenRecyclerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TokenRecyclerAdapter.ViewHolder holder, int position) {

        String category = categories.get(position);
        holder.tokenName.setText(category);
        if (position % 2 == 1) {
            holder.itemView.setBackgroundColor(Color.WHITE);
        } else {
            holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.row_one));
        }
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        HelveticaNeueTextView tokenName;

        ViewHolder(View itemView) {
            super(itemView);
            tokenName = itemView.findViewById(R.id.tokenName);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onItemClick(getAdapterPosition());
        }
    }
}
