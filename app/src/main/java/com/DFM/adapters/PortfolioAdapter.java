package com.DFM.adapters;

import android.content.Context;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.DFM.R;
import com.DFM.dto.BrokerStatement;
import com.DFM.fonts.HelveticaNeueBoldTextView;
import com.DFM.fonts.HelveticaNeueTextView;
import com.DFM.utils.EnglishNumberToWords;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Sandeep on 06/12/17.
 */

public class PortfolioAdapter extends RecyclerView.Adapter<PortfolioAdapter.ViewHolder> implements TextToSpeech.OnInitListener {

    public TextToSpeech tts;
    DecimalFormat formatter;
    DecimalFormat valueFormatter;
    DecimalFormat speechFormatter;
    private List<BrokerStatement> companies = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;
    private int position = -1;

    public PortfolioAdapter(Context context, List<BrokerStatement> companies) {
        this.mInflater = LayoutInflater.from(context);
        this.companies = companies;
        this.context = context;

        NumberFormat nFormatter = NumberFormat.getNumberInstance(Locale.US);
        NumberFormat nValueFormatter = NumberFormat.getNumberInstance(Locale.US);
        NumberFormat nSpeechFormatter = NumberFormat.getNumberInstance(Locale.US);
        formatter = (DecimalFormat) nFormatter;
        formatter.applyPattern("#,###,###.##");
        valueFormatter = (DecimalFormat) nValueFormatter;
        valueFormatter.applyPattern("#,###,###.###");
        speechFormatter = (DecimalFormat) nSpeechFormatter;
        speechFormatter.applyPattern("#######.##");

        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);
        valueFormatter.setMaximumFractionDigits(3);
        valueFormatter.setMinimumFractionDigits(3);
        speechFormatter.setMaximumFractionDigits(2);
        speechFormatter.setMinimumFractionDigits(2);

        tts = new TextToSpeech(context, this);
    }

    @Override
    public PortfolioAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.portfolio_item, parent, false);
        return new PortfolioAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PortfolioAdapter.ViewHolder holder, int position) {

        BrokerStatement company = companies.get(position);
        double price = company.getPrice();
        long currentBalance = company.getCurrentBalance();
        holder.header.setText(company.getSymbol().trim() + " (" + valueFormatter.format(price) + ")");
        holder.value.setText(formatter.format(price * currentBalance));
        holder.playStop.setImageResource(R.drawable.ic_play);
    }

    @Override
    public int getItemCount() {
        return companies.size();
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            tts.setLanguage(Locale.US);
            tts.setPitch(0.8f);
            tts.setSpeechRate(0.75f);
            tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                @Override
                public void onDone(String utteranceId) {
                    notifyItemChanged(position);
                }

                @Override
                public void onError(String utteranceId) {
                }

                @Override
                public void onStart(String utteranceId) {
                }
            });
        }
    }

    public void playAll() {
        StringBuilder builder = new StringBuilder();
        for (BrokerStatement company : companies) {
            double price = company.getPrice();
            long currentBalance = company.getCurrentBalance();
            Double result = new Double(speechFormatter.format(price * currentBalance));
            int dollars = (int) Math.floor(result);
            int cent = (int) Math.floor((result - dollars) * 100.0f);
            String text = company.getSecurityNameEn() + EnglishNumberToWords.convert(dollars) + " point "
                    + EnglishNumberToWords.convert(cent);
            builder.append(text);
        }
        Bundle params = new Bundle();
        params.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "");
        tts.speak(builder.toString(), TextToSpeech.QUEUE_ADD, params, "");
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        HelveticaNeueTextView value;
        HelveticaNeueBoldTextView header;
        ImageView playStop;

        ViewHolder(View itemView) {
            super(itemView);
            header = itemView.findViewById(R.id.company);
            value = itemView.findViewById(R.id.value);
            playStop = itemView.findViewById(R.id.playStop);
            itemView.setOnClickListener(this);
            itemView.setTag(playStop);
        }

        @Override
        public void onClick(View view) {
            ImageView playStop = (ImageView) view.getTag();
            if (tts.isSpeaking()) {
                tts.stop();
            }
            if (position != getAdapterPosition())
                notifyItemChanged(position);
            position = getAdapterPosition();
            if (playStop.getDrawable().getConstantState() == ContextCompat.getDrawable(context, R.drawable.ic_play).getConstantState()) {

                if (tts != null) {
                    tts.stop();
                    playStop.setImageResource(R.drawable.ic_play);
                }
                BrokerStatement company = companies.get(position);
                double price = company.getPrice();
                long currentBalance = company.getCurrentBalance();
                Double result = new Double(speechFormatter.format(price * currentBalance));
                int dollars = (int) Math.floor(result);
                int cent = (int) Math.floor((result - dollars) * 100.0f);
                String text = company.getSecurityNameEn() + " " + EnglishNumberToWords.convert(dollars) + " point "
                        + EnglishNumberToWords.convertAfterDecimal(cent);
                tts.setLanguage(Locale.US);
                tts.setPitch(0.8f);
                tts.setSpeechRate(0.75f);

                Bundle params = new Bundle();
                params.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "");
                tts.speak(text, TextToSpeech.QUEUE_ADD, params, "");
                playStop.setImageResource(R.drawable.ic_stop);
            } else {
                playStop.setImageResource(R.drawable.ic_play);
            }
        }
    }
}
