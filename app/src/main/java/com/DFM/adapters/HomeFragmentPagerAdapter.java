package com.DFM.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.DFM.dto.Icon;
import com.DFM.fragments.GridFragment;

import java.util.List;

/**
 * Created by Sandeep on 17/10/17.
 */

public class HomeFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context context;
    private List<Icon> iconList;
    private String language;

    public HomeFragmentPagerAdapter(FragmentManager fm, Context context, List<Icon> iconList, String language) {
        super(fm);
        this.context = context;
        this.iconList = iconList;
        this.language = language;
    }

    /**
     * It is a callback method used to display the fragments depending on position.
     *
     * @param position
     * @return
     */
    @Override
    public Fragment getItem(int position) {
        return GridFragment.newInstance(position + 1, language);
        /*switch (position) {
            case 0:
                return GridFragment.newInstance("", "");
            case 1:
                return GridFragment.newInstance("", "");
            case 2:
                return GridFragment.newInstance("", "");
            default:
                return null;
        }*/
    }

    /**
     * This is a callback method in FragmentPagerAdapter to return the number of pager items.
     *
     * @return
     */
    @Override
    public int getCount() {
        int size = iconList.size();
        float result = (float) size / (float) 6;
        int count = (int) Math.ceil(result);
        return count;
    }
}
