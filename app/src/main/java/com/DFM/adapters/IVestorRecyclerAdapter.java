package com.DFM.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.DFM.R;
import com.DFM.dto.IVestorRecord;
import com.DFM.fonts.HelveticaNeueTextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Sandeep on 21/11/17.
 */

public class IVestorRecyclerAdapter extends RecyclerView.Adapter<IVestorRecyclerAdapter.ViewHolder> {

    DecimalFormat formatter;
    private List<IVestorRecord> records = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;

    public IVestorRecyclerAdapter(Context context, List<IVestorRecord> records) {
        this.mInflater = LayoutInflater.from(context);
        this.records = records;
        NumberFormat nFormatter = NumberFormat.getNumberInstance(Locale.US);
        formatter = (DecimalFormat) nFormatter;
        formatter.applyPattern("#,###,###.##");
        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);
        this.context = context;
    }

    @Override
    public IVestorRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.ivestor_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(IVestorRecyclerAdapter.ViewHolder holder, int position) {

        IVestorRecord record = records.get(position);
        holder.valueDate.setText(record.getDate().split("T")[0]);
        holder.description.setText(record.getDescription());
        holder.credit.setText(formatter.format(record.getCredit()));
        holder.debit.setText(formatter.format(record.getDebit()));
        holder.balance.setText(formatter.format(record.getBalance()));
        if (position % 2 == 1) {
            holder.itemView.setBackgroundColor(Color.WHITE);
        } else {
            holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.row_one));
        }
    }

    @Override
    public int getItemCount() {
        return records.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        HelveticaNeueTextView valueDate, description, credit, debit, balance;

        ViewHolder(View itemView) {
            super(itemView);
            valueDate = itemView.findViewById(R.id.valueDate);
            description = itemView.findViewById(R.id.description);
            credit = itemView.findViewById(R.id.credit);
            debit = itemView.findViewById(R.id.debit);
            balance = itemView.findViewById(R.id.balance);
        }
    }
}
