package com.DFM.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.DFM.R;
import com.DFM.dto.Icon;
import com.DFM.fonts.HelveticaNeueTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandeep on 17/10/17.
 */

public class RecyclerGridAdapter extends RecyclerView.Adapter<RecyclerGridAdapter.ViewHolder> {

    private List<Icon> icons = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public RecyclerGridAdapter(Context context, List<Icon> icons) {
        this.mInflater = LayoutInflater.from(context);
        this.icons = icons;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.grid_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Icon icon = icons.get(position);
        holder.iconTitle.setText(icon.getTitle());
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return 6;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        HelveticaNeueTextView iconTitle;

        ViewHolder(View itemView) {
            super(itemView);
            iconTitle = (HelveticaNeueTextView) itemView.findViewById(R.id.iconTitle);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }
}
