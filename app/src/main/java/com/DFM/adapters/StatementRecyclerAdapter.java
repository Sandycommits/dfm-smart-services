package com.DFM.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.DFM.R;
import com.DFM.fonts.HelveticaNeueTextView;
import com.DFM.interfaces.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandeep on 14/11/17.
 */

public class StatementRecyclerAdapter extends RecyclerView.Adapter<StatementRecyclerAdapter.ViewHolder> {

    private List<String> categories = new ArrayList<>();
    private LayoutInflater mInflater;
    private OnItemClickListener mClickListener;
    private int[] drawables;
    private String language;

    public StatementRecyclerAdapter(Context context, List<String> categories, String language, int[] drawables) {
        this.mInflater = LayoutInflater.from(context);
        this.categories = categories;
        mClickListener = (OnItemClickListener) context;
        this.language = language;
        this.drawables = drawables;
    }

    @Override
    public StatementRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.statement_item, parent, false);
        return new StatementRecyclerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StatementRecyclerAdapter.ViewHolder holder, int position) {

        String category = categories.get(position);
        holder.title.setText(category);
        holder.statementSymbol.setBackgroundResource(drawables[position]);
        if (language.equalsIgnoreCase("ar")) {
            holder.sideArrow.setImageResource(R.drawable.ic_side_arrow_rtl);
        }
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        HelveticaNeueTextView title;
        ImageView statementSymbol;
        ImageView sideArrow;

        ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            statementSymbol = itemView.findViewById(R.id.statementSymbol);
            sideArrow = itemView.findViewById(R.id.sideArrow);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onItemClick(getAdapterPosition());
        }
    }
}
