package com.DFM.adapters;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.DFM.R;
import com.DFM.dto.Icon;
import com.DFM.interfaces.OnGridItemTouchListener;
import com.DFM.utils.CommonMethods;

import java.util.List;

/**
 * Created by Sandeep on 18/10/17.
 */

public class GridAdapter extends ArrayAdapter<Icon> {

    private int layoutId;
    private Context context;
    private OnGridItemTouchListener listener;
    private String language;

    public GridAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Icon> objects, OnGridItemTouchListener listener) {
        super(context, resource, objects);
        this.layoutId = resource;
        this.context = context;
        this.listener = listener;
        Configuration config = context.getResources().getConfiguration();
        language = config.locale.getLanguage();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        GridAdapter.GridViewHolder gridViewHolder = null;
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layoutId, null);
            gridViewHolder = new GridAdapter.GridViewHolder();
            gridViewHolder.title = convertView.findViewById(R.id.iconTitle);
            gridViewHolder.icon = convertView.findViewById(R.id.homeIcon);
            convertView.setTag(gridViewHolder);
        } else {
            gridViewHolder = (GridAdapter.GridViewHolder) convertView.getTag();
        }

        final Icon icon = getItem(position);
        if (!language.equalsIgnoreCase("ar")) {
            gridViewHolder.title.setText(icon.getTitle().trim());
        } else {
            gridViewHolder.title.setText(icon.getTitleAR().trim());
        }
        Resources resources = context.getResources();
        String name = icon.getIcon().split("\\.")[0];
        final int resourceId = resources.getIdentifier(name, "drawable", context.getPackageName());
        CommonMethods.getLog(name);
        gridViewHolder.icon.setImageDrawable(ContextCompat.getDrawable(context, resourceId));
        if (icon.getEnabled().equalsIgnoreCase("NO")) {
            convertView.setAlpha(0.25f);
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onGridItemTouch(icon);
            }
        });
//        gridViewHolder.menuIcon.setImageResource(menu.getIcon());
        return convertView;

    }

    private class GridViewHolder {

        TextView title;
        AppCompatImageView icon;
    }
}
