package com.DFM.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.DFM.R;
import com.DFM.dto.Disclosure;
import com.DFM.fonts.HelveticaNeueBoldTextView;
import com.DFM.fonts.HelveticaNeueTextView;
import com.DFM.interfaces.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandeep on 31/10/17.
 */

public class DisclosureRecyclerAdapter extends RecyclerView.Adapter<DisclosureRecyclerAdapter.ViewHolder> {

    private List<Disclosure> disclosures = new ArrayList<>();
    private LayoutInflater mInflater;
    private OnItemClickListener mClickListener;
    private Context context;
    private String language;

    public DisclosureRecyclerAdapter(Context context, List<Disclosure> disclosures, String language) {
        this.mInflater = LayoutInflater.from(context);
        this.disclosures = disclosures;
        mClickListener = (OnItemClickListener) context;
        this.context = context;
        this.language = language;
    }

    @Override
    public DisclosureRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.disclosure_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DisclosureRecyclerAdapter.ViewHolder holder, int position) {

        Disclosure broker = disclosures.get(position);
        holder.headline.setText(broker.getHeadline());
        holder.issuer.setText(broker.getIssuer());
        holder.date.setText(broker.getDate());
        if (language.equalsIgnoreCase("ar")) {
            holder.sideArrow.setImageResource(R.drawable.ic_side_arrow_rtl);
        }
        if (position % 2 == 1) {
            holder.itemView.setBackgroundColor(Color.WHITE);
        } else {
            holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.row_one));
        }
    }

    public void updateList(List<Disclosure> list) {
        disclosures = list;
        notifyDataSetChanged();
    }

    public Disclosure getItem(int position) {
        return disclosures.get(position);
    }

    @Override
    public int getItemCount() {
        return disclosures.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        HelveticaNeueTextView headline;
        HelveticaNeueBoldTextView issuer, date;
        ImageView sideArrow;

        ViewHolder(View itemView) {
            super(itemView);
            headline = itemView.findViewById(R.id.headline);
            issuer = itemView.findViewById(R.id.issuer);
            date = itemView.findViewById(R.id.date);
            sideArrow = itemView.findViewById(R.id.sideArrow);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onItemClick(getAdapterPosition());
        }
    }
}
