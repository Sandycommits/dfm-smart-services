package com.DFM.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.DFM.R;
import com.DFM.dto.MovementRecord;
import com.DFM.fonts.HelveticaNeueTextView;
import com.DFM.interfaces.OnItemClickListener;
import com.DFM.utils.CommonMethods;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandeep on 27/11/17.
 */

public class MovementRecyclerAdapter extends RecyclerView.Adapter<MovementRecyclerAdapter.ViewHolder> {

    private List<MovementRecord> records = new ArrayList<>();
    private LayoutInflater mInflater;
    private OnItemClickListener mClickListener;
    private Context context;

    public MovementRecyclerAdapter(Context context, List<MovementRecord> records) {
        this.mInflater = LayoutInflater.from(context);
        this.records = records;
        mClickListener = (OnItemClickListener) context;
        this.context = context;
    }

    @Override
    public MovementRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.movement_item, parent, false);
        return new MovementRecyclerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovementRecyclerAdapter.ViewHolder holder, int position) {

        MovementRecord record = records.get(position);
        holder.date.setText(CommonMethods.getDateInFormat(record.getDate()));
        holder.fromMember.setText(record.getFromMember());
        holder.fromNIN.setText(record.getFromNIN());
        holder.fromAccount.setText(record.getFromAccount());
        holder.toMember.setText(record.getToMember());
        holder.toNIN.setText(record.getToNIN());
        holder.toAccount.setText(record.getToAccount());
        if (position % 2 == 1) {
            holder.itemView.setBackgroundColor(Color.WHITE);
        } else {
            holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.row_one));
        }
    }

    @Override
    public int getItemCount() {
        return records.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        HelveticaNeueTextView date, fromMember, toMember, fromNIN, toNIN, fromAccount, toAccount;

        ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            fromMember = itemView.findViewById(R.id.fromMember);
            toMember = itemView.findViewById(R.id.toMember);
            fromNIN = itemView.findViewById(R.id.fromNIN);
            toNIN = itemView.findViewById(R.id.toNIN);
            fromAccount = itemView.findViewById(R.id.fromAccount);
            toAccount = itemView.findViewById(R.id.toAccount);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onItemClick(getAdapterPosition());
        }
    }


}
