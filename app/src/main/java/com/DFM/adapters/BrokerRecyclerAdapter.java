package com.DFM.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.DFM.R;
import com.DFM.dto.Broker;
import com.DFM.fonts.HelveticaNeueTextView;
import com.DFM.fragments.BrokerDialogFragment;
import com.DFM.interfaces.OnBrokerSelectionListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandeep on 26/10/17.
 */

public class BrokerRecyclerAdapter extends RecyclerView.Adapter<BrokerRecyclerAdapter.ViewHolder> {

    private List<Broker> brokerList = new ArrayList<>();
    private LayoutInflater mInflater;
    private OnBrokerSelectionListener mClickListener;
    private BrokerDialogFragment dialogFragment;

    public BrokerRecyclerAdapter(Context context, List<Broker> brokerList, BrokerDialogFragment dialogFragment) {
        this.mInflater = LayoutInflater.from(context);
        this.brokerList = brokerList;
        mClickListener = (OnBrokerSelectionListener) context;
        this.dialogFragment = dialogFragment;
    }

    @Override
    public BrokerRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.broker_search_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BrokerRecyclerAdapter.ViewHolder holder, int position) {

        Broker broker = brokerList.get(position);
        holder.title.setText(broker.getBroker_name_en() + " - " + broker.getBroker_name_ar());
    }

    @Override
    public int getItemCount() {
        return brokerList.size();
    }

    public void updateList(List<Broker> brokerList) {
        this.brokerList = brokerList;
        notifyDataSetChanged();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        HelveticaNeueTextView title;

        ViewHolder(View itemView) {
            super(itemView);
            title = (HelveticaNeueTextView) itemView.findViewById(R.id.title);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onBrokerSelected(brokerList.get(getAdapterPosition()));
            dialogFragment.dismiss();
        }
    }
}
