package com.DFM.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.DFM.AboutActivity;
import com.DFM.DisclosuresActivity;
import com.DFM.EFormsActivity;
import com.DFM.FeedbackActivity;
import com.DFM.GuidesActivity;
import com.DFM.HowToTradeActivity;
import com.DFM.MyStatementActivity;
import com.DFM.PortfolioActivity;
import com.DFM.R;
import com.DFM.StockQuotesActivity;
import com.DFM.TokenDetailsActivity;
import com.DFM.TokenNumberActivity;
import com.DFM.TrackRequestActivity;
import com.DFM.adapters.GridAdapter;
import com.DFM.dto.Data;
import com.DFM.dto.Icon;
import com.DFM.dto.Login;
import com.DFM.dto.NIN;
import com.DFM.dto.Ticket;
import com.DFM.dto.UserCode;
import com.DFM.interfaces.OnGridItemTouchListener;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.Constants;
import com.DFM.utils.PreferenceConnector;
import com.DFM.utils.ProgressDialog;
import com.Wsdl2Code.WebServices.Authenticate.Authenticate;
import com.Wsdl2Code.WebServices.MiscFunctions.IWsdl2CodeEvents;
import com.Wsdl2Code.WebServices.MiscFunctions.MiscFunctions;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.mapper.MapperWrapper;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GridFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GridFragment extends Fragment implements OnGridItemTouchListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ProgressDialog progressDialog;

    // TODO: Rename and change types of parameters
    private int mParam1;
    private String mParam2;

    public GridFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param position Parameter 1.
     * @return A new instance of fragment GridFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GridFragment newInstance(int position, String language) {
        GridFragment fragment = new GridFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, position);
        args.putString(ARG_PARAM2, language);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_grid, container, false);
        return inflater.inflate(R.layout.fragment_grid, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressDialog = ProgressDialog.getInstance();
        Data data = (Data) CommonMethods.retrieveObject(getActivity(), PreferenceConnector.ICONS, new Data());
        List<Icon> iconList = data.getIcons();
        List<Icon> tempList = getFirstSix(iconList);
        GridView gridView = (GridView) view.findViewById(R.id.gridView);
        GridAdapter adapter = new GridAdapter(getActivity(), R.layout.grid_item, tempList, this);
//        iconList = null;
        gridView.setAdapter(adapter);
        adapter = null;
    }

    private List<Icon> getFirstSix(List<Icon> iconsList) {
        List<Icon> tempList = new ArrayList<>();
        /*for (int i = 0; i < 6; i++) {
            tempList.add(iconsList.get(i));
        }*/
        int start = 6 * (mParam1 - 1);
        int end = 6 * mParam1;
//        if (iconsList.size() >= end)
        int size = iconsList.size();
        if (size > end) {
            tempList = iconsList.subList(start, end);
        } else {
            tempList = iconsList.subList(start, size);
        }
        return tempList;
    }

    @Override
    public void onGridItemTouch(Icon icon) {
        navigateScreen(icon);
    }

    private void navigateScreen(Icon icon) {

        if (icon.getEnabled().equalsIgnoreCase("YES")) {
            String title = icon.getTitle().toLowerCase().trim();
            Intent intent;
            switch (title) {
                case Constants.ITEM_1:
                    intent = new Intent(getActivity(), AboutActivity.class);
                    startActivity(intent);
                    break;
                case Constants.ITEM_2:
                    redirectURL(1);
                    break;
                case Constants.ITEM_3:
                    intent = new Intent(getActivity(), StockQuotesActivity.class);
                    startActivity(intent);
                    break;
                case Constants.ITEM_4:
                    intent = new Intent(getActivity(), HowToTradeActivity.class);
                    startActivity(intent);
                    break;
                case Constants.ITEM_5:
                    intent = new Intent(getActivity(), GuidesActivity.class);
                    startActivity(intent);
                    break;
                case Constants.ITEM_6:
                    if (CommonMethods.isOnline(getActivity())) {
                        intent = new Intent(getActivity(), DisclosuresActivity.class);
                        startActivity(intent);
                    } else {
                        CommonMethods.displayOKDialogue(getActivity(), getString(R.string.no_internet));
                    }
                    break;
                case Constants.ITEM_7:
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.MARKET_APP));
                    startActivity(intent);
                    break;
                case Constants.ITEM_8:
                    intent = new Intent(getActivity(), FeedbackActivity.class);
                    startActivity(intent);
                    break;
                case Constants.ITEM_9:
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.IR_APP));
                    startActivity(intent);
                    break;
                case Constants.ITEM_10:
                    intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(Constants.SERVICE_CATALOG));
                    startActivity(intent);
                    break;
                case Constants.ITEM_11:
                    redirectURL(2);
                    break;
                case Constants.ITEM_12:
                    redirectURL(4);
                    break;
                case Constants.ITEM_13:
                    if (CommonMethods.isOnline(getActivity())) {
                        intent = new Intent(getActivity(), MyStatementActivity.class);
                        startActivity(intent);
                    } else {
                        CommonMethods.displayOKDialogue(getActivity(), getString(R.string.no_internet));
                    }
                    break;
                case Constants.ITEM_14:
                    intent = new Intent(getActivity(), EFormsActivity.class);
                    startActivity(intent);
                    break;
                case Constants.ITEM_15:
                    if (CommonMethods.isOnline(getActivity())) {
                        intent = new Intent(getActivity(), TrackRequestActivity.class);
                        startActivity(intent);
                    } else {
                        CommonMethods.displayOKDialogue(getActivity(), getString(R.string.no_internet));
                    }
                    break;
                case Constants.ITEM_16:
                    if (CommonMethods.isOnline(getActivity())) {
                        issueToken();
                    } else {
                        CommonMethods.displayOKDialogue(getActivity(), getString(R.string.no_internet));
                    }
                    break;
                case Constants.ITEM_17:
                    if (CommonMethods.isOnline(getActivity())) {
                        intent = new Intent(getActivity(), PortfolioActivity.class);
                        startActivity(intent);
                    } else {
                        CommonMethods.displayOKDialogue(getActivity(), getString(R.string.no_internet));
                    }
                    break;
                case Constants.ITEM_18:
                    redirectURL(3);
                    break;
                default:
                    break;
            }
        } else {
            if (mParam2.equalsIgnoreCase("en")) {
                CommonMethods.displayOKDialogue(getActivity(), icon.getDisableMsgEN());
            } else {
                CommonMethods.displayOKDialogue(getActivity(), icon.getDisableMsgAR());
            }
        }


    }

    private void redirectURL(final int type) {

        if (CommonMethods.isOnline(getActivity())) {
            Authenticate authenticate = new Authenticate(new com.Wsdl2Code.WebServices.Authenticate.IWsdl2CodeEvents() {
                @Override
                public void Wsdl2CodeStartedRequest() {

                }

                @Override
                public void Wsdl2CodeFinished(String methodName, Object data) {
                    progressDialog.dismissProgressDialog();
                    String response = data.toString();
                    if (type == 1) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(response + "&redirect=http://marketwatch.dfm.ae"));
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(response));
                        startActivity(intent);
                    }
                }

                @Override
                public void Wsdl2CodeFinishedWithException(Exception ex) {
                    progressDialog.dismissProgressDialog();
                }

                @Override
                public void Wsdl2CodeEndedRequest() {

                }
            });
            progressDialog.displayProgressDialog(getActivity());
            Login login = (Login) CommonMethods.retrieveObject(getActivity(), PreferenceConnector.LOGIN, new Login());
            String session = login.getData().getSession();
            String request = login.getUserName() + "," + CommonMethods.getCurrentDateURL() + ",DFM-SMartApp," + session;
            try {
                authenticate.GetEncryptedStringNewAsync(login.getData().getSession(), "en", request, type);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            CommonMethods.displayOKDialogue(getActivity(), getString(R.string.no_internet));
        }
    }

    private void issueToken() {
        MiscFunctions miscFunctions = new MiscFunctions(new IWsdl2CodeEvents() {
            @Override
            public void Wsdl2CodeStartedRequest() {

            }

            @Override
            public void Wsdl2CodeFinished(String methodName, Object data) {
                progressDialog.dismissProgressDialog();
                XStream xStream = new XStream(new DomDriver()) {
                    // to enable ignoring of unknown elements
                    @Override
                    protected MapperWrapper wrapMapper(MapperWrapper next) {
                        return new MapperWrapper(next) {
                            @Override
                            public boolean shouldSerializeMember(Class definedIn, String fieldName) {
                                if (definedIn == Object.class) {
                                    try {
                                        return this.realClass(fieldName) != null;
                                    } catch (Exception e) {
                                        return false;
                                    }
                                }
                                return super.shouldSerializeMember(definedIn, fieldName);
                            }
                        };
                    }
                };
                xStream.processAnnotations(Login.class);
                Login login = (Login) xStream.fromXML(data.toString());
                if (login.getStatus().equalsIgnoreCase("valid")) {
                    Data response = login.getData();
                    if (response.getOfficeClosedEN() != null) {
                        if (mParam2.equalsIgnoreCase("ar")) {
                            CommonMethods.displayOKDialogue(getActivity(), response.getOfficeClosedAR());
                        } else {
                            CommonMethods.displayOKDialogue(getActivity(), response.getOfficeClosedEN());
                        }
                    } else {
                        Ticket ticket = login.getData().getTicket();
                        if (ticket != null) {
                            Intent intent = new Intent(getActivity(), TokenDetailsActivity.class);
                            intent.putExtra("ticket", ticket);
                            startActivity(intent);
                        } else {
                            ArrayList<String> categories = login.getData().getCategory().getCategories();
                            ArrayList<String> arCategories = login.getData().getCategory().getCategoriesAR();
                            ArrayList<String> symbols = login.getData().getCategory().getCategoryIdList();
                            Intent intent = new Intent(getActivity(), TokenNumberActivity.class);
                            if (mParam2.equalsIgnoreCase("ar")) {
                                intent.putStringArrayListExtra("categories", arCategories);
                            } else {
                                intent.putStringArrayListExtra("categories", categories);
                            }
                            intent.putStringArrayListExtra("symbols", symbols);
                            startActivity(intent);
                        }
                    }
                }
            }

            @Override
            public void Wsdl2CodeFinishedWithException(Exception ex) {
                progressDialog.dismissProgressDialog();
            }

            @Override
            public void Wsdl2CodeEndedRequest() {

            }
        });
        progressDialog.displayProgressDialog(getActivity());
        UserCode userCode = (UserCode) CommonMethods.retrieveObject(getActivity(), PreferenceConnector.USER_CODES, new UserCode());
        NIN root = CommonMethods.getRootNin(userCode);
        Login login = (Login) CommonMethods.retrieveObject(getActivity(), PreferenceConnector.LOGIN, new Login());
        try {
            miscFunctions.IssueTokenListAsync(root.getNIN(), login.getData().getLoginType());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
