package com.DFM.fragments;

import android.app.DialogFragment;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.DFM.R;
import com.DFM.adapters.BrokerRecyclerAdapter;
import com.DFM.dto.Broker;
import com.DFM.dto.BrokerData;
import com.DFM.fonts.HelveticaNeueEditText;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.PreferenceConnector;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link android.app.DialogFragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BrokerDialogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BrokerDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BrokerDialogFragment extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.searchEditor)
    HelveticaNeueEditText searchEditor;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView brokerRecycler;
    private OnFragmentInteractionListener mListener;
    private BrokerRecyclerAdapter adapter;
    private BrokerData brokerData;

    public BrokerDialogFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BrokerDialogFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BrokerDialogFragment newInstance(String param1, String param2) {
        BrokerDialogFragment fragment = new BrokerDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        View view = inflater.inflate(R.layout.fragment_broker_dialog, container, false);
        ButterKnife.bind(this, view);
        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(getResources().getDimensionPixelSize(R.dimen.dialogue_width), getResources().getDimensionPixelSize(R.dimen.dialogue_height));
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        searchEditor.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // filter your list from your input
                filter(s.toString());
                //you can use runnable postDelayed like 500 ms to delay search text
            }
        });

        brokerData = (BrokerData) CommonMethods.retrieveObject(getActivity(), PreferenceConnector.BROKER_DATA, new BrokerData());
        brokerRecycler = view.findViewById(R.id.brokersRecycler);
        adapter = new BrokerRecyclerAdapter(getActivity(), brokerData.getBrokers(), this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        brokerRecycler.setHasFixedSize(true);
        brokerRecycler.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        brokerRecycler.setLayoutManager(layoutManager);
        brokerRecycler.setAdapter(adapter);
    }

    void filter(String text) {
        List<Broker> temp = new ArrayList();
        for (Broker broker : brokerData.getBrokers()) {
            String combined = broker.getBroker_name_en().toLowerCase() + broker.getBroker_name_ar().toLowerCase();
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (combined.contains(text.toLowerCase())) {
                temp.add(broker);
            }
        }
        //update recyclerview
        adapter.updateList(temp);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
