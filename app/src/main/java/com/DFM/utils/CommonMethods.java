package com.DFM.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Base64;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.DFM.MyApplication;
import com.DFM.R;
import com.DFM.dto.BrokerData;
import com.DFM.dto.Company;
import com.DFM.dto.Data;
import com.DFM.dto.Login;
import com.DFM.dto.NIN;
import com.DFM.dto.Table;
import com.DFM.dto.UserCode;
import com.DFM.fonts.HelveticaNeueButton;
import com.DFM.interfaces.DialogListener;
import com.DFM.interfaces.OnDateSelectionListener;
import com.DFM.interfaces.OnDialogItemClickListener;
import com.DFM.interfaces.OnItemClickListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.wx.wheelview.adapter.ArrayWheelAdapter;
import com.wx.wheelview.widget.WheelView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by Sandeep on 11/10/17.
 */

public class CommonMethods {

    private static final String DFM = "DFM";

    /**
     * Prints log statement
     *
     * @param msg
     */
    public static void getLog(String msg) {
        Log.i(DFM, msg);
    }

    /**
     * displays the toast message
     *
     * @param ctx
     * @param msg
     */
    public static void displayToast(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
    }

    public static void displaySnackBar(Activity ctx, String message) {
        Snackbar snackbar = Snackbar
                .make(ctx.getWindow().getDecorView().getRootView(), message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    /**
     * Check edit text fields are empty or not
     *
     * @param fields
     * @return
     */
    public static boolean validateEditTextFields(EditText[] fields) {
        for (EditText currentField : fields) {
            if (currentField.getText().toString().length() <= 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check edit text fields are empty or not and returns string
     *
     * @param fields
     * @return
     */
    public static String validateTextFields(EditText[] fields) {
        for (EditText currentField : fields) {
            if (currentField.getText().toString().length() <= 0) {
                return currentField.getHint().toString();
            }
        }
        return "";
    }

    /**
     * Stores the hash map in shared preferences
     */
    public static void storeMapObject(Context ctx, HashMap<String, ArrayList<Company>> hashMap, String key) {

        Type type = new TypeToken<HashMap<String, ArrayList<Company>>>() {
        }.getType();
        Gson gson = new GsonBuilder().enableComplexMapKeySerialization()
                .setPrettyPrinting().create();
        String json = gson.toJson(hashMap, type);
        PreferenceConnector.writeString(ctx, key, json);
        gson = null;
        json = null;
    }

    /**
     * Retrieves the hash map object from shared preferences
     *
     * @return
     */
    public static HashMap<String, ArrayList<Company>> retrieveMapObject(Context ctx, String key) {
        HashMap<String, ArrayList<Company>> object = null;
        try {
            Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
            Type type = new TypeToken<HashMap<String, ArrayList<Company>>>() {
            }.getType();
            String json = PreferenceConnector.readString(ctx, key, "");
            object = gson.fromJson(json, type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return object;
    }

    /**
     * Stores the object in shared preferences
     */
    public static void storeObject(Context ctx, Object object, String key) {
        Gson gson = new Gson();
        String json = gson.toJson(object);
        PreferenceConnector.writeString(ctx, key, json);
        gson = null;
        json = null;
    }

    /**
     * Retrieves the json object from shared preferences
     *
     * @return
     */
    public static Object retrieveObject(Context ctx, String key, Object obj) {
        Object object = null;
        try {
            Gson gson = new Gson();
            String json = PreferenceConnector.readString(ctx, key, "");
            if (obj instanceof Login) {
                object = gson.fromJson(json, Login.class);
            } else if (obj instanceof Data) {
                object = gson.fromJson(json, Data.class);
            } else if (obj instanceof BrokerData) {
                object = gson.fromJson(json, BrokerData.class);
            } else if (obj instanceof Table) {
                object = gson.fromJson(json, Table.class);
            } else if (obj instanceof UserCode) {
                object = gson.fromJson(json, UserCode.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return object;
    }

    /*
    * Display Sample dialogue with "OK" button
    * */
    public static void displayOKDialogue(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /*
    * Display Sample dialogue with "OK" button
    * */
    public static void displayConfirmationDialogue(Context context, String message, final DialogListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        listener.onConfirmation(true);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static Bitmap getBitmap(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (drawable instanceof BitmapDrawable) {
            return BitmapFactory.decodeResource(context.getResources(), drawableId);
        } else if (drawable instanceof VectorDrawable) {
            return getBitmap((VectorDrawable) drawable);
        } else {
            throw new IllegalArgumentException("unsupported drawable type");
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static Bitmap getBitmap(VectorDrawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }

    /**
     * Converts the DMS (Degress, minutes and seconds value to Decimal for latitude and longitude)
     */

    public static double getDecimalValue(String input) {
        String[] values = input.split(" ");
        int hour = Integer.parseInt(values[0]);
        double minutes = Double.parseDouble(values[1]) / 60;
        double seconds = Double.parseDouble(values[2]) / 3600;
        //Formula
        double result = hour + minutes + seconds;
        return result;
    }

    public static NIN getRootNin(UserCode userCode) {
        NIN temp = new NIN();
        if (userCode != null) {
            ArrayList<NIN> nins = userCode.getNinList();
            for (NIN nin : nins) {
                if (nin.getIsRoot().equals("1")) {
                    return nin;
                }
            }
        }
        temp.setNIN("");
        return temp;
    }

    /**
     * Displays the date picker dialog
     *
     * @param ctx
     * @param listener
     */
    public static void displayDatePickerDialog(Context ctx, final OnDateSelectionListener listener, boolean isMax) {
        int mYear, mMonth, mDay;
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(ctx, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear = monthOfYear + 1;
                String month = monthOfYear + "";
                String day = dayOfMonth + "";

                if (month.length() == 1) {
                    month = "0" + month;
                }

                if (day.length() == 1) {
                    day = "0" + day;
                }
                listener.onDateSelected(day
                        + "-" + month + "-" + year);
            }
        }, mYear, mMonth, mDay);
        if (isMax)
            datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        datePickerDialog.show();
    }

    public static String getCurrentTimeOnly() {
        SimpleDateFormat df = new SimpleDateFormat("hh:mm aa", Locale.US);
        String currentTime = df.format(new Date());
        return currentTime;
    }

    public static String getCurrentDateDisplay() {
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        String currentTime = df.format(new Date());
        return currentTime;
    }

    public static String getCurrentDateIssueToken() {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss aa", Locale.US);
        String currentTime = df.format(new Date());
        return currentTime;
    }

    public static String getCurrentDateOnly() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        String currentTime = df.format(new Date());
        return currentTime;
    }

    public static String getCurrentDateURL() {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        String currentTime = df.format(new Date());
        return currentTime;
    }

    public static String get20YearsDate() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.YEAR, -20);
        String currentTime = df.format(cal.getTime());
        return currentTime;
    }

    public static String getPreviousMonthDate() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, -1);
        String currentTime = df.format(cal.getTime());
        return currentTime;
    }

    public static String getPreviousMonthDateDisplay() {
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, -1);
        String currentTime = df.format(cal.getTime());
        return currentTime;
    }

    public static void showListDialog(Context context, String[] data, final OnDialogItemClickListener listener, int position) {
        final Dialog dialog = new Dialog(new ContextThemeWrapper(context, R.style.DialogSlideAnim));
        dialog.setContentView(R.layout.feedback_dialog);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT, LinearLayoutCompat.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setWindowAnimations(R.style.DialogAnimation);


        final WheelView wheelView = dialog.findViewById(R.id.dialogWheel);
        HelveticaNeueButton doneButton = dialog.findViewById(R.id.done);

//        String[] wheelArray = context.getResources().getStringArray(R.array.subjects);
        if (data != null) {
            List<String> mList = Arrays.asList(data);
            wheelView.setWheelAdapter(new ArrayWheelAdapter(context));
            wheelView.setSkin(WheelView.Skin.Holo);
            wheelView.setWheelData(mList);
            WheelView.WheelViewStyle style = new WheelView.WheelViewStyle();
            boolean isTablet = context.getResources().getBoolean(R.bool.isTablet);
            if (isTablet) {
                wheelView.setWheelSize(7);
            } else {
                wheelView.setWheelSize(5);
            }
            style.holoBorderColor = Color.LTGRAY;
            style.textColor = Color.LTGRAY;
            style.selectedTextColor = Color.BLACK;
            wheelView.setStyle(style);
            wheelView.setSelection(position);
            dialog.show();
            doneButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String selected = (String) wheelView.getSelectionItem();

//                subject.setText(selected);
                    listener.onDialogItem(selected);
                    dialog.dismiss();
                }
            });
        }
    }

    public static void showSymbolDialog(Context context, String[] data, final OnItemClickListener listener, int position) {
        final Dialog dialog = new Dialog(new ContextThemeWrapper(context, R.style.DialogSlideAnim));
        dialog.setContentView(R.layout.feedback_dialog);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT, LinearLayoutCompat.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setWindowAnimations(R.style.DialogAnimation);


        final WheelView wheelView = dialog.findViewById(R.id.dialogWheel);
        HelveticaNeueButton doneButton = dialog.findViewById(R.id.done);

//        String[] wheelArray = context.getResources().getStringArray(R.array.subjects);
        List<String> mList = Arrays.asList(data);
        wheelView.setWheelSize(5);
        wheelView.setWheelAdapter(new ArrayWheelAdapter(context));
        wheelView.setSkin(WheelView.Skin.Holo);
        wheelView.setWheelData(mList);
        WheelView.WheelViewStyle style = new WheelView.WheelViewStyle();
        style.selectedTextSize = 20;
        style.textSize = 16;
        style.holoBorderColor = Color.LTGRAY;
        style.textColor = Color.LTGRAY;
        style.selectedTextColor = Color.BLACK;
        wheelView.setStyle(style);
        wheelView.setSelection(position);
        dialog.show();
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selected = wheelView.getCurrentPosition();

//                subject.setText(selected);
                listener.onItemClick(selected);
                dialog.dismiss();
            }
        });
    }

    /**
     * retrieve the String array from NIN array list
     *
     * @param ninList
     * @return
     */
    public static String[] getNinArray(ArrayList<NIN> ninList) {
        int size = ninList.size();
        String[] array = new String[size];
        for (int i = 0; i < size; i++) {
            NIN nin = ninList.get(i);
            array[i] = nin.getNIN() + "-" + nin.getAccountName();
        }
        return array;
    }

    public static String[] getBrokerArray(ArrayList<Company> companies, String language) {
        String[] array = new String[0];
        if (companies != null) {
            int size = companies.size() + 1;
            array = new String[size];
            array[0] = MyApplication.getContext().getResources().getString(R.string.all);
            for (int i = 1; i < size; i++) {
                Company company = companies.get(i - 1);
                String name = "";
                if (language.equalsIgnoreCase("ar")) {
                    String arabic = company.getNameAR();
                    if (arabic.contains("-")) {
                        name = arabic.split("-")[0].trim();
                    }
                    array[i] = company.getBroker() + " - " + (name.isEmpty() ? "N/A" : name);
                } else {
                    name = company.getNameEN();
                    array[i] = name;
                }
            }
        }
        return array;
    }

    public static String[] getCompanyArray(ArrayList<Company> companies, String language) {
        String[] array = new String[0];
        if (companies != null) {
            int size = companies.size() + 1;
            array = new String[size];
            array[0] = MyApplication.getContext().getResources().getString(R.string.all);
            for (int i = 1; i < size; i++) {
                Company company = companies.get(i - 1);
                String name = language.equalsIgnoreCase("ar") ? company.getNameAR().trim() : company.getNameEN();
                array[i] = company.getSymbol() + " - " + (name.isEmpty() ? "N/A" : name);
            }
        }
        return array;
    }

    public static String getInitialLiteral(String input) {
        String nin = "";
        if (input.contains("-")) {
            String[] literals = input.split("-");
            int size = literals.length;
            if (size > 2) {
                nin = literals[0] + "-" + literals[1];
            } else {
                nin = literals[0].trim();
            }
        }
        return nin;
    }

    public static String getDateInFormat(String date) {
        String result = "";
        if (date.length() == 8)
            result = new StringBuilder(date).insert(4, "-").insert(7, "-").toString();
        return result;
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Converts bitmap to base64
     *
     * @param bitmap
     * @return
     */
    public static String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public static String getRequiredDateFormat(String startDate) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US);
        Date date = null;
        String str = "";
        try {
            date = inputFormat.parse(startDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String getResolutionDateFormat(String startDate) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        Date date = null;
        String str = "";
        try {
            date = inputFormat.parse(startDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String getIVestorDateFormat(String startDate) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date date = null;
        String str = "";
        try {
            date = inputFormat.parse(startDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String getAppDateFormat(String startDate) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Date date = null;
        String str = "";
        try {
            date = inputFormat.parse(startDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String getServiceDateFormat(String startDate) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date date = null;
        String str = "";
        try {
            date = inputFormat.parse(startDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static Bitmap getRotatedBitMap(Bitmap bitmap, String filePath) {
        ExifInterface ei = null;
        Bitmap rotatedBitmap = null;
        try {
            ei = new ExifInterface(filePath);

            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            switch (orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(bitmap, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(bitmap, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(bitmap, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = bitmap;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rotatedBitmap;
    }

    public static void hideKeyBoard(Activity activity) {
        // Check if no view has focus:
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPx(Context context, float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    public static Bitmap getScreenShot(View screenView) {
        screenView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
        screenView.setDrawingCacheEnabled(false);
        return bitmap;
    }

    public static void store(Bitmap bm, String fileName, Context context) {
        final String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Screenshots";
        File dir = new File(dirPath);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dirPath, fileName);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Uri uri = Uri.fromFile(file);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/*");

        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, "");
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        try {
            context.startActivity(Intent.createChooser(intent, "Share Screenshot"));
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "No App Available", Toast.LENGTH_SHORT).show();
        }
    }

    public static void shareImage(File file, Context context) {
        Uri uri = Uri.fromFile(file);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/*");

        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, "");
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        try {
            context.startActivity(Intent.createChooser(intent, "Share Screenshot"));
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "No App Available", Toast.LENGTH_SHORT).show();
        }
    }

    /*
    * Checks whether internet connection is available or not
    * */
    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static int getStringPosition(String[] strings, String selected) {
        if (strings != null) {
            for (int i = 0; i < strings.length; i++) {
                if (strings[i].equalsIgnoreCase(selected)) {
                    return i;
                }
            }
        }
        return 0;
    }
}
