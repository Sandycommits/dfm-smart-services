package com.DFM.utils;

/**
 * Created by Sandeep on 15/10/17.
 */

public class Constants {

    //    UAT
//    public static final String BASE_URL = "http://esrvuat.dfm.ae:8010/Auth/WS/";
//    public static final String EPAY_URL = "http://esrvuat.dfm.ae:8010/WebApplication.UI/RemoteOperations/Payment/PaymentTypeSelectionAnonymous.aspx?";

//    Production
public static final String BASE_URL = "https://auth2.dfm.ae/misc2/ws/";
    public static final String EPAY_URL = "https://esrv.dfm.ae/WebApplication.UI/RemoteOperations/Payment/PaymentTypeSelectionAnonymous.aspx?";

    public static final String ACTIVATION_CODE = "tu76geji340t89u9";
    public static final int TIME_OUT = 30000;

    public static final String[] banners = {"http://ads.dfm.ae/banners/dfm/dmf2014/mobile%20app/banner1.png", "http://ads.dfm.ae/banners/dfm/dmf2014/mobile%20app/banner2.png", "http://ads.dfm.ae/banners/dfm/dmf2014/mobile%20app/banner3.png", "http://ads.dfm.ae/banners/dfm/dmf2014/mobile%20app/banner4.png"};

    /*Home screen pager icons*/
    public static final String ITEM_1 = "about dfm";
    public static final String ITEM_2 = "market watch - real-time";
    public static final String ITEM_3 = "stock quotes";
    public static final String ITEM_4 = "how to trade";
    public static final String ITEM_5 = "guides";
    public static final String ITEM_6 = "disclosures";


    public static final String ITEM_7 = "market watch app";
    public static final String ITEM_8 = "feedback";
    public static final String ITEM_9 = "dfm ir";
    public static final String ITEM_10 = "services catalogue";
    public static final String ITEM_11 = "eipo";
    public static final String ITEM_12 = "broker services";


    public static final String ITEM_13 = "e-reports";
    public static final String ITEM_14 = "e-forms";
    public static final String ITEM_15 = "dfm track request";
    public static final String ITEM_16 = "issue token number";
    public static final String ITEM_17 = "portfolio";
    public static final String ITEM_18 = "smart voting";

    /*Pdf documents*/
    public static final String pdf_1 = "http://www.dfm.ae/docs/default-source/guides/the-investor-handbook---products-and-services.pdf";
    public static final String pdf_2 = "http://www.dfm.ae/docs/default-source/default-document-library/how-to-trade-on-dfm-and-nasdaq-dubai.pdf";
    public static final String pdf_3 = "http://www.dfm.ae/docs/default-source/default-document-library/ivestor-user-guide-eng-2018.pdf?sfvrsn=6";
    public static final String pdf_4 = "http://www.dfm.ae/docs/default-source/DFM-Documents/dfm-at-a-glance.pdf?sfvrsn=6";
    public static final String pdf_5 = "http://www.dfm.ae/docs/default-source/guides/02-dfm-investors-rights-and-responsibilities_english6604d7f7f6026339b0d9ff00009be840.pdf";
    public static final String pdf_6 = "http://www.dfm.ae/docs/default-source/Sharia/standard-for-issuing-acquiring-and-trading-shares.pdf";
    public static final String pdf_7 = "http://www.dfm.ae/docs/default-source/Sharia/hedging-standards_en_v6_highres-5.pdf?sfvrsn=6";
    public static final String pdf_8 = "http://www.dfm.ae/docs/default-source/default-document-library/dfm-guide-on-ipo-communications.pdf?sfvrsn=0";
    public static final String pdf_9 = "http://www.dfm.ae/docs/default-source/guides/dfm_ir-guide2018.pdf?sfvrsn=2";
    public static final String pdf_10 = "http://www.dfm.ae/docs/default-source/Sharia/dfm-standard-for-issuing-acquiring-and-trading-sukuk.pdf";
    public static final String pdf_11 = "http://www.dfm.ae/docs/default-source/guides/dfm-guide-to-going-public-2018.pdf?sfvrsn=2";

    /*Pdf documents*/
    public static final String pdf_1a = "http://www.dfm.ae/docs/default-source/guides/الكتيب-التعريفي---المنتجات-و-الخدمات.pdf?sfvrsn=0";
    public static final String pdf_2a = "http://www.dfm.ae/docs/default-source/guides/كيفية-التداول-في-سوق-دبي-المالي-و-ناسداك-دبي.pdf?sfvrsn=0";
    public static final String pdf_3a = "http://www.dfm.ae/docs/default-source/default-document-library/ivestor-user-guide-arabic-2018.pdf?sfvrsn=6";
    public static final String pdf_4a = "http://www.dfm.ae/docs/default-source/guides/نظرة-عامة-على-سوق-دبي-المالي.pdf?sfvrsn=0";
    public static final String pdf_5a = "http://www.dfm.ae/docs/default-source/guides/dfm-investors-rights-and-responsibilities.pdf?sfvrsn=8";
    public static final String pdf_6a = "http://www.dfm.ae/docs/default-source/Sharia/معيار-سوق-دبي-المالي-لإصدار-وتملك-وتداول-الأسهم.pdf?sfvrsn=2";
    public static final String pdf_7a = "http://www.dfm.ae/docs/default-source/Sharia/hedging-standards_en_v6_highres-5fa97d4f7f6026339b0d9ff00009be840.pdf?sfvrsn=2";
    public static final String pdf_8a = "http://www.dfm.ae/docs/default-source/default-document-library/dfm-guide-to-ipo-communications-arabic-compressed.pdf?sfvrsn=0";
    public static final String pdf_9a = "http://www.dfm.ae/docs/default-source/guides/dfm_ir-guide2018.pdf?sfvrsn=2";
    public static final String pdf_10a = "http://www.dfm.ae/docs/default-source/Sharia/معيار-سوق-دبي-المالي-إلصدار-وتملك-وتداول-الصكوك.pdf";

    public static final String DISCLOSURE_PDF_PREFIX = "http://feeds.dfm.ae/documents";
    public static final String CALL = "(04) 305 5555";
    public static final String MAIL = "customerservice@dfm.ae";
    public static final String WEB = "http://www.dfm.ae";
    public static final String TWITTER = "https://twitter.com/DFMalerts";
    public static final String LINKEDIN = "https://www.linkedin.com/company/dubai-financial-market_2/";
    public static final String INSTAGRAM = "https://www.instagram.com/dubaifinancialmarket/";
    public static final String MARKET_APP = "https://play.google.com/store/apps/details?id=net.directfn.android.dfm";
    public static final String IR_APP = "https://play.google.com/store/apps/details?id=com.euroland.mobiletools.ae_dfm";
    public static final String SERVICE_CATALOG = "http://www.dfm.ae/products/services-catalog";

}
