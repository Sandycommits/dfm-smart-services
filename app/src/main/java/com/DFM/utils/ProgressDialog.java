package com.DFM.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.WindowManager;
import android.widget.ImageView;

import com.DFM.R;
import com.bumptech.glide.Glide;

/**
 * Created by Sandeep on 12/10/17.
 */

public class ProgressDialog {

    private static ProgressDialog progressDialog;
    //    private android.app.ProgressDialog dialog;
    private Dialog dialog;

    private ProgressDialog() {

    }

    public static ProgressDialog getInstance() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog();
        }

        return progressDialog;
    }

    public void displayProgressDialog(Context ctx) {
        /*dialog = android.app.ProgressDialog.show(ctx,
                "Status",
                "Loading, Please wait...");*/
        dialog = new Dialog(ctx, R.style.TransparentDialog);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        dialog.setContentView(R.layout.gif_layout);
        ImageView imageView = dialog.findViewById(R.id.gifView);
        Glide.with(ctx).load(R.drawable.loading_gif_src).into(imageView);
        dialog.setCancelable(true);
        dialog.show();
    }

    public void dismissProgressDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}
