package com.DFM;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import iammert.com.expandablelib.ExpandCollapseListener;
import iammert.com.expandablelib.ExpandableLayout;
import iammert.com.expandablelib.Section;

public class HowToTradeActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.expandLayout)
    ExpandableLayout expandLayout;
    private int rotationAngle = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_to_trade);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.how_to_trade));

        loadExpandableLayout();
    }

    private void loadExpandableLayout() {


        expandLayout.setRenderer(new ExpandableLayout.Renderer<String, String>() {
            @Override
            public void renderParent(View view, String model, boolean isExpanded, int parentPosition) {
                ((TextView) view.findViewById(R.id.headerView)).setText(model);
//                view.findViewById(R.id.expandIcon).setBackgroundResource(R.drawable.down_arrow);
            }

            @Override
            public void renderChild(View view, String model, int parentPosition, int childPosition) {
                WebView webView = view.findViewById(R.id.childView);
                WebSettings settings = webView.getSettings();
                settings.setJavaScriptEnabled(true);
                float fontSize = getResources().getDimension(R.dimen.webview_text_size);
                settings.setDefaultFontSize((int) fontSize);
                webView.loadData(model, "text/html", "utf-8");
            }
        });

        expandLayout.setExpandListener(new ExpandCollapseListener.ExpandListener<String>() {
            @Override
            public void onExpanded(int parentIndex, String parent, View view) {
                ImageView arrow = view.findViewById(R.id.expandIcon);
                rotationAngle = 0;
                ObjectAnimator anim = ObjectAnimator.ofFloat(arrow, "rotation", rotationAngle, rotationAngle + 180);
                anim.setDuration(500);
                anim.start();
            }
        });

        expandLayout.setCollapseListener(new ExpandCollapseListener.CollapseListener<String>() {
            @Override
            public void onCollapsed(int parentIndex, String parent, View view) {
                rotationAngle = 180;
                ImageView arrow = view.findViewById(R.id.expandIcon);
                ObjectAnimator anim = ObjectAnimator.ofFloat(arrow, "rotation", rotationAngle, rotationAngle + 180);
                anim.setDuration(500);
                anim.start();
            }
        });

        String[] headings = getResources().getStringArray(R.array.how_to_trade);
        String[] childs = getResources().getStringArray(R.array.how_to_trade_content);
        for (int i = 0; i < headings.length; i++) {

            Section<String, String> section = new Section<>();
            section.parent = headings[i];
            section.children.add(childs[i]);
            expandLayout.addSection(section);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
