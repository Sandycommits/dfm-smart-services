package com.DFM;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.DFM.dto.Login;
import com.DFM.dto.Register;
import com.DFM.fonts.HelveticaNeueBoldTextView;
import com.DFM.fonts.HelveticaNeueButton;
import com.DFM.fonts.HelveticaNeueEditText;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.PreferenceConnector;
import com.Wsdl2Code.WebServices.Authenticate.Authenticate;
import com.Wsdl2Code.WebServices.Authenticate.IWsdl2CodeEvents;
import com.kyleduo.switchbutton.SwitchButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements IWsdl2CodeEvents, CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.userName)
    HelveticaNeueEditText userName;
    @BindView(R.id.password)
    HelveticaNeueEditText password;
    @BindView(R.id.signIn)
    HelveticaNeueButton signIn;
    @BindView(R.id.signUp)
    HelveticaNeueButton signUp;
    @BindView(R.id.forgotPasword)
    HelveticaNeueBoldTextView forgotPassword;
    @BindView(R.id.skipLogin)
    HelveticaNeueBoldTextView skipLogin;
    @BindView(R.id.remember)
    SwitchButton remember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        remember.setOnCheckedChangeListener(this);
        Login login = (Login) CommonMethods.retrieveObject(this, PreferenceConnector.LOGIN, new Login());
        Register register = (Register) getIntent().getSerializableExtra("register");
        if (register != null) {
            CommonMethods.displayOKDialogue(this, getString(R.string.registration_successful));
            userName.setText(register.getEmail());
            password.setText(register.getPassword());
        } else {
            if (login != null && login.isRemember()) {
                userName.setText(login.getUserName());
                password.setText(login.getPassword());
                remember.setChecked(true);
            } else {
                if (login != null) {
                    userName.setText(login.getUserName());
                    password.setText("");
                } else {
                    userName.setText("");
                    password.setText("");
                }
                remember.setChecked(false);
            }
        }
    }

    @Override
    @OnClick({R.id.signIn, R.id.forgotPasword, R.id.signUp, R.id.skipLogin})
    public void onViewClicks(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.signIn:
                if (CommonMethods.isOnline(this)) {
                    boolean allFieldsOK = CommonMethods.validateEditTextFields(new EditText[]{userName, password});
                    if (allFieldsOK) {
                        showProgressDialog();
                        Authenticate authenticate = new Authenticate(LoginActivity.this);
                        try {
                            authenticate.Login_NewAsync(userName.getText().toString(), password.getText().toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        CommonMethods.displayToast(this, getString(R.string.fields_required));
                    }
                } else {
                    CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
                }
                break;
            case R.id.forgotPasword:
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://esrv.dfm.ae/ForgotPassword/ForgotPassword.aspx?lan=" + language)));
                break;
            case R.id.signUp:
                startActivity(new Intent(this, SignupActivity.class));
                this.finish();
                break;
            case R.id.skipLogin:
                break;
            default:
                break;
        }
    }

    @Override
    public void Wsdl2CodeStartedRequest() {

    }

    @Override
    public void Wsdl2CodeFinished(String methodName, Object data) {
        try {
            dismissProgressDialog();
//            XStream xStream = new XStream(new DomDriver());
            xStream.processAnnotations(Login.class);
            Login login = (Login) xStream.fromXML(data.toString());
            if (login.getStatus().equalsIgnoreCase("valid")) {
                login.setUserName(userName.getText().toString());
                login.setPassword(password.getText().toString());
                if (remember.isChecked()) {
                    login.setRemember(true);
                } else {
                    login.setRemember(false);
                }
                CommonMethods.storeObject(this, login, PreferenceConnector.LOGIN);
                startActivity(new Intent(this, DashboardActivity.class));
                this.finish();
            } else {
                CommonMethods.displayOKDialogue(this, login.getData().getDescription());
            }
        } catch (Exception e) {
            CommonMethods.getLog("Exception...");
        }
    }

    @Override
    public void Wsdl2CodeFinishedWithException(Exception ex) {
        dismissProgressDialog();
        CommonMethods.getLog(ex.getMessage());
    }

    @Override
    public void Wsdl2CodeEndedRequest() {

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

    }
}
