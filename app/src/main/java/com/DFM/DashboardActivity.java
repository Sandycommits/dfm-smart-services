package com.DFM;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.DFM.adapters.HomeFragmentPagerAdapter;
import com.DFM.adapters.NavigationAdapter;
import com.DFM.dto.Company;
import com.DFM.dto.Data;
import com.DFM.dto.Icon;
import com.DFM.dto.Login;
import com.DFM.dto.NIN;
import com.DFM.dto.Table;
import com.DFM.dto.Ticket;
import com.DFM.dto.UserCode;
import com.DFM.interfaces.DialogListener;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.Constants;
import com.DFM.utils.PreferenceConnector;
import com.DFM.utils.ProgressDialog;
import com.Wsdl2Code.WebServices.Enquiry.Enquiry;
import com.Wsdl2Code.WebServices.MiscFunctions.IWsdl2CodeEvents;
import com.Wsdl2Code.WebServices.MiscFunctions.MiscFunctions;
import com.Wsdl2Code.WebServices.MobileAds.MobileAds;
import com.Wsdl2Code.WebServices.PushNotification.PushNotification;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashboardActivity extends BaseActivity implements ViewPager.OnPageChangeListener, AdapterView.OnItemClickListener, View.OnClickListener {

    public static ViewPager pager;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @BindView(R.id.adView)
    WebView adView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.navigationDrawer)
    DrawerLayout navigationDrawer;
    @BindView(R.id.navigationListView)
    ListView navigationListView;
    @BindView(R.id.banner)
    ImageView banner;
    @BindView(R.id.indicator_layout)
    LinearLayout indicatorLayout;
    @BindView(R.id.dimView)
    View dimView;
    @BindView(R.id.parentLayout)
    RelativeLayout parentLayout;
    private Handler handler;
    private Runnable runnable;
    private ArrayList<Icon> iconList = new ArrayList<>();
    private ActionBarDrawerToggle mDrawerToggle;
    private Login mainLogin;
    private int pageCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                    /* host Activity */
                navigationDrawer,       /* DrawerLayout object */
                toolbar,
                R.string.open,  /* "open drawer" description for accessibility */
                R.string.close  /* "close drawer" description for accessibility */
        );
        navigationDrawer.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
//        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.blue));
        mainLogin = (Login) CommonMethods.retrieveObject(this, PreferenceConnector.LOGIN, new Login());
        loadNavigationMenu();
        addCircularFloatingMenu();
        getHomePageIcons();
        checkInvestor();
        registerDevice();
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadBanner();
    }

    private void loadBanner() {
        final String[] banners = Constants.banners;


        handler = new Handler();
        runnable = new Runnable() {
            int i = 0;

            public void run() {
                Glide.with(DashboardActivity.this).load(banners[i])
                        .apply(new RequestOptions().placeholder(R.drawable.banner)).into(banner);
                i++;
                if (i > banners.length - 1) {
                    i = 0;
                }
                handler.postDelayed(this, 5000);  //for interval...
            }
        };
        handler.post(runnable); //for initial delay..
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (handler != null) {
            handler.removeCallbacks(runnable);
            handler = null;
        }
    }

    private void registerDevice() {

        PushNotification notification = new PushNotification(new com.Wsdl2Code.WebServices.PushNotification.IWsdl2CodeEvents() {
            @Override
            public void Wsdl2CodeStartedRequest() {

            }

            @Override
            public void Wsdl2CodeFinished(String methodName, Object Data) {
                Data.toString();
            }

            @Override
            public void Wsdl2CodeFinishedWithException(Exception ex) {
                ex.printStackTrace();
            }

            @Override
            public void Wsdl2CodeEndedRequest() {

            }
        });
        String token = FirebaseInstanceId.getInstance().getToken();
        try {
            notification.Register_DeviceAsync(mainLogin.getData().getSession(), mainLogin.getUserName(), "DFMSmartAppAndroid", "1.2.0", token, "ANDROID", "Production", "Active");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkInvestor() {
        if (!mainLogin.getData().getLoginType().equalsIgnoreCase("investor")) {
            getAccountInfo(mainLogin.getData().getSession(), "", null);
        } else {
            Enquiry userCoderEnquiry = new Enquiry(new com.Wsdl2Code.WebServices.Enquiry.IWsdl2CodeEvents() {
                @Override
                public void Wsdl2CodeStartedRequest() {

                }

                @Override
                public void Wsdl2CodeFinished(String methodName, Object data) {
                    try {
//                        XStream xStream = new XStream(new DomDriver());
                        xStream.processAnnotations(Login.class);
                        Login login = (Login) xStream.fromXML(data.toString());
                        UserCode userCode = login.getData().getUserCodes();
                        saveHashMap(userCode);
                        CommonMethods.storeObject(DashboardActivity.this, userCode, PreferenceConnector.USER_CODES);
                        NIN nin = CommonMethods.getRootNin(userCode);
                        if (nin != null)
                            getAccountInfo(mainLogin.getData().getSession(), nin.getNIN(), nin.getAccountName());
                    } catch (Exception e) {

                    }
                }

                @Override
                public void Wsdl2CodeFinishedWithException(Exception ex) {
                }

                @Override
                public void Wsdl2CodeEndedRequest() {

                }
            });
            try {
                userCoderEnquiry.getUserCodesAsync(mainLogin.getData().getSession());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void saveHashMap(UserCode userCode) {
        HashMap<String, ArrayList<Company>> companyMap = new HashMap<>();
        HashMap<String, ArrayList<Company>> brokerMap = new HashMap<>();
        if (userCode != null) {
            for (NIN nin : userCode.getNinList()) {
                String ninString = nin.getNIN();
                if (userCode.getCompanyList() != null) {
                    ArrayList<Company> companies = getFilteredList(ninString, userCode, 1);
                    companyMap.put(ninString, companies);
                }
                if (userCode.getBrokersList() != null) {
                    ArrayList<Company> brokers = getFilteredList(ninString, userCode, 2);
                    brokerMap.put(ninString, brokers);
                }
            }
        }
        CommonMethods.storeMapObject(this, companyMap, PreferenceConnector.COMPANY_MAP);
        CommonMethods.storeMapObject(this, brokerMap, PreferenceConnector.BROKER_MAP);
    }

    private ArrayList<Company> getFilteredList(String nin, UserCode userCode, int position) {
        ArrayList<Company> tempList = new ArrayList<>();
        if (position == 1) {
            for (Company company : userCode.getCompanyList()) {
                if (company.getNIN().trim().equalsIgnoreCase(nin)) {
                    tempList.add(company);
                }
            }
        } else {
            for (Company company : userCode.getBrokersList()) {
                if (company.getNIN().trim().equalsIgnoreCase(nin)) {
                    tempList.add(company);
                }
            }
        }
        return tempList;
    }

    private void getAccountInfo(String session, String nan, final String accountName) {
        Enquiry profileEnquiry = new Enquiry(new com.Wsdl2Code.WebServices.Enquiry.IWsdl2CodeEvents() {
            @Override
            public void Wsdl2CodeStartedRequest() {

            }

            @Override
            public void Wsdl2CodeFinished(String methodName, Object data) {
                xStream.processAnnotations(Login.class);
                try {
                    Login login = (Login) xStream.fromXML(data.toString());
                    Table table = login.getData().getRecord().getTable();
                    if (table != null) {
                        if (accountName != null) {
                            table.setENNAME(accountName);
                        }
                        table.setEmail(mainLogin.getUserName());
                        CommonMethods.storeObject(DashboardActivity.this, table, PreferenceConnector.PROFILE);
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void Wsdl2CodeFinishedWithException(Exception ex) {
//                dismissProgressDialog();
            }

            @Override
            public void Wsdl2CodeEndedRequest() {

            }
        });
//        showProgressDialog();
        try {
            profileEnquiry.GetAccountInfoAsync(session, nan);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addCircularFloatingMenu() {

        FloatingActionButton darkButton = new FloatingActionButton.Builder(this)
                .setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.circular_float_background))
                .build();

        if (darkButton.getParent() != null)
            ((ViewGroup) darkButton.getParent()).removeView(darkButton);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ABOVE, indicatorLayout.getId());
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        params.bottomMargin = (int) CommonMethods.convertDpToPx(this, 5);
        darkButton.setLayoutParams(params);
        parentLayout.addView(darkButton);

        SubActionButton.Builder rLSubBuilder = new SubActionButton.Builder(this);
        rLSubBuilder.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.action_menu_background));
        ImageView rlIcon1 = new ImageView(this);
        ImageView rlIcon2 = new ImageView(this);
        ImageView rlIcon3 = new ImageView(this);
        ImageView rlIcon4 = new ImageView(this);
        ImageView rlIcon5 = new ImageView(this);

        rlIcon1.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_logout));
        rlIcon2.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_issue_token));
        rlIcon3.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_track_request));
        rlIcon4.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_dividends));
        rlIcon5.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_statement));

        // Set 4 SubActionButtons
        final FloatingActionMenu centerBottomMenu = new FloatingActionMenu.Builder(this)
                .setStartAngle(0)
                .setEndAngle(-180)
                .addSubActionView(rLSubBuilder.setContentView(rlIcon1).build())
                .addSubActionView(rLSubBuilder.setContentView(rlIcon2).build())
                .addSubActionView(rLSubBuilder.setContentView(rlIcon3).build())
                .addSubActionView(rLSubBuilder.setContentView(rlIcon4).build())
                .addSubActionView(rLSubBuilder.setContentView(rlIcon5).build())
                .attachTo(darkButton)
                .build();

        rlIcon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logOut();
                centerBottomMenu.close(true);
            }
        });

        rlIcon2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CommonMethods.isOnline(DashboardActivity.this)) {
                    issueToken();
                } else {
                    CommonMethods.displayOKDialogue(DashboardActivity.this, getString(R.string.no_internet));
                }
                centerBottomMenu.close(true);
            }
        });

        rlIcon3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CommonMethods.isOnline(DashboardActivity.this)) {
                    if (mainLogin.getData().getLoginType().equalsIgnoreCase("investor")) {
                        Intent intent = new Intent(DashboardActivity.this, TrackRequestActivity.class);
                        startActivity(intent);
                    } else {
                        CommonMethods.displayOKDialogue(DashboardActivity.this, getString(R.string.this_feature));
                    }
                } else {
                    CommonMethods.displayOKDialogue(DashboardActivity.this, getString(R.string.no_internet));
                }
                centerBottomMenu.close(true);
            }
        });

        rlIcon4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mainLogin.getData().getLoginType().equalsIgnoreCase("investor")) {
                    Intent intent = new Intent(DashboardActivity.this, EFormsActivity.class);
                    startActivity(intent);
                } else {
                    CommonMethods.displayOKDialogue(DashboardActivity.this, getString(R.string.this_feature));
                }
                centerBottomMenu.close(true);

            }
        });

        rlIcon5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mainLogin.getData().getLoginType().equalsIgnoreCase("investor")) {
                    if (CommonMethods.isOnline(DashboardActivity.this)) {
                        Intent intent = new Intent(DashboardActivity.this, MyStatementActivity.class);
                        startActivity(intent);
                        centerBottomMenu.close(true);
                    } else {
                        CommonMethods.displayOKDialogue(DashboardActivity.this, getString(R.string.no_internet));
                    }
                } else {
                    CommonMethods.displayOKDialogue(DashboardActivity.this, getString(R.string.this_feature));
                }
                centerBottomMenu.close(true);
            }
        });

        centerBottomMenu.setStateChangeListener(new FloatingActionMenu.MenuStateChangeListener() {
            @Override
            public void onMenuOpened(FloatingActionMenu floatingActionMenu) {
                dimView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onMenuClosed(FloatingActionMenu floatingActionMenu) {
                dimView.setVisibility(View.GONE);
            }
        });
    }

    private void issueToken() {
        final ProgressDialog progressDialog = ProgressDialog.getInstance();
        MiscFunctions miscFunctions = new MiscFunctions(new IWsdl2CodeEvents() {
            @Override
            public void Wsdl2CodeStartedRequest() {

            }

            @Override
            public void Wsdl2CodeFinished(String methodName, Object data) {
                progressDialog.dismissProgressDialog();
//                XStream xStream = new XStream(new DomDriver());
                xStream.processAnnotations(Login.class);
                try {
                    Login login = (Login) xStream.fromXML(data.toString());
                    if (login.getStatus().equalsIgnoreCase("valid")) {
                        Data response = login.getData();
                        if (response.getOfficeClosedEN() != null) {
                            if (language.equalsIgnoreCase("ar")) {
                                CommonMethods.displayOKDialogue(DashboardActivity.this, response.getOfficeClosedAR());
                            } else {
                                CommonMethods.displayOKDialogue(DashboardActivity.this, response.getOfficeClosedEN());
                            }
                        } else {
                            Ticket ticket = login.getData().getTicket();
                            if (ticket != null) {
                                Intent intent = new Intent(DashboardActivity.this, TokenDetailsActivity.class);
                                intent.putExtra("ticket", ticket);
                                startActivity(intent);
                            } else {
                                ArrayList<String> categories = login.getData().getCategory().getCategories();
                                ArrayList<String> arCategories = login.getData().getCategory().getCategoriesAR();
                                ArrayList<String> symbols = login.getData().getCategory().getCategoryIdList();
                                Intent intent = new Intent(DashboardActivity.this, TokenNumberActivity.class);
                                if (language.equalsIgnoreCase("ar")) {
                                    intent.putStringArrayListExtra("categories", arCategories);
                                } else {
                                    intent.putStringArrayListExtra("categories", categories);
                                }
                                intent.putStringArrayListExtra("symbols", symbols);
                                startActivity(intent);
                            }
                        }
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void Wsdl2CodeFinishedWithException(Exception ex) {
                progressDialog.dismissProgressDialog();
            }

            @Override
            public void Wsdl2CodeEndedRequest() {

            }
        });
        progressDialog.displayProgressDialog(this);
        UserCode userCode = (UserCode) CommonMethods.retrieveObject(this, PreferenceConnector.USER_CODES, new UserCode());
        NIN root = CommonMethods.getRootNin(userCode);
        Login login = (Login) CommonMethods.retrieveObject(this, PreferenceConnector.LOGIN, new Login());
        try {
            miscFunctions.IssueTokenListAsync(root.getNIN(), login.getData().getLoginType());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void loadNavigationMenu() {
        String[] menu = getResources().getStringArray(R.array.navigation_menu);
        NavigationAdapter adapter = new NavigationAdapter(this, R.layout.menu_item, menu);
        View footerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.menu_footer, null, false);
        ImageView twitter = footerView.findViewById(R.id.twitter);
        twitter.setOnClickListener(this);
        ImageView linkedin = footerView.findViewById(R.id.linkedin);
        linkedin.setOnClickListener(this);
        ImageView instagram = footerView.findViewById(R.id.instagram);
        instagram.setOnClickListener(this);
        navigationListView.addFooterView(footerView);
        navigationListView.setAdapter(adapter);
        navigationListView.setOnItemClickListener(this);
    }

    private void loadAd() {

        MobileAds mobileAds = new MobileAds(new com.Wsdl2Code.WebServices.MobileAds.IWsdl2CodeEvents() {
            @Override
            public void Wsdl2CodeStartedRequest() {

            }

            @Override
            public void Wsdl2CodeFinished(String methodName, Object data) {
                dismissProgressDialog();
                try {
                    String afterDecode = URLDecoder.decode(data.toString(), "UTF-8");
                    adView.getSettings().setAllowFileAccess(true);
                    adView.getSettings().setJavaScriptEnabled(true);
                    adView.getSettings().setAppCacheEnabled(true);
                    adView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//                    adView.getSettings().setPluginState(WebSettings.PluginState.ON);
                    adView.loadData(afterDecode.trim(), "text/html", null);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void Wsdl2CodeFinishedWithException(Exception ex) {
                dismissProgressDialog();
            }

            @Override
            public void Wsdl2CodeEndedRequest() {

            }
        });
        showProgressDialog();
        try {
            mobileAds.MobileAdsBannerAsync("Android", "en");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewClicks(View view) {

    }

    private void getHomePageIcons() {
        MiscFunctions miscFunctions = new MiscFunctions(new IWsdl2CodeEvents() {
            @Override
            public void Wsdl2CodeStartedRequest() {

            }

            @Override
            public void Wsdl2CodeFinished(String methodName, Object data) {
                dismissProgressDialog();
                /*XStream xStream = new XStream(new DomDriver()) {
                    // to enable ignoring of unknown elements
                    @Override
                    protected MapperWrapper wrapMapper(MapperWrapper next) {
                        return new MapperWrapper(next) {
                            @Override
                            public boolean shouldSerializeMember(Class definedIn, String fieldName) {
                                if (definedIn == Object.class) {
                                    try {
                                        return this.realClass(fieldName) != null;
                                    } catch (Exception e) {
                                        return false;
                                    }
                                }
                                return super.shouldSerializeMember(definedIn, fieldName);
                            }
                        };
                    }
                };*/
                xStream.processAnnotations(Login.class);
                try {
                    Login login = (Login) xStream.fromXML(data.toString());
                    iconList = login.getData().getIcons();
                    CommonMethods.storeObject(DashboardActivity.this, login.getData(), PreferenceConnector.ICONS);
                    loadPager();
                    loadAd();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void Wsdl2CodeFinishedWithException(Exception ex) {
                dismissProgressDialog();
            }

            @Override
            public void Wsdl2CodeEndedRequest() {

            }
        });
        showProgressDialog();
        try {
            miscFunctions.HomePageIconsAsync(mainLogin.getData().getLoginType());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadPager() {
        pager = (ViewPager) findViewById(R.id.pager);
        HomeFragmentPagerAdapter pagerAdapter = new HomeFragmentPagerAdapter(getSupportFragmentManager(), this, iconList, language);
        pager.setAdapter(pagerAdapter);
        pager.addOnPageChangeListener(this);
        loadIndicator();
    }

    private void loadIndicator() {
        indicatorLayout.setVisibility(View.VISIBLE);
        int size = iconList.size();
        float result = (float) size / (float) 6;
        pageCount = (int) Math.ceil(result);
        for (int i = 0; i < pageCount; i++) {
            ImageView iv = new ImageView(this);
            iv.setImageResource(R.drawable.dot_inactive);
            iv.setPadding(0, 0, 0, 0);
            indicatorLayout.addView(iv);
            iv = null;
        }
        ImageView iv = (ImageView) indicatorLayout.getChildAt(0);
        iv.setImageResource(R.drawable.dot_active);
        iv = null;
    }

    /**
     * The following are the callback methods for OnPageChangeListener interface
     *
     * @param i
     * @param v
     * @param i1
     */
    @Override
    public void onPageScrolled(int i, float v, int i1) {
    }

    @Override
    public void onPageSelected(int position) {
        if (position < pageCount) {
            indicatorLayout.setVisibility(View.VISIBLE);
            for (int j = 0; j < pageCount; j++) {
                ImageView iv = (ImageView) indicatorLayout.getChildAt(j);
                if (j == position) {
                    iv.setImageResource(R.drawable.dot_active);
                } else {
                    iv.setImageResource(R.drawable.dot_inactive);
                }
                iv = null;
            }
        } else {
            indicatorLayout.setVisibility(View.GONE);
        }

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent;
        switch (i) {
            case 0:
                intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
                break;
            case 1:
                intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{Constants.MAIL});
                intent.putExtra(Intent.EXTRA_SUBJECT, "Customer Service:");

                startActivity(Intent.createChooser(intent, "Email via..."));
                break;
            case 2:
                if (CommonMethods.isOnline(this)) {
                    intent = new Intent(this, LocationActivity.class);
                    startActivity(intent);
                } else {
                    CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
                }
                break;
            case 3:

                if (mainLogin.getData().getLoginType().equalsIgnoreCase("investor")) {
                    if (CommonMethods.isOnline(this)) {
                        intent = new Intent(this, MyStatementActivity.class);
                        startActivity(intent);
                    } else {
                        CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
                    }
                } else {
                    CommonMethods.displayOKDialogue(DashboardActivity.this, getString(R.string.this_feature));
                }
                break;
            case 4:
                if (mainLogin.getData().getLoginType().equalsIgnoreCase("investor")) {
                    intent = new Intent(this, EFormsActivity.class);
                    startActivity(intent);
                } else {
                    CommonMethods.displayOKDialogue(DashboardActivity.this, getString(R.string.this_feature));
                }
                break;
            case 5:
                Uri call = Uri.parse("tel:" + Constants.CALL);
                intent = new Intent(Intent.ACTION_DIAL, call);
                startActivity(intent);
                break;
            case 6:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(Constants.WEB));
                startActivity(intent);
                break;
            case 7:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                this.finish();
                break;
            case 8:
                logOut();
                break;
            default:
                break;
        }
        navigationDrawer.closeDrawer(Gravity.START);

    }

    private void logOut() {

        CommonMethods.displayConfirmationDialogue(this, getString(R.string.logout), new DialogListener() {
            @Override
            public void onConfirmation(boolean isConfirmed) {
                if (true) {
                    Login login = (Login) CommonMethods.retrieveObject(DashboardActivity.this, PreferenceConnector.LOGIN, new Login());
                    PreferenceConnector.getEditor(DashboardActivity.this).clear().apply();
                    PreferenceConnector.writeBoolean(DashboardActivity.this, PreferenceConnector.TERMS, true);
                    CommonMethods.storeObject(DashboardActivity.this, login, PreferenceConnector.LOGIN);
                    Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
                    startActivity(intent);
                    DashboardActivity.this.finish();
                }
            }
        });
    }

    @Override
    public void onClick(View view) {

        int id = view.getId();
        Intent intent;
        switch (id) {
            case R.id.twitter:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(Constants.TWITTER));
                startActivity(intent);
                break;
            case R.id.instagram:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(Constants.INSTAGRAM));
                startActivity(intent);
                break;
            case R.id.linkedin:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(Constants.LINKEDIN));
                startActivity(intent);
                break;
            default:
                break;
        }
    }
}
