package com.DFM;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.DFM.adapters.IVestorRecyclerAdapter;
import com.DFM.dto.IVestor;
import com.DFM.dto.IVestorRecord;
import com.DFM.dto.Login;
import com.DFM.dto.NIN;
import com.DFM.dto.UserCode;
import com.DFM.fonts.HelveticaNeueBoldTextView;
import com.DFM.fonts.HelveticaNeueEditText;
import com.DFM.fonts.HelveticaNeueTextView;
import com.DFM.interfaces.OnDateSelectionListener;
import com.DFM.interfaces.OnDialogItemClickListener;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.PreferenceConnector;
import com.Wsdl2Code.WebServices.Enquiry.Enquiry;
import com.Wsdl2Code.WebServices.Enquiry.IWsdl2CodeEvents;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class IVestorActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.totalValue)
    HelveticaNeueBoldTextView totalValue;
    @BindView(R.id.selectNin)
    HelveticaNeueEditText selectNin;
    @BindView(R.id.startDate)
    HelveticaNeueTextView startDate;
    @BindView(R.id.endDate)
    HelveticaNeueTextView endDate;
    @BindView(R.id.iVestorRecycler)
    RecyclerView iVestorRecycler;
    DecimalFormat formatter;
    @BindView(R.id.print)
    ImageView print;
    @BindView(R.id.rootView)
    View rootView;
    int PERMISSION_ID = 1;
    @BindView(R.id.noData)
    RelativeLayout noData;
    private UserCode userCode;
    private String[] permissions = new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ivestor);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.iVestor_statement));
        print.setVisibility(View.VISIBLE);

        NumberFormat nFormatter = NumberFormat.getNumberInstance(Locale.US);
        formatter = (DecimalFormat) nFormatter;
        formatter.applyPattern("#,###,###.##");
        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);

        endDate.setText(CommonMethods.getCurrentDateDisplay());
        startDate.setText(CommonMethods.getPreviousMonthDateDisplay());
        userCode = (UserCode) CommonMethods.retrieveObject(this, PreferenceConnector.USER_CODES, new UserCode());
        NIN root = CommonMethods.getRootNin(userCode);
        selectNin.setText(root.getNIN() + "-" + root.getAccountName());

        callService();
    }

    private void callService() {
        if (CommonMethods.isOnline(this)) {
            Enquiry enquiry = new Enquiry(new IWsdl2CodeEvents() {
                @Override
                public void Wsdl2CodeStartedRequest() {

                }

                @Override
                public void Wsdl2CodeFinished(String methodName, Object data) {
                    dismissProgressDialog();
//                    XStream xStream = new XStream(new DomDriver());
                    xStream.processAnnotations(Login.class);
                    Login login = (Login) xStream.fromXML(data.toString());
                    if (login.getStatus().equalsIgnoreCase("valid")) {
                        IVestor iVestor = login.getData().getiVestor();
                        ArrayList<IVestorRecord> records = iVestor.getRecords();
                        if (records != null) {
                            noData.setVisibility(View.GONE);
                            loadRecycler(records);
                        } else {
                            loadRecycler(new ArrayList<IVestorRecord>());
                            noData.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void Wsdl2CodeFinishedWithException(Exception ex) {
                    dismissProgressDialog();
                }

                @Override
                public void Wsdl2CodeEndedRequest() {

                }
            });
            Login login = (Login) CommonMethods.retrieveObject(this, PreferenceConnector.LOGIN, new Login());
            String nin = CommonMethods.getInitialLiteral(selectNin.getText().toString());
            String fromDate = CommonMethods.getServiceDateFormat(startDate.getText().toString());
            String toDate = CommonMethods.getServiceDateFormat(endDate.getText().toString());
            showProgressDialog();
            try {
                enquiry.GetiVESTORStatementAsync(login.getData().getSession(), nin, fromDate, toDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }
    }

    private void loadRecycler(ArrayList<IVestorRecord> records) {
        if (records.size() > 0) {
            IVestorRecord record = records.get(0);
            totalValue.setText(formatter.format(record.getClosingBalance()));
            IVestorRecyclerAdapter adapter = new IVestorRecyclerAdapter(IVestorActivity.this, records);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            iVestorRecycler.setHasFixedSize(true);
//        iVestorRecycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
            iVestorRecycler.setLayoutManager(layoutManager);
            iVestorRecycler.setAdapter(adapter);
        }
    }

    @Override
    @OnClick({R.id.selectNin, R.id.startDate, R.id.endDate, R.id.print})
    public void onViewClicks(View view) {
        int id = view.getId();
        String nin = CommonMethods.getInitialLiteral(selectNin.getText().toString());
        switch (id) {
            case R.id.selectNin:
                String[] ninStrings = CommonMethods.getNinArray(userCode.getNinList());
                int selectedPosition = CommonMethods.getStringPosition(ninStrings, selectNin.getText().toString());
                CommonMethods.showListDialog(this, ninStrings, new OnDialogItemClickListener() {
                    @Override
                    public void onDialogItem(String selected) {
                        selectNin.setText(selected);
                        callService();
                    }
                }, selectedPosition);
                break;
            case R.id.startDate:
                CommonMethods.displayDatePickerDialog(this, new OnDateSelectionListener() {
                    @Override
                    public void onDateSelected(String date) {
                        startDate.setText(date);
                        callService();
                    }
                }, false);
                break;
            case R.id.endDate:
                CommonMethods.displayDatePickerDialog(this, new OnDateSelectionListener() {
                    @Override
                    public void onDateSelected(String date) {
                        endDate.setText(date);
                        callService();
                    }
                }, true);
                break;
            case R.id.print:
                if (CommonMethods.hasPermissions(this, permissions)) {
                    Bitmap bitmap = CommonMethods.getScreenShot(rootView);
                    CommonMethods.store(bitmap, "dfm_screenshot.jpeg", this);
                } else {
                    ActivityCompat.requestPermissions(this, permissions, PERMISSION_ID);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            Bitmap bitmap = CommonMethods.getScreenShot(rootView);
            CommonMethods.store(bitmap, "dfm_screenshot.jpeg", this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

}
