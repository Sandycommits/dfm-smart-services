package com.DFM;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.DFM.dto.Login;
import com.DFM.dto.Record;
import com.DFM.fonts.HelveticaNeueBoldTextView;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.Constants;
import com.Wsdl2Code.WebServices.Enquiry.Enquiry;
import com.Wsdl2Code.WebServices.Enquiry.IWsdl2CodeEvents;
import com.wx.wheelview.adapter.ArrayWheelAdapter;
import com.wx.wheelview.widget.WheelView;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StockQuotesActivity extends BaseActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.wheelView)
    WheelView wheelView;
    List<String> mList;
    @BindView(R.id.symbol)
    HelveticaNeueBoldTextView symbol;
    @BindView(R.id.stockPrice)
    HelveticaNeueBoldTextView stockPrice;
    @BindView(R.id.quoteCall)
    ImageView quoteCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_quotes);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.stock_quotes));
        loadWheel();
    }

    @Override
    @OnClick({R.id.quoteCall})
    public void onViewClicks(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.quoteCall:
                Uri call = Uri.parse("tel:" + Constants.CALL);
                Intent intent = new Intent(Intent.ACTION_DIAL, call);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    private void loadWheel() {

        String[] wheelArray = getString(R.string.stock_symbols).split(",");
        mList = Arrays.asList(wheelArray);
        wheelView.setWheelAdapter(new ArrayWheelAdapter(this));
        wheelView.setSkin(WheelView.Skin.Holo);
        wheelView.setWheelData(mList);
        WheelView.WheelViewStyle style = new WheelView.WheelViewStyle();
        boolean isTablet = getResources().getBoolean(R.bool.isTablet);
        if (isTablet) {
            style.selectedTextSize = 24;
            style.textSize = 20;
            wheelView.setWheelSize(9);
        } else {
            style.selectedTextSize = 20;
            style.textSize = 16;
            wheelView.setWheelSize(5);
        }
        style.holoBorderColor = Color.LTGRAY;
        style.textColor = Color.LTGRAY;
        style.selectedTextColor = Color.BLACK;
        wheelView.setStyle(style);

        wheelView.setOnWheelItemSelectedListener(new WheelView.OnWheelItemSelectedListener() {
            @Override
            public void onItemSelected(final int position, Object o) {
                if (CommonMethods.isOnline(StockQuotesActivity.this)) {

                    Enquiry enquiry = new Enquiry(new IWsdl2CodeEvents() {
                        @Override
                        public void Wsdl2CodeStartedRequest() {

                        }

                        @Override
                        public void Wsdl2CodeFinished(String methodName, Object data) {
                            dismissProgressDialog();
//                            XStream xStream = new XStream(new DomDriver());
                            xStream.processAnnotations(Login.class);
                            Login login = (Login) xStream.fromXML(data.toString());
                            if (login.getStatus().equalsIgnoreCase("VALID")) {
                                Record record = login.getData().getStockQuotes().getRecord();
                                symbol.setText(record.getSymbol());
                                stockPrice.setText(record.getCurrent_Price());
                            } else {
                                CommonMethods.displayOKDialogue(StockQuotesActivity.this, getString(R.string.price_not) + " " + mList.get(position));
                            }
                        }

                        @Override
                        public void Wsdl2CodeFinishedWithException(Exception ex) {
                            dismissProgressDialog();
                        }

                        @Override
                        public void Wsdl2CodeEndedRequest() {

                        }
                    });
                    showProgressDialog();
                    try {
                        enquiry.GetStockQuotesAsync(mList.get(position), false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    CommonMethods.displayOKDialogue(StockQuotesActivity.this, getString(R.string.no_internet));
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
