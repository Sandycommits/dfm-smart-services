package com.DFM.fonts;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

/**
 * Created by Sandeep on 10/10/17.
 */

public class HelveticaNeueEditText extends AppCompatEditText {

    public HelveticaNeueEditText(Context context) {
        super(context);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "HelveticaNeue.ttf");
        this.setTypeface(face);
    }

    public HelveticaNeueEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "HelveticaNeue.ttf");
        this.setTypeface(face);
    }

    public HelveticaNeueEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "HelveticaNeue.ttf");
        this.setTypeface(face);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
