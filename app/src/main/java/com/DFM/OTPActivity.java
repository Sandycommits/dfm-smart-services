package com.DFM;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.LinearLayout;

import com.DFM.dto.Data;
import com.DFM.dto.Login;
import com.DFM.dto.Register;
import com.DFM.dto.RegistrationResponse;
import com.DFM.fonts.HelveticaNeueButton;
import com.DFM.fonts.HelveticaNeueEditText;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.Constants;
import com.Wsdl2Code.WebServices.Enquiry.Enquiry;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPActivity extends BaseActivity {

    @BindView(R.id.submit)
    HelveticaNeueButton submit;
    @BindView(R.id.reSendCode)
    HelveticaNeueButton reSendCode;

    @BindView(R.id.emailCode)
    HelveticaNeueEditText emailCode;
    @BindView(R.id.smsCode)
    HelveticaNeueEditText smsCode;

    @BindView(R.id.smsCodeLayout)
    LinearLayout smsCodeLayout;

    private Register register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);
        setCountDown();
        register = (Register) getIntent().getSerializableExtra("register");
        if (register.getSmsCode().isEmpty()) {
            smsCodeLayout.setVisibility(View.GONE);
        } else {
            smsCodeLayout.setVisibility(View.VISIBLE);
        }
        register.getMobile();
    }

    private void setCountDown() {
        reSendCode.setBackgroundResource(R.drawable.otp_inactive_shape);
        new CountDownTimer(6000, 1000) {

            public void onTick(long millisUntilFinished) {
//                smileTextView.setText(getString(R.string.smile_for_camera) +": "+ (millisUntilFinished/1000));
//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                reSendCode.setText(getString(R.string.re_send) + " (" + millisUntilFinished / 1000 + ")");
            }

            public void onFinish() {
                reSendCode.setText(getString(R.string.re_send));
                reSendCode.setBackgroundResource(R.drawable.otp_shape);
                reSendCode.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (CommonMethods.isOnline(OTPActivity.this)) {
                            sendCodes();
                        } else {
                            CommonMethods.displayOKDialogue(OTPActivity.this, getString(R.string.no_internet));
                        }
                    }
                });
            }
        }.start();
    }

    private void sendCodes() {
        Enquiry enquiry = new Enquiry(new com.Wsdl2Code.WebServices.Enquiry.IWsdl2CodeEvents() {
            @Override
            public void Wsdl2CodeStartedRequest() {

            }

            @Override
            public void Wsdl2CodeFinished(String methodName, Object data) {
//                XStream xStream = new XStream(new DomDriver());
                xStream.processAnnotations(Login.class);
                Login login = (Login) xStream.fromXML(data.toString());
                final Data codeData = login.getData();
                register.setEmailCode(codeData.getEmailCode());
                if (!register.getSmsCode().isEmpty()) {
                    Enquiry mobileEnquiry = new Enquiry(new com.Wsdl2Code.WebServices.Enquiry.IWsdl2CodeEvents() {
                        @Override
                        public void Wsdl2CodeStartedRequest() {

                        }

                        @Override
                        public void Wsdl2CodeFinished(String methodName, Object Data) {
                            dismissProgressDialog();
                            register.setSmsCode(codeData.getSMSCode());
                            setCountDown();
                        }

                        @Override
                        public void Wsdl2CodeFinishedWithException(Exception ex) {
                            dismissProgressDialog();
                        }

                        @Override
                        public void Wsdl2CodeEndedRequest() {

                        }
                    });
                    try {
                        mobileEnquiry.Send_SMSAsync(register.getMobile(), "", "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    dismissProgressDialog();
                    register.setSmsCode("");
                    setCountDown();
                }
            }

            @Override
            public void Wsdl2CodeFinishedWithException(Exception ex) {
                dismissProgressDialog();
            }

            @Override
            public void Wsdl2CodeEndedRequest() {

            }
        });
        try {
            showProgressDialog();
            enquiry.Send_EmailAsync(register.getEmail(), "eservices@dfm.ae", "", "", "en", Constants.ACTIVATION_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @OnClick({R.id.submit})
    public void onViewClicks(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.submit:
                String smsString = smsCode.getText().toString();
                String emailString = emailCode.getText().toString();
                if (register.getSmsCode().isEmpty()) {
                    if (register.getEmailCode().equals(emailString)) {
                        callRegistrationService();
                    } else {
                        CommonMethods.displayOKDialogue(this, getString(R.string.match_email_code));
                    }
                } else {
                    if (register.getEmailCode().equals(emailString) && register.getSmsCode().equals(smsString)) {
                        callRegistrationService();
                    } else {
                        CommonMethods.displayOKDialogue(this, getString(R.string.match_email_sms_code));
                    }
                }
                break;
            default:
                break;
        }
    }

    private void callRegistrationService() {

        if (CommonMethods.isOnline(this)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("FirstName", register.getFirstName());
            map.put("LastName", register.getLastName());
            map.put("Email", register.getEmail());
            map.put("Password", register.getPassword());
            map.put("Mobile", register.getMobile());
            map.put("NIN", register.getNAN());
            map.put("encryptionKey", Constants.ACTIVATION_CODE);
            showProgressDialog();
            apiService.registration(map).enqueue(new Callback<RegistrationResponse>() {
                @Override
                public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
                    dismissProgressDialog();
                    Intent intent = new Intent(OTPActivity.this, LoginActivity.class);
                    intent.putExtra("register", register);
                    startActivity(intent);
                    OTPActivity.this.finish();
                }

                @Override
                public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                    dismissProgressDialog();
                }
            });
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        this.finish();
    }
}
