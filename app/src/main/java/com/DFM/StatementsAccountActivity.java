package com.DFM;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.DFM.dto.AccountRecord;
import com.DFM.dto.Company;
import com.DFM.dto.Login;
import com.DFM.dto.NIN;
import com.DFM.dto.UserCode;
import com.DFM.expandable.AccountAdapter;
import com.DFM.expandable.AccountHeader;
import com.DFM.fonts.HelveticaNeueEditText;
import com.DFM.fonts.HelveticaNeueTextView;
import com.DFM.interfaces.OnAccountItemClickListener;
import com.DFM.interfaces.OnDateSelectionListener;
import com.DFM.interfaces.OnDialogItemClickListener;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.PreferenceConnector;
import com.Wsdl2Code.WebServices.Enquiry.Enquiry;
import com.Wsdl2Code.WebServices.Enquiry.IWsdl2CodeEvents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StatementsAccountActivity extends BaseActivity implements OnAccountItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.selectNin)
    HelveticaNeueEditText selectNin;
    @BindView(R.id.selectBroker)
    HelveticaNeueEditText selectBroker;
    @BindView(R.id.selectCompany)
    HelveticaNeueEditText selectCompany;
    @BindView(R.id.accountRecycler)
    RecyclerView accountRecycler;
    @BindView(R.id.startDate)
    HelveticaNeueTextView startDate;
    @BindView(R.id.endDate)
    HelveticaNeueTextView endDate;
    @BindView(R.id.print)
    ImageView print;
    @BindView(R.id.rootView)
    View rootView;
    @BindView(R.id.noData)
    RelativeLayout noData;
    int PERMISSION_ID = 1;
    private UserCode userCode;
    private HashMap<String, ArrayList<Company>> brokerMap = new HashMap<>();
    private HashMap<String, ArrayList<Company>> companyMap = new HashMap<>();
    private String[] permissions = new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statements_account);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.statement_of_account));
        print.setVisibility(View.VISIBLE);

        loadData();
        callService();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            Bitmap bitmap = CommonMethods.getScreenShot(rootView);
            CommonMethods.store(bitmap, "dfm_screenshot.jpeg", this);
        }
    }

    @Override
    @OnClick({R.id.selectNin, R.id.startDate, R.id.endDate, R.id.selectBroker, R.id.selectCompany, R.id.print})
    public void onViewClicks(View view) {
        int id = view.getId();
        String nin = CommonMethods.getInitialLiteral(selectNin.getText().toString());
        switch (id) {
            case R.id.selectNin:
                String[] ninStrings = CommonMethods.getNinArray(userCode.getNinList());
                int selectedPosition = CommonMethods.getStringPosition(ninStrings, selectNin.getText().toString());
                CommonMethods.showListDialog(this, ninStrings, new OnDialogItemClickListener() {
                    @Override
                    public void onDialogItem(String selected) {
                        selectNin.setText(selected);
                        selectBroker.setText(getString(R.string.all));
                        selectCompany.setText(getString(R.string.all));
                        callService();
                    }
                }, selectedPosition);
                break;
            case R.id.startDate:
                CommonMethods.displayDatePickerDialog(this, new OnDateSelectionListener() {
                    @Override
                    public void onDateSelected(String date) {
                        startDate.setText(date);
                        callService();
                    }
                }, false);
                break;
            case R.id.endDate:
                CommonMethods.displayDatePickerDialog(this, new OnDateSelectionListener() {
                    @Override
                    public void onDateSelected(String date) {
                        endDate.setText(date);
                        callService();
                    }
                }, true);
                break;
            case R.id.selectBroker:
                String[] brokerArray = CommonMethods.getBrokerArray(brokerMap.get(nin), language);
                if (brokerArray.length > 0) {
                    int broPosition = CommonMethods.getStringPosition(brokerArray, selectBroker.getText().toString());
                    CommonMethods.showListDialog(this, brokerArray, new OnDialogItemClickListener() {
                        @Override
                        public void onDialogItem(String selected) {
                            selectBroker.setText(selected);
                            callService();
                        }
                    }, broPosition);
                }
                break;
            case R.id.selectCompany:
                String[] companyArray = CommonMethods.getCompanyArray(companyMap.get(nin), language);
                if (companyArray.length > 0) {
                    int compPosition = CommonMethods.getStringPosition(companyArray, selectCompany.getText().toString());
                    CommonMethods.showListDialog(this, companyArray, new OnDialogItemClickListener() {
                        @Override
                        public void onDialogItem(String selected) {
                            selectCompany.setText(selected);
                            callService();
                        }
                    }, compPosition);
                }
                break;
            case R.id.print:
                if (CommonMethods.hasPermissions(this, permissions)) {
                    Bitmap bitmap = CommonMethods.getScreenShot(rootView);
                    CommonMethods.store(bitmap, "dfm_screenshot.jpeg", this);
                } else {
                    ActivityCompat.requestPermissions(this, permissions, PERMISSION_ID);
                }
                break;
            default:
                break;
        }
    }

    private void callService() {

        if (CommonMethods.isOnline(this)) {
            Enquiry enquiry = new Enquiry(new IWsdl2CodeEvents() {
                @Override
                public void Wsdl2CodeStartedRequest() {

                }

                @Override
                public void Wsdl2CodeFinished(String methodName, Object data) {
                    dismissProgressDialog();
//                    XStream xStream = new XStream(new DomDriver());
                    xStream.processAnnotations(Login.class);
                    try {
                        Login login = (Login) xStream.fromXML(data.toString());
                        ArrayList<AccountRecord> records = login.getData().getAccounts().getRecords();
                        if (records != null) {
                            noData.setVisibility(View.GONE);
                            bindData(records);
                        } else {
                            bindData(new ArrayList<AccountRecord>());
                            noData.setVisibility(View.VISIBLE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void Wsdl2CodeFinishedWithException(Exception ex) {
                    dismissProgressDialog();
                }

                @Override
                public void Wsdl2CodeEndedRequest() {

                }
            });
            Login login = (Login) CommonMethods.retrieveObject(this, PreferenceConnector.LOGIN, new Login());
            String nin = CommonMethods.getInitialLiteral(selectNin.getText().toString());
            String broker = CommonMethods.getInitialLiteral(selectBroker.getText().toString());
            String company = CommonMethods.getInitialLiteral(selectCompany.getText().toString());
            String fromDate = CommonMethods.getServiceDateFormat(startDate.getText().toString());
            String toDate = CommonMethods.getServiceDateFormat(endDate.getText().toString());
            showProgressDialog();
            try {
                enquiry.GetStatementOfAccountsAsync(login.getData().getSession(), nin, broker, company, fromDate, toDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }
    }

    private void bindData(ArrayList<AccountRecord> records) {
        LinkedHashSet<String> symbols = new LinkedHashSet<>();
        for (AccountRecord record : records) {
            symbols.add(record.getSymbol());
        }

        List<AccountHeader> categories = new ArrayList<>();
        for (String symbol : symbols) {
            ArrayList<AccountRecord> tempList = new ArrayList<>();
            AccountRecord headerRecord = null;
            for (AccountRecord record : records) {
                if (symbol.equalsIgnoreCase(record.getSymbol())) {
                    if (!record.getBrokerCode().isEmpty()) {
                        tempList.add(record);
                    } else {
                        headerRecord = record;
                    }
                }
            }
            categories.add(new AccountHeader(headerRecord, tempList));
        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        //instantiate your adapter with the list of genres
        AccountAdapter adapter = new AccountAdapter(categories, this);
        accountRecycler.setLayoutManager(layoutManager);
        accountRecycler.setAdapter(adapter);
    }

    private void loadData() {
        brokerMap = CommonMethods.retrieveMapObject(this, PreferenceConnector.BROKER_MAP);
        companyMap = CommonMethods.retrieveMapObject(this, PreferenceConnector.COMPANY_MAP);
        startDate.setText(CommonMethods.getPreviousMonthDateDisplay());
        endDate.setText(CommonMethods.getCurrentDateDisplay());
        userCode = (UserCode) CommonMethods.retrieveObject(this, PreferenceConnector.USER_CODES, new UserCode());
        NIN root = CommonMethods.getRootNin(userCode);
        selectNin.setText(root.getNIN() + "-" + root.getAccountName());
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onAccountItemClick(AccountRecord record) {
        Intent intent = new Intent(this, AccountDetailsActivity.class);
        intent.putExtra("account_record", record);
        startActivity(intent);
    }
}
