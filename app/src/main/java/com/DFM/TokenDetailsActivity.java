package com.DFM;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.DFM.dto.Login;
import com.DFM.dto.Table;
import com.DFM.dto.Ticket;
import com.DFM.fonts.HelveticaNeueBoldTextView;
import com.DFM.fonts.HelveticaNeueTextView;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.PreferenceConnector;
import com.Wsdl2Code.WebServices.MiscFunctions.IWsdl2CodeEvents;
import com.Wsdl2Code.WebServices.MiscFunctions.MiscFunctions;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TokenDetailsActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nameAR)
    HelveticaNeueBoldTextView nameAR;
    @BindView(R.id.nameEN)
    HelveticaNeueBoldTextView nameEN;
    @BindView(R.id.ticketNumber)
    HelveticaNeueBoldTextView ticketNumber;
    @BindView(R.id.waitingCount)
    HelveticaNeueBoldTextView waitingCount;
    @BindView(R.id.date)
    HelveticaNeueTextView date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_token_details);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.token_number_details));
        Ticket ticket = (Ticket) getIntent().getSerializableExtra("ticket");
        if (ticket != null) {
            loadTicket(ticket);
        } else {
            loadData();
        }
    }

    @Override
    public void onViewClicks(View view) {

    }

    private void loadData() {

        String category = getIntent().getStringExtra("category");
        Table table = (Table) CommonMethods.retrieveObject(this, PreferenceConnector.PROFILE, new Table());

        MiscFunctions miscFunctions = new MiscFunctions(new IWsdl2CodeEvents() {
            @Override
            public void Wsdl2CodeStartedRequest() {

            }

            @Override
            public void Wsdl2CodeFinished(String methodName, Object data) {
                dismissProgressDialog();
//                XStream xStream = new XStream(new DomDriver());
                xStream.processAnnotations(Login.class);
                Login login = (Login) xStream.fromXML(data.toString());
                Ticket ticket = login.getData().getTicket();
                loadTicket(ticket);
            }

            @Override
            public void Wsdl2CodeFinishedWithException(Exception ex) {
                dismissProgressDialog();
            }

            @Override
            public void Wsdl2CodeEndedRequest() {

            }
        });
        showProgressDialog();
        try {
            miscFunctions.IssueTicketAsync(category, table.getPHONE2());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadTicket(Ticket ticket) {
        String[] names = ticket.getTitle().split("_");
        nameEN.setText(names[0]);
        nameAR.setText(names[1]);
        ticketNumber.setText(ticket.getTicketNum());
        waitingCount.setText(ticket.getTotalTicketWaiting());
        date.setText(CommonMethods.getCurrentDateIssueToken());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
