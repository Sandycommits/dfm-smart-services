package com.DFM;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.DFM.dto.BrokerStatement;
import com.DFM.dto.Company;
import com.DFM.dto.GlobalAccount;
import com.DFM.dto.Login;
import com.DFM.dto.NIN;
import com.DFM.dto.UserCode;
import com.DFM.expandable.GlobalAdapter;
import com.DFM.expandable.GlobalHeader;
import com.DFM.fonts.HelveticaNeueEditText;
import com.DFM.fonts.HelveticaNeueTextView;
import com.DFM.interfaces.OnDateSelectionListener;
import com.DFM.interfaces.OnDialogItemClickListener;
import com.DFM.interfaces.OnGlobalItemClickListener;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.PreferenceConnector;
import com.Wsdl2Code.WebServices.Enquiry.Enquiry;
import com.Wsdl2Code.WebServices.Enquiry.IWsdl2CodeEvents;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GlobalAccountActivity extends BaseActivity implements OnGlobalItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.selectNin)
    HelveticaNeueEditText selectNin;
    @BindView(R.id.selectBroker)
    HelveticaNeueEditText selectBroker;
    @BindView(R.id.selectCompany)
    HelveticaNeueEditText selectCompany;
    @BindView(R.id.globalDate)
    HelveticaNeueTextView globalDate;
    @BindView(R.id.globalRecycler)
    RecyclerView globalRecycler;
    //    @BindView(R.id.globalExpand)
//    ExpandableLayout globalExpand;
    @BindView(R.id.print)
    ImageView print;
    @BindView(R.id.rootView)
    View rootView;
    @BindView(R.id.noData)
    RelativeLayout noData;

    DecimalFormat formatter;
    DecimalFormat sharesFormatter;
    DecimalFormat valueFormatter;
    int PERMISSION_ID = 1;
    private UserCode userCode;
    private int rotationAngle = 0;
    private HashMap<String, ArrayList<Company>> brokerMap = new HashMap<>();
    private HashMap<String, ArrayList<Company>> companyMap = new HashMap<>();
    private String[] permissions = new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
//    private double totalBalance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_global_account);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.global_account_balance));
        print.setVisibility(View.VISIBLE);

        NumberFormat nFormatter = NumberFormat.getNumberInstance(Locale.US);
        NumberFormat nSharesFormatter = NumberFormat.getNumberInstance(Locale.US);
        NumberFormat nValueFormatter = NumberFormat.getNumberInstance(Locale.US);
        formatter = (DecimalFormat) nFormatter;
        formatter.applyPattern("#,###,###.##");
        sharesFormatter = (DecimalFormat) nSharesFormatter;
        sharesFormatter.applyPattern("#,###,###.##");
        valueFormatter = (DecimalFormat) nValueFormatter;
        valueFormatter.applyPattern("#,###,###.###");

        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);
        valueFormatter.setMaximumFractionDigits(3);
        valueFormatter.setMinimumFractionDigits(3);
        loadData();
    }

    private void loadData() {

        brokerMap = CommonMethods.retrieveMapObject(this, PreferenceConnector.BROKER_MAP);
        companyMap = CommonMethods.retrieveMapObject(this, PreferenceConnector.COMPANY_MAP);
        globalDate.setText(CommonMethods.getCurrentDateDisplay());
        userCode = (UserCode) CommonMethods.retrieveObject(this, PreferenceConnector.USER_CODES, new UserCode());
        NIN root = CommonMethods.getRootNin(userCode);
        selectNin.setText(root.getNIN() + "-" + root.getAccountName());
        callService();
    }

    @Override
    @OnClick({R.id.selectNin, R.id.selectBroker, R.id.selectCompany, R.id.globalDate, R.id.print})
    public void onViewClicks(View view) {
        int id = view.getId();
        String nin = CommonMethods.getInitialLiteral(selectNin.getText().toString());
        switch (id) {
            case R.id.selectNin:
                String[] ninStrings = CommonMethods.getNinArray(userCode.getNinList());
                int selectedPosition = CommonMethods.getStringPosition(ninStrings, selectNin.getText().toString());
                CommonMethods.showListDialog(this, ninStrings, new OnDialogItemClickListener() {
                    @Override
                    public void onDialogItem(String selected) {
                        selectNin.setText(selected);
                        selectBroker.setText(getString(R.string.all));
                        selectCompany.setText(getString(R.string.all));
                        callService();
                    }
                }, selectedPosition);
                break;
            case R.id.selectBroker:
                String[] brokerArray = CommonMethods.getBrokerArray(brokerMap.get(nin), language);
                if (brokerArray.length > 0) {
                    int broPosition = CommonMethods.getStringPosition(brokerArray, selectBroker.getText().toString());
                    CommonMethods.showListDialog(this, brokerArray, new OnDialogItemClickListener() {
                        @Override
                        public void onDialogItem(String selected) {
                            selectCompany.setText(getString(R.string.all));
                            selectBroker.setText(selected);
                            if (!selected.equalsIgnoreCase(getString(R.string.all))) {
                                brokerService();
                            } else {
                                callService();
                            }
                        }
                    }, broPosition);
                }
                break;
            case R.id.selectCompany:
                String[] companyArray = CommonMethods.getCompanyArray(companyMap.get(nin), language);
                if (companyArray.length > 0) {
                    int compPosition = CommonMethods.getStringPosition(companyArray, selectCompany.getText().toString());
                    CommonMethods.showListDialog(this, companyArray, new OnDialogItemClickListener() {
                        @Override
                        public void onDialogItem(String selected) {
                            selectBroker.setText(getString(R.string.all));
                            selectCompany.setText(selected);
                            if (!selected.equalsIgnoreCase(getString(R.string.all))) {
                                companyService();
                            } else {
                                callService();
                            }
                        }
                    }, compPosition);
                }
                break;
            case R.id.globalDate:
                CommonMethods.displayDatePickerDialog(this, new OnDateSelectionListener() {
                    @Override
                    public void onDateSelected(String date) {
                        globalDate.setText(date);

                        String broker = selectBroker.getText().toString();
                        String company = selectCompany.getText().toString();
                        String all = getString(R.string.all);
                        if (broker.equalsIgnoreCase(all) && company.equalsIgnoreCase(all)) {
                            callService();
                        } else if (!broker.equalsIgnoreCase(all)) {
                            brokerService();
                        } else if (!company.equalsIgnoreCase(all)) {
                            companyService();
                        }
                    }
                }, true);
                break;
            case R.id.print:
                if (CommonMethods.hasPermissions(this, permissions)) {
                    Bitmap bitmap = CommonMethods.getScreenShot(rootView);
                    CommonMethods.store(bitmap, "dfm_screenshot.jpeg", this);
                } else {
                    ActivityCompat.requestPermissions(this, permissions, PERMISSION_ID);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            Bitmap bitmap = CommonMethods.getScreenShot(rootView);
            CommonMethods.store(bitmap, "dfm_screenshot.jpeg", this);
        }
    }

    private void companyService() {
        if (CommonMethods.isOnline(this)) {
            Enquiry enquiry = new Enquiry(new IWsdl2CodeEvents() {
                @Override
                public void Wsdl2CodeStartedRequest() {

                }

                @Override
                public void Wsdl2CodeFinished(String methodName, Object data) {
                    dismissProgressDialog();
//                    XStream xStream = new XStream(new DomDriver());
                    xStream.processAnnotations(Login.class);
                    Login login = (Login) xStream.fromXML(data.toString());
                    if (login.getStatus().equalsIgnoreCase("valid")) {
                        GlobalAccount globalAccount = login.getData().getBalanceByCompany();
                        ArrayList<BrokerStatement> brokers = globalAccount.getBrokers();
                        if (brokers != null) {
                            noData.setVisibility(View.GONE);
                            bindData(brokers);
                        } else {
                            bindData(new ArrayList<BrokerStatement>());
                            noData.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void Wsdl2CodeFinishedWithException(Exception ex) {
                    dismissProgressDialog();
                }

                @Override
                public void Wsdl2CodeEndedRequest() {

                }
            });
            showProgressDialog();
            Login login = (Login) CommonMethods.retrieveObject(this, PreferenceConnector.LOGIN, new Login());
            String nin = CommonMethods.getInitialLiteral(selectNin.getText().toString());
            String date = CommonMethods.getServiceDateFormat(globalDate.getText().toString());
            String company = CommonMethods.getInitialLiteral(selectCompany.getText().toString());
            try {
                enquiry.BalanceByCompanyAsync(login.getData().getSession(), nin, company, date, "0");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }
    }

    private void brokerService() {

        if (CommonMethods.isOnline(this)) {
            Enquiry enquiry = new Enquiry(new IWsdl2CodeEvents() {
                @Override
                public void Wsdl2CodeStartedRequest() {

                }

                @Override
                public void Wsdl2CodeFinished(String methodName, Object data) {
                    dismissProgressDialog();
//                    XStream xStream = new XStream(new DomDriver());
                    xStream.processAnnotations(Login.class);
                    Login login = (Login) xStream.fromXML(data.toString());
                    if (login.getStatus().equalsIgnoreCase("valid")) {
                        GlobalAccount globalAccount = login.getData().getGlobalAccountFilter();
                        ArrayList<BrokerStatement> brokers = globalAccount.getBrokers();
                        if (brokers != null) {
                            noData.setVisibility(View.GONE);
                            bindData(brokers);
                        } else {
                            bindData(new ArrayList<BrokerStatement>());
                            noData.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void Wsdl2CodeFinishedWithException(Exception ex) {
                    dismissProgressDialog();
                }

                @Override
                public void Wsdl2CodeEndedRequest() {

                }
            });
            showProgressDialog();
            Login login = (Login) CommonMethods.retrieveObject(this, PreferenceConnector.LOGIN, new Login());
            String nin = CommonMethods.getInitialLiteral(selectNin.getText().toString());
            String date = CommonMethods.getServiceDateFormat(globalDate.getText().toString());
            String broker = CommonMethods.getInitialLiteral(selectBroker.getText().toString());
            try {
                enquiry.BalanceByBrokerAsync(login.getData().getSession(), nin, broker, date, "0");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }
    }

    private void callService() {
        if (CommonMethods.isOnline(this)) {
            Enquiry enquiry = new Enquiry(new IWsdl2CodeEvents() {
                @Override
                public void Wsdl2CodeStartedRequest() {

                }

                @Override
                public void Wsdl2CodeFinished(String methodName, Object data) {
                    dismissProgressDialog();
//                    XStream xStream = new XStream(new DomDriver());
                    xStream.processAnnotations(Login.class);
                    Login login = (Login) xStream.fromXML(data.toString());
                    if (login.getStatus().equalsIgnoreCase("valid")) {
                        GlobalAccount globalAccount = login.getData().getGlobalAccount();
                        ArrayList<BrokerStatement> brokers = globalAccount.getBrokers();
                        if (brokers != null) {
                            noData.setVisibility(View.GONE);
                            bindData(brokers);
                        } else {
                            bindData(new ArrayList<BrokerStatement>());
                            noData.setVisibility(View.VISIBLE);
                        }
                    } else {
                        CommonMethods.displayOKDialogue(GlobalAccountActivity.this, getString(R.string.session_expired));
                    }
                }

                @Override
                public void Wsdl2CodeFinishedWithException(Exception ex) {
                    dismissProgressDialog();
                }

                @Override
                public void Wsdl2CodeEndedRequest() {

                }
            });
            showProgressDialog();
            Login login = (Login) CommonMethods.retrieveObject(this, PreferenceConnector.LOGIN, new Login());
            String nin = CommonMethods.getInitialLiteral(selectNin.getText().toString());
            String date = CommonMethods.getServiceDateFormat(globalDate.getText().toString());
            try {
                enquiry.GlobalAccountBalanceAsync(login.getData().getSession(), nin, date, "0");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }
    }

    private void bindData(ArrayList<BrokerStatement> brokerStatements) {

        LinkedHashSet<String> symbols = new LinkedHashSet<>();
        for (BrokerStatement statement : brokerStatements) {
            symbols.add(statement.getSymbol().trim() + " (" + valueFormatter.format(statement.getPrice()) + ")");
        }

        List<GlobalHeader> categories = new ArrayList<>();
        for (String symbol : symbols) {
            String symbolString = symbol.contains("(") ? symbol.split("\\(")[0].trim() : symbol;
            ArrayList<BrokerStatement> tempList = new ArrayList<>();
            for (BrokerStatement statement : brokerStatements) {
                if (symbolString.equalsIgnoreCase(statement.getSymbol().trim())) {
                    tempList.add(statement);
                }
            }
            categories.add(new GlobalHeader(symbol, tempList));
//            tempList.clear();
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        //instantiate your adapter with the list of genres
        GlobalAdapter adapter = new GlobalAdapter(categories, this);
        globalRecycler.setLayoutManager(layoutManager);
        globalRecycler.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onTradeItemClick(BrokerStatement statement) {
        Intent intent = new Intent(GlobalAccountActivity.this, DetailsActivity.class);
        intent.putExtra("details", statement);
        startActivity(intent);
    }
}
