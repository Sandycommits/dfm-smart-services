package com.DFM;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.DFM.dto.BrokerStatement;
import com.DFM.fonts.HelveticaNeueEditText;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView((R.id.frozen))
    HelveticaNeueEditText frozen;
    @BindView((R.id.offered))
    HelveticaNeueEditText offered;
    @BindView((R.id.pending))
    HelveticaNeueEditText pending;
    @BindView((R.id.currentBalance))
    HelveticaNeueEditText currentBalance;
    DecimalFormat formatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.details));

        NumberFormat nFormatter = NumberFormat.getNumberInstance(Locale.US);
        formatter = (DecimalFormat) nFormatter;
        formatter.applyPattern("#,###,###.##");
        BrokerStatement statement = (BrokerStatement) getIntent().getSerializableExtra("details");
        statement.getCurrentBalance();
        frozen.setText(statement.getFrozen());
        offered.setText(statement.getPledged());
        pending.setText(statement.getPending());
        currentBalance.setText(formatter.format(statement.getCurrentBalance()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
