package com.DFM;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.DFM.dto.Attachment;
import com.DFM.dto.Body;
import com.DFM.dto.BrokerStatement;
import com.DFM.dto.CaseData;
import com.DFM.dto.CaseInfoResponse;
import com.DFM.dto.EpayResponse;
import com.DFM.dto.Fee;
import com.DFM.dto.FormInfoResponse;
import com.DFM.dto.GlobalAccount;
import com.DFM.dto.Header;
import com.DFM.dto.IVestorCardRequest;
import com.DFM.dto.IVestorCardResponse;
import com.DFM.dto.Login;
import com.DFM.dto.NIN;
import com.DFM.dto.PayementInfo;
import com.DFM.dto.Record;
import com.DFM.dto.Security;
import com.DFM.dto.Table;
import com.DFM.dto.UserCode;
import com.DFM.fonts.HelveticaNeueBoldTextView;
import com.DFM.fonts.HelveticaNeueButton;
import com.DFM.fonts.HelveticaNeueEditText;
import com.DFM.fonts.HelveticaNeueTextView;
import com.DFM.interfaces.OnDialogItemClickListener;
import com.DFM.interfaces.OnItemClickListener;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.Constants;
import com.DFM.utils.PreferenceConnector;
import com.Wsdl2Code.WebServices.Enquiry.Enquiry;
import com.Wsdl2Code.WebServices.Enquiry.IWsdl2CodeEvents;
import com.Wsdl2Code.WebServices.ePay5.ePay5;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.mapper.MapperWrapper;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FamilyTransfersActivity extends BaseActivity {

    private static final int FROM_EMIRATES_ID = 1;
    private static final int FROM_PASSPORT = 2;
    private static final int FROM_BOOK = 3;
    private static final int TO_EMIRATES_ID = 4;
    private static final int TO_PASSPORT = 5;
    private static final int TO_BOOK = 6;
    private static final int TO_OTHERS = 7;
    private static final int PERMISSION_CODE = 1;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.selectNin)
    HelveticaNeueEditText selectNin;
    @BindView(R.id.investorName)
    HelveticaNeueEditText investorName;
    @BindView(R.id.otherInvestorNumber)
    HelveticaNeueEditText otherInvestorNumber;
    @BindView(R.id.applicationDate)
    HelveticaNeueBoldTextView applicationDate;
    @BindView(R.id.securitiesParent)
    LinearLayout securitiesParent;
    @BindView(R.id.otherInvestorName)
    HelveticaNeueEditText otherInvestorName;
    @BindView(R.id.noShares)
    HelveticaNeueTextView noShares;
    @BindView(R.id.optional)
    LinearLayout optional;
    @BindView(R.id.submit)
    HelveticaNeueButton submit;
    @BindView(R.id.update)
    HelveticaNeueButton update;
    @BindView(R.id.transferorID)
    ImageView transferorID;
    @BindView(R.id.transferorIdClose)
    ImageView transferorIdClose;
    @BindView(R.id.transferorPassport)
    ImageView transferorPassport;
    @BindView(R.id.transferorPassportClose)
    ImageView transferorPassportClose;
    @BindView(R.id.transferorBook)
    ImageView transferorBook;
    @BindView(R.id.transferorBookClose)
    ImageView transferorBookClose;
    @BindView(R.id.emiratesID)
    ImageView emiratesID;
    @BindView(R.id.passport)
    ImageView passport;
    @BindView(R.id.familyBook)
    ImageView familyBook;
    @BindView(R.id.otherDocs)
    ImageView otherDocs;
    @BindView(R.id.idClose)
    ImageView idClose;
    @BindView(R.id.passportClose)
    ImageView passportClose;
    @BindView(R.id.bookClose)
    ImageView bookClose;
    @BindView(R.id.otherClose)
    ImageView otherClose;
    DecimalFormat valueFormatter;
    private UserCode userCode;
    private Login login;
    private int userID;
    private Fee fee;
    private ArrayList<View> rows = new ArrayList<>();
    private String[] permissions = new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA};
    private List<Attachment> attachments = new ArrayList<>();
    private ArrayList<BrokerStatement> securities = new ArrayList<>();
    private ArrayList<Security> formSecurities = new ArrayList<>();
    private ArrayList<BrokerStatement> brokers = new ArrayList<>();
    private Body formData;
    private List<Security> oldSecurities = new ArrayList<>();

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family_transfers);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.family_transfer_form));

        NumberFormat nValueFormatter = NumberFormat.getNumberInstance(Locale.US);
        valueFormatter = (DecimalFormat) nValueFormatter;
        valueFormatter.applyPattern("#,###,###.###");


        valueFormatter.setMaximumFractionDigits(3);
        valueFormatter.setMinimumFractionDigits(3);

        userCode = (UserCode) CommonMethods.retrieveObject(this, PreferenceConnector.USER_CODES, new UserCode());
        login = (Login) CommonMethods.retrieveObject(this, PreferenceConnector.LOGIN, new Login());
        NIN root = CommonMethods.getRootNin(userCode);
        selectNin.setText(root.getNIN());
        selectNin.setTag(root.getNIN() + "-" + root.getAccountName());
        investorName.setText(root.getAccountName());
        userID = Integer.parseInt(login.getData().getUserID());
        Body body = (Body) getIntent().getSerializableExtra("isEdit");
        loadTransferee();
        if (body != null) {
            loadExistent(body);
        } else {
            initialSetup();
        }
    }

    private void loadExistent(Body body) {
        IVestorCardRequest request = new IVestorCardRequest();
        Header header = new Header();
        header.setUserID(userID);
        header.setRefNo(body.getRefNo());
        header.setFormOperation(4);
        CaseData caseData = new CaseData();
        caseData.setCaseID(body.getCrmTrackingNumber());
        caseData.setUserID(userID + "");
        body.setUserID(userID);
        applicationDate.setText(CommonMethods.getAppDateFormat(body.getApplicationDate()));
        body.setApplicationDate(CommonMethods.getIVestorDateFormat(body.getApplicationDate()));
        request.setBody(body);
        request.setCaseData(caseData);
        request.setHeader(header);
        if (CommonMethods.isOnline(this)) {
            showProgressDialog();
            apiService.requestFamilyTransfer(request).enqueue(new Callback<IVestorCardResponse>() {
                @Override
                public void onResponse(Call<IVestorCardResponse> call, Response<IVestorCardResponse> response) {
                    dismissProgressDialog();
                    IVestorCardResponse iVestorCardResponse = response.body();
                    FormInfoResponse formResponse = iVestorCardResponse.getFormInfoResponse();
                    formData = formResponse.getFormData();
                    NIN root = CommonMethods.getRootNin(userCode);
                    selectNin.setText(root.getNIN());
                    selectNin.setTag(root.getNIN() + "-" + root.getAccountName());
                    formData.setInvestorNumber(root.getNIN());
                    investorName.setText(formData.getInvestorName());
                    otherInvestorName.setText(formData.getToInvestorName());
                    otherInvestorNumber.setText(formData.getToInvestorNumber());
                    otherInvestorNumber.setFocusable(false);
                    selectNin.setClickable(false);
                    selectNin.setBackgroundResource(R.drawable.disabled_edittext);
                    investorName.setBackgroundResource(R.drawable.disabled_edittext);
                    otherInvestorName.setBackgroundResource(R.drawable.disabled_edittext);
                    otherInvestorNumber.setBackgroundResource(R.drawable.disabled_edittext);
                    oldSecurities = formData.getSecurities();
                    if (oldSecurities != null && oldSecurities.size() > 0) {
                        loadSecurities();
                    }
                    optional.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFailure(Call<IVestorCardResponse> call, Throwable t) {
                    dismissProgressDialog();
                }
            });
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }
    }

    private void loadSecurities() {

        for (Security security : oldSecurities) {
            View view = getLayoutInflater().inflate(R.layout.security_item, null);
            securitiesParent.addView(view);
            final TextView selectCompany = view.findViewById(R.id.company);
            final EditText securitiesCount = view.findViewById(R.id.securitiesCount);
            final TextView securityPrice = view.findViewById(R.id.securityPrice);
            final TextView closingDate = view.findViewById(R.id.closingDate);
            final TextView fees = view.findViewById(R.id.fees);
            selectCompany.setText(security.getSymbol());
            selectCompany.setClickable(false);
            securitiesCount.setText(security.getShares());
            securitiesCount.setFocusable(false);
            securityPrice.setText(security.getPrice());
            closingDate.setText(security.getClosingDate());
            fees.setText(security.getFees());
        }
        if (formData != null) {
            submit.setVisibility(View.GONE);
            update.setVisibility(View.VISIBLE);
        } else {
            submit.setVisibility(View.VISIBLE);
            update.setVisibility(View.GONE);
        }
    }

    private void loadTransferee() {
        otherInvestorNumber.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        CommonMethods.hideKeyBoard(FamilyTransfersActivity.this);
                        if (CommonMethods.isOnline(FamilyTransfersActivity.this)) {

                            String entered = v.getText().toString();
                            Enquiry accountInfo = new Enquiry(new IWsdl2CodeEvents() {
                                @Override
                                public void Wsdl2CodeStartedRequest() {

                                }

                                @Override
                                public void Wsdl2CodeFinished(String methodName, Object data) {
                                    xStream.processAnnotations(Login.class);
                                    Login login = (Login) xStream.fromXML(data.toString());
                                    Record record = login.getData().getRecord();
                                    if (record != null) {
                                        Table table = record.getTable();
                                        if (table != null) {
                                            otherInvestorName.setText(table.getENNAME());
                                            if (noShares.getVisibility() == View.GONE)
                                                optional.setVisibility(View.VISIBLE);
                                        } else {
                                            CommonMethods.displayOKDialogue(FamilyTransfersActivity.this, getString(R.string.please_enter_valid));
                                            otherInvestorName.setText("");
                                            optional.setVisibility(View.GONE);
                                        }
                                    }
                                }

                                @Override
                                public void Wsdl2CodeFinishedWithException(Exception ex) {

                                }

                                @Override
                                public void Wsdl2CodeEndedRequest() {

                                }
                            });
                            try {
                                accountInfo.GetAccountInfoAsync(login.getData().getSession(), entered);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            CommonMethods.displayOKDialogue(FamilyTransfersActivity.this, getString(R.string.no_internet));
                        }
                        return true;
                    }
                });
    }

    private void initialSetup() {
        applicationDate.setText(CommonMethods.getCurrentDateDisplay());
        final Enquiry enquiry = new Enquiry(new IWsdl2CodeEvents() {
            @Override
            public void Wsdl2CodeStartedRequest() {

            }

            @Override
            public void Wsdl2CodeFinished(String methodName, Object data) {
                dismissProgressDialog();
                xStream.processAnnotations(Login.class);
                Login login = (Login) xStream.fromXML(data.toString());
                if (login.getStatus().equalsIgnoreCase("valid")) {
                    GlobalAccount globalAccount = login.getData().getGlobalAccountFilter();
                    brokers = globalAccount.getBrokers();
                    if (brokers != null) {
                        securitiesParent.removeAllViews();
                        noShares.setVisibility(View.GONE);
//                        optional.setVisibility(View.VISIBLE);
                        loadData(brokers);
                    } else {
                        noShares.setVisibility(View.VISIBLE);
                        optional.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void Wsdl2CodeFinishedWithException(Exception ex) {
                dismissProgressDialog();
            }

            @Override
            public void Wsdl2CodeEndedRequest() {

            }
        });


        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("UserID", userID);
        hashMap.put("ReferenceNum", "MobileCall");
        if (CommonMethods.isOnline(this)) {
            showProgressDialog();
            apiService.getFormsFees(hashMap).enqueue(new Callback<Fee>() {
                @Override
                public void onResponse(Call<Fee> call, Response<Fee> response) {
                    fee = response.body();
                    String nin = CommonMethods.getInitialLiteral(selectNin.getTag().toString());
                    String date = CommonMethods.getCurrentDateOnly();
                    String broker = "CDS";
                    try {
                        enquiry.BalanceByBrokerForFamilyTransferAsync(login.getData().getSession(), nin, broker, date, "0");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Fee> call, Throwable t) {
                    dismissProgressDialog();
                }
            });
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }

    }

    private void loadData(final ArrayList<BrokerStatement> brokers) {
        final String[] companies = getCompaniesArray(brokers);
        for (BrokerStatement security : brokers) {
            View view = getLayoutInflater().inflate(R.layout.security_item, null);
            securitiesParent.addView(view);
            final TextView selectCompany = view.findViewById(R.id.company);
            final EditText securitiesCount = view.findViewById(R.id.securitiesCount);
            final TextView securityPrice = view.findViewById(R.id.securityPrice);
            final TextView closingDate = view.findViewById(R.id.closingDate);
            final TextView fees = view.findViewById(R.id.fees);
            selectCompany.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int selectedItem = CommonMethods.getStringPosition(companies, selectCompany.getText().toString());
                    CommonMethods.showSymbolDialog(FamilyTransfersActivity.this, companies, new OnItemClickListener() {
                        @Override
                        public void onItemClick(int selected) {
                            final BrokerStatement security = brokers.get(selected);
                            selectCompany.setText(security.getSymbol());
                            securitiesCount.setBackgroundResource(R.drawable.enabled_edittext);
                            securitiesCount.setFocusable(true);
                            securitiesCount.setFocusableInTouchMode(true);
                            securitiesCount.setClickable(true);
                            securityPrice.setText(valueFormatter.format(security.getPrice()));
                            closingDate.setText(CommonMethods.getDateInFormat(security.getDate()));
                            securitiesCount.setOnEditorActionListener(
                                    new EditText.OnEditorActionListener() {
                                        @Override
                                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                            CommonMethods.hideKeyBoard(FamilyTransfersActivity.this);
                                            String entered = v.getText().toString();
                                            long shares = Long.parseLong(entered);
                                            long available = security.getAvailable();
                                            double minimumAmount = Double.parseDouble(fee.getMinimumAmount());
                                            if (shares > available) {
                                                CommonMethods.displayOKDialogue(FamilyTransfersActivity.this, getString(R.string.entered_value_is) + " " + available);
                                                securitiesCount.setText(available + "");
                                                fees.setText((long) minimumAmount + "");
                                            } else {
                                                Double percentage = Double.parseDouble(fee.getPercentage());
                                                long totalAmount = (long) (shares * security.getPrice());
                                                double result = totalAmount * percentage;
                                                if (result < minimumAmount) {
                                                    fees.setText((long) minimumAmount + "");
                                                } else {
                                                    fees.setText((long) result + "");
                                                }
                                            }
                                            return true;
                                        }
                                    });
                        }
                    }, selectedItem);
                }
            });
            rows.add(view);
        }

    }

    private String[] getCompaniesArray(ArrayList<BrokerStatement> brokers) {
        String[] temp;
        if (brokers != null) {
            temp = new String[brokers.size()];
            int size = brokers.size();
            for (int i = 0; i < size; i++) {
                temp[i] = brokers.get(i).getSymbol();
            }
            return temp;
        }
        return new String[0];
    }

    @Override
    @OnClick({R.id.selectNin, R.id.submit, R.id.transferorID, R.id.transferorPassport, R.id.transferorBook, R.id.emiratesID, R.id.passport, R.id.familyBook, R.id.otherDocs, R.id.transferorIdClose, R.id.transferorPassportClose, R.id.transferorBookClose, R.id.idClose, R.id.passportClose, R.id.bookClose, R.id.otherClose, R.id.update})
    public void onViewClicks(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.selectNin:
                String[] ninStrings = CommonMethods.getNinArray(userCode.getNinList());
                int selectedPosition = CommonMethods.getStringPosition(ninStrings, selectNin.getText().toString() + "-" + investorName.getText().toString());
                CommonMethods.showListDialog(this, ninStrings, new OnDialogItemClickListener() {
                    @Override
                    public void onDialogItem(String selected) {
                        if (selected.contains("-")) {
                            String content[] = selected.split("-");
                            selectNin.setText(content[0]);
                            investorName.setText(content[1]);
                        } else {
                            selectNin.setText(selected);
                            investorName.setText(selected);
                        }
                        selectNin.setTag(selected);
                        initialSetup();
                    }
                }, selectedPosition);
                break;
            case R.id.transferorID:
                if (CommonMethods.hasPermissions(this, permissions)) {
                    EasyImage.openChooserWithGallery(this, "Select option", FROM_EMIRATES_ID);
                } else {
                    ActivityCompat.requestPermissions(this, permissions, FROM_EMIRATES_ID);
                }
                break;
            case R.id.transferorPassport:
                if (CommonMethods.hasPermissions(this, permissions)) {
                    EasyImage.openChooserWithGallery(this, "Select option", FROM_PASSPORT);
                } else {
                    ActivityCompat.requestPermissions(this, permissions, FROM_PASSPORT);
                }
                break;
            case R.id.transferorBook:
                if (CommonMethods.hasPermissions(this, permissions)) {
                    EasyImage.openChooserWithGallery(this, "Select option", FROM_BOOK);
                } else {
                    ActivityCompat.requestPermissions(this, permissions, FROM_BOOK);
                }
                break;
            case R.id.emiratesID:
                if (CommonMethods.hasPermissions(this, permissions)) {
                    EasyImage.openChooserWithGallery(this, "Select option", TO_EMIRATES_ID);
                } else {
                    ActivityCompat.requestPermissions(this, permissions, TO_EMIRATES_ID);
                }
                break;
            case R.id.passport:
                if (CommonMethods.hasPermissions(this, permissions)) {
                    EasyImage.openChooserWithGallery(this, "Select option", TO_PASSPORT);
                } else {
                    ActivityCompat.requestPermissions(this, permissions, TO_PASSPORT);
                }
                break;
            case R.id.familyBook:
                if (CommonMethods.hasPermissions(this, permissions)) {
                    EasyImage.openChooserWithGallery(this, "Select option", TO_BOOK);
                } else {
                    ActivityCompat.requestPermissions(this, permissions, TO_BOOK);
                }
                break;
            case R.id.otherDocs:
                if (CommonMethods.hasPermissions(this, permissions)) {
                    EasyImage.openChooserWithGallery(this, "Select option", TO_OTHERS);
                } else {
                    ActivityCompat.requestPermissions(this, permissions, TO_OTHERS);
                }
                break;
            case R.id.transferorIdClose:
                transferorIdClose.setVisibility(View.GONE);
                transferorID.setImageResource(R.drawable.ic_clip);
                removeAttachment(0);
                break;
            case R.id.transferorPassportClose:
                transferorPassportClose.setVisibility(View.GONE);
                transferorPassport.setImageResource(R.drawable.ic_clip);
                removeAttachment(1);
                break;
            case R.id.transferorBookClose:
                transferorBookClose.setVisibility(View.GONE);
                transferorBook.setImageResource(R.drawable.ic_clip);
                removeAttachment(2);
                break;
            case R.id.idClose:
                idClose.setVisibility(View.GONE);
                emiratesID.setImageResource(R.drawable.ic_clip);
                removeAttachment(3);
                break;
            case R.id.passportClose:
                passportClose.setVisibility(View.GONE);
                passport.setImageResource(R.drawable.ic_clip);
                removeAttachment(4);
                break;
            case R.id.bookClose:
                bookClose.setVisibility(View.GONE);
                familyBook.setImageResource(R.drawable.ic_clip);
                removeAttachment(5);
                break;
            case R.id.otherClose:
                otherClose.setVisibility(View.GONE);
                otherDocs.setImageResource(R.drawable.ic_clip);
                removeAttachment(6);
                break;
            case R.id.submit:
                if (rows != null) {
                    for (View row : rows) {
                        TextView company = row.findViewById(R.id.company);
                        String companyName = company.getText().toString();
                        if (!companyName.equalsIgnoreCase(getString(R.string.symbol))) {
                            BrokerStatement security = getSecurity(company.getText().toString());
                            TextView feesView = row.findViewById(R.id.fees);
                            EditText sharesView = row.findViewById(R.id.securitiesCount);
                            TextView dateView = row.findViewById(R.id.closingDate);
                            String feesString = feesView.getText().toString();
                            String sharesString = sharesView.getText().toString();
                            if (!feesString.isEmpty()) {
                                long fees = Long.parseLong(feesString);
                                long shares = Long.parseLong(sharesString);
                                security.setFees(fees + "");
                                security.setShares(shares + "");
                                if (shares > 0 && fees > 0) {
                                    security.setDate(dateView.getText().toString());
                                    securities.add(security);
                                }
                            }
                        }
                    }
                    if (securities != null && securities.size() > 0) {
                        Body body = new Body();
                        NIN root = CommonMethods.getRootNin(userCode);
                        body.setApplicationDate(CommonMethods.getCurrentDateOnly());
                        body.setCrmTrackingNumber("");
                        body.setUserID(userID);
                        String selected = selectNin.getTag().toString();
                        if (selected.contains("-")) {
                            String[] investor = selected.split("-");
                            body.setInvestorName(investor[1]);
                            body.setInvestorNumber(investor[0]);
                        } else {
                            body.setInvestorName(root.getAccountName());
                            body.setInvestorNumber(root.getNIN());
                        }
                        String otherInvestor = otherInvestorName.getText().toString();
                        body.setTotalAmount(getTotalFees());
                        if (!otherInvestor.isEmpty()) {
                            body.setToInvestorName(otherInvestor);
                            body.setToInvestorNumber(otherInvestorNumber.getText().toString());
                            submitForm(body);
                        } else {
                            CommonMethods.displayOKDialogue(this, getString(R.string.please_enter_valid));
                        }
                    } else {
                        CommonMethods.displayOKDialogue(this, getString(R.string.please_enter_correct));
                    }
                }
                break;
            case R.id.update:
                updateForm();
                break;
            default:
                break;
        }
    }

    private void removeAttachment(int position) {

        for (int i = 0; i < attachments.size(); i++) {
            Attachment attachment = attachments.get(i);
            if (position == attachment.getPosition())
                attachments.remove(attachment);
        }
    }

    private void updateForm() {
        IVestorCardRequest request = new IVestorCardRequest();
        Header header = new Header();
        header.setUserID(userID);
        header.setRequestID((int) Math.random());
        header.setSessionID("");
        header.setHasAttachments(true);
        header.setRefNo(formData.getRefNo());
        header.setFormOperation(2);

        CaseData caseData = new CaseData();
        caseData.setCaseID(formData.getCrmTrackingNumber());
        caseData.setUserID(userID + "");
        caseData.setCaseSubject("");
        request.setAttachments(loadAttachments());

        formData.setSecurities(null);
        request.setBody(formData);
        request.setCaseData(caseData);
        request.setHeader(header);
        if (oldSecurities != null && oldSecurities.size() > 0)
            request.setSecurities(oldSecurities);
        if (CommonMethods.isOnline(this)) {
            showProgressDialog();
            apiService.requestFamilyTransfer(request).enqueue(new Callback<IVestorCardResponse>() {
                @Override
                public void onResponse(Call<IVestorCardResponse> call, Response<IVestorCardResponse> response) {
                    IVestorCardResponse cardResponse = response.body();
                    FormInfoResponse formInfo = cardResponse.getFormInfoResponse();
                    PayementInfo payementInfo = formInfo.getPayementInfo();
                    if (payementInfo != null && payementInfo.getPaymentAmount() > 0) {

                    } else {
                        CaseInfoResponse caseInfo = cardResponse.getCaseInfoResponse();
                        Intent intent = new Intent(FamilyTransfersActivity.this, TrackRequestActivity.class);
                        startActivity(intent);
                        FamilyTransfersActivity.this.finish();
                    }
                }

                @Override
                public void onFailure(Call<IVestorCardResponse> call, Throwable t) {
                    dismissProgressDialog();
                }
            });
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }
    }

    private BrokerStatement getSecurity(String company) {
        for (BrokerStatement security : brokers) {
            if (security.getSymbol().equalsIgnoreCase(company)) {
                return security;
            }
        }
        return new BrokerStatement();
    }

    private void submitForm(Body body) {

        IVestorCardRequest request = new IVestorCardRequest();
        Header header = new Header();
        header.setUserID(userID);
        header.setRequestID((int) Math.random());
        header.setSessionID("");
        header.setHasAttachments(true);
        header.setRefNo("");
        header.setFormOperation(1);

        CaseData caseData = new CaseData();
        caseData.setCaseID("");
        caseData.setUserID(userID + "");
        caseData.setCaseSubject("");
        request.setAttachments(loadAttachments());

        request.setBody(body);
        request.setCaseData(caseData);
        request.setHeader(header);
        request.setSecurities(getSecurities());
        if (CommonMethods.isOnline(this)) {
            String json = new Gson().toJson(request);
            showProgressDialog();
            apiService.requestFamilyTransfer(request).enqueue(new Callback<IVestorCardResponse>() {
                @Override
                public void onResponse(Call<IVestorCardResponse> call, Response<IVestorCardResponse> response) {
                    try {
                        IVestorCardResponse cardResponse = response.body();
                        FormInfoResponse formInfo = cardResponse.getFormInfoResponse();
                        PayementInfo payementInfo = formInfo.getPayementInfo();
                        if (payementInfo != null && payementInfo.getPaymentAmount() > 0) {
                            String text = "CUSTID=" + userID + "&UserName=" + login.getUserName() + "&USERID=" + userID + "&ServiceCode=MOB&PaymentAmount=" + payementInfo.getPaymentAmount() + "&isCaseCreation=true&RefNo=" + formInfo.getReferenceNumber() + "&TRName=Transfers: Family Transfer (up to 2nd degree kinship)&SavedFormUrl=" + formInfo.getUrl() + "?CUSTID=" + userID + "&Lang=en&OtpRequired=false";
                            ePay5 ePay5 = new ePay5(new com.Wsdl2Code.WebServices.ePay5.IWsdl2CodeEvents() {
                                @Override
                                public void Wsdl2CodeStartedRequest() {

                                }

                                @Override
                                public void Wsdl2CodeFinished(String methodName, Object data) {
                                    dismissProgressDialog();
                                    XStream stream = new XStream(new DomDriver()) {
                                        // to enable ignoring of unknown elements
                                        @Override
                                        protected MapperWrapper wrapMapper(MapperWrapper next) {
                                            return new MapperWrapper(next) {
                                                @Override
                                                public boolean shouldSerializeMember(Class definedIn, String fieldName) {
                                                    if (definedIn == Object.class) {
                                                        try {
                                                            return this.realClass(fieldName) != null;
                                                        } catch (Exception e) {
                                                            return false;
                                                        }
                                                    }
                                                    return super.shouldSerializeMember(definedIn, fieldName);
                                                }
                                            };
                                        }
                                    };
                                    stream.processAnnotations(EpayResponse.class);
                                    EpayResponse epayResponse = (EpayResponse) stream.fromXML(data.toString());
                                    if (login.getStatus().equalsIgnoreCase("valid")) {
                                        String encoded = Constants.EPAY_URL + epayResponse.getData();
                                /*Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(encoded));
                                startActivity(intent);*/

                                        Intent intent = new Intent(FamilyTransfersActivity.this, PaymentActivity.class);
                                        intent.putExtra("payment", encoded);
                                        startActivity(intent);
                                        FamilyTransfersActivity.this.finish();
                                    } else {
                                        CommonMethods.displayOKDialogue(FamilyTransfersActivity.this, getString(R.string.something_wrong));
                                    }
                                }

                                @Override
                                public void Wsdl2CodeFinishedWithException(Exception ex) {
                                    dismissProgressDialog();
                                }

                                @Override
                                public void Wsdl2CodeEndedRequest() {

                                }
                            });
                            try {
                                ePay5.ePay5EncryptionNewAsync(login.getData().getSession(), text);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            CaseInfoResponse caseInfo = cardResponse.getCaseInfoResponse();
                            Intent intent = new Intent(FamilyTransfersActivity.this, TrackRequestActivity.class);
                            startActivity(intent);
                            FamilyTransfersActivity.this.finish();
                        }
                    } catch (Exception e) {

                    }
                }

                @Override
                public void onFailure(Call<IVestorCardResponse> call, Throwable t) {
                    dismissProgressDialog();
                }
            });
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }
    }

    private List<Attachment> loadAttachments() {

        int size = attachments.size();
        List<Attachment> finalAttachments = new ArrayList<>();
        boolean flag = false;
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < size; j++) {
                Attachment attachment = attachments.get(j);
                if (i == attachment.getPosition()) {
                    finalAttachments.add(attachment);
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                Attachment emptyAttachment = new Attachment();
                emptyAttachment.setDocumentName("");
                emptyAttachment.setFileBytes("");
                emptyAttachment.setFileName("");
                emptyAttachment.setFileType("");
                finalAttachments.add(emptyAttachment);
            } else {
                flag = false;
            }
        }
        return finalAttachments;
    }

    private List<Security> getSecurities() {
        int count = 1;
        for (BrokerStatement statement : securities) {
            Security security = new Security();
            security.setFees(statement.getFees());
            security.setClosingDate(statement.getDate());
            security.setPrice(statement.getPrice() + "");
            security.setShares(statement.getShares());
            security.setCompany(statement.getSymbol());
            security.setRow(count + "");
            security.setAvailable(statement.getAvailable() + "");
            security.setSymbol(statement.getSymbol());
            security.setCurrency(statement.getCurrency());
            formSecurities.add(security);
            count++;
        }
        return formSecurities;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == FROM_EMIRATES_ID) {
                EasyImage.openChooserWithGallery(this, "Select option", FROM_EMIRATES_ID);
            } else if (requestCode == FROM_PASSPORT) {
                EasyImage.openChooserWithGallery(this, "Select option", FROM_PASSPORT);
            } else if (requestCode == FROM_BOOK) {
                EasyImage.openChooserWithGallery(this, "Select option", FROM_BOOK);
            } else if (requestCode == TO_EMIRATES_ID) {
                EasyImage.openChooserWithGallery(this, "Select option", TO_EMIRATES_ID);
            } else if (requestCode == TO_PASSPORT) {
                EasyImage.openChooserWithGallery(this, "Select option", TO_PASSPORT);
            } else if (requestCode == TO_BOOK) {
                EasyImage.openChooserWithGallery(this, "Select option", TO_BOOK);
            } else if (requestCode == TO_OTHERS) {
                EasyImage.openChooserWithGallery(this, "Select option", TO_OTHERS);
            }
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new EasyImage.Callbacks() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource imageSource, int i) {
                CommonMethods.displayOKDialogue(FamilyTransfersActivity.this, getString(R.string.something_wrong));
            }

            @Override
            public void onImagePicked(File file, EasyImage.ImageSource imageSource, int i) {
                String filePath = file.getPath();
                Bitmap originalBitmap = BitmapFactory.decodeFile(filePath);

                Bitmap bitmap = CommonMethods.getRotatedBitMap(originalBitmap, filePath);


                Attachment attachment = new Attachment();
                if (i == FROM_EMIRATES_ID) {
                    removeAttachment(0);
                    transferorIdClose.setVisibility(View.VISIBLE);
                    transferorID.setImageBitmap(bitmap);
                    attachment.setDocumentName("Transferor Emirates ID");
                    attachment.setPosition(0);
                } else if (i == FROM_PASSPORT) {
                    removeAttachment(1);
                    transferorPassportClose.setVisibility(View.VISIBLE);
                    transferorPassport.setImageBitmap(bitmap);
                    attachment.setDocumentName("Transferor Passport");
                    attachment.setPosition(1);
                } else if (i == FROM_BOOK) {
                    removeAttachment(2);
                    transferorBookClose.setVisibility(View.VISIBLE);
                    transferorBook.setImageBitmap(bitmap);
                    attachment.setDocumentName("Transferor Bank Statement");
                    attachment.setPosition(2);
                } else if (i == TO_EMIRATES_ID) {
                    removeAttachment(3);
                    idClose.setVisibility(View.VISIBLE);
                    emiratesID.setImageBitmap(bitmap);
                    attachment.setDocumentName("Transferee Emirates ID");
                    attachment.setPosition(3);
                } else if (i == TO_PASSPORT) {
                    removeAttachment(4);
                    passportClose.setVisibility(View.VISIBLE);
                    passport.setImageBitmap(bitmap);
                    attachment.setDocumentName("Transferee Passport");
                    attachment.setPosition(4);
                } else if (i == TO_BOOK) {
                    removeAttachment(5);
                    bookClose.setVisibility(View.VISIBLE);
                    familyBook.setImageBitmap(bitmap);
                    attachment.setDocumentName("Transferee Bank Statement");
                    attachment.setPosition(5);
                } else if (i == TO_OTHERS) {
                    removeAttachment(6);
                    otherClose.setVisibility(View.VISIBLE);
                    otherDocs.setImageBitmap(bitmap);
                    attachment.setDocumentName("Transferee Other");
                    attachment.setPosition(6);
                }
                attachment.setFileBytes(CommonMethods.bitmapToBase64(bitmap));
                attachment.setFileName(file.getName());
                attachment.setFileType("jpeg");
                attachments.add(attachment);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource imageSource, int i) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public long getTotalFees() {
        long total = 0;
        for (BrokerStatement security : securities) {
            total += Long.parseLong(security.getFees());
        }
        return total;
    }
}
