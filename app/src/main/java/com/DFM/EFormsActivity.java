package com.DFM;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.DFM.adapters.StatementRecyclerAdapter;
import com.DFM.interfaces.OnItemClickListener;
import com.DFM.utils.CommonMethods;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EFormsActivity extends BaseActivity implements OnItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.statementRecycler)
    RecyclerView statementRecycler;
    private int[] drawables = new int[]{R.drawable.ic_ivestor, R.drawable.ic_cashdivident, R.drawable.ic_family};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eforms);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.e_forms));
        loadItems();
    }

    private void loadItems() {
        String[] categories = getResources().getStringArray(R.array.eforms_list);
        StatementRecyclerAdapter adapter = new StatementRecyclerAdapter(this, Arrays.asList(categories), language, drawables);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        statementRecycler.setHasFixedSize(true);
        statementRecycler.setLayoutManager(layoutManager);
        statementRecycler.setAdapter(adapter);
    }

    @Override
    public void onViewClicks(View view) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onItemClick(int position) {
        if (CommonMethods.isOnline(this)) {
            Intent intent;
            switch (position) {
                case 0:
                    if (CommonMethods.isOnline(this)) {
                        intent = new Intent(this, IVestorCardActivity.class);
                        startActivity(intent);
                    } else {
                        CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
                    }
                    break;
                case 1:
                    if (CommonMethods.isOnline(this)) {
                        intent = new Intent(this, CashDividendsActivity.class);
                        startActivity(intent);
                    } else {
                        CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
                    }
                    break;
                case 2:
                    if (CommonMethods.isOnline(this)) {
                        intent = new Intent(this, FamilyTransfersActivity.class);
                        startActivity(intent);
                    } else {
                        CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
                    }
                    break;
                default:
                    break;
            }
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }
    }
}
