package com.DFM;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.DFM.adapters.TokenRecyclerAdapter;
import com.DFM.interfaces.DialogListener;
import com.DFM.interfaces.OnItemClickListener;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.PreferenceConnector;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TokenNumberActivity extends BaseActivity implements OnItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tokenRecycler)
    RecyclerView tokenRecycler;
    List<String> categorySymbols = new ArrayList<>();
    private int count = 0;
    private long previousTimeStamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_token_number);
        ButterKnife.bind(this);
        count = PreferenceConnector.readInteger(this, PreferenceConnector.TICKET_COUNT, 0);
        previousTimeStamp = PreferenceConnector.readLong(this, PreferenceConnector.PREVIOUS_TIME, 0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.issue_token_number));
        loadTokenData();
    }

    @Override
    public void onViewClicks(View view) {

    }

    private void loadTokenData() {

        Intent intent = getIntent();
        categorySymbols = intent.getStringArrayListExtra("symbols");
        List<String> categories = intent.getStringArrayListExtra("categories");
        TokenRecyclerAdapter adapter = new TokenRecyclerAdapter(TokenNumberActivity.this, categories);
        LinearLayoutManager layoutManager = new LinearLayoutManager(TokenNumberActivity.this);
        tokenRecycler.setHasFixedSize(true);
        tokenRecycler.addItemDecoration(new DividerItemDecoration(TokenNumberActivity.this, DividerItemDecoration.VERTICAL));
        tokenRecycler.setLayoutManager(layoutManager);
        tokenRecycler.setAdapter(adapter);

/*        MiscFunctions miscFunctions = new MiscFunctions(new IWsdl2CodeEvents() {
            @Override
            public void Wsdl2CodeStartedRequest() {

            }

            @Override
            public void Wsdl2CodeFinished(String methodName, Object data) {
                dismissProgressDialog();
                XStream xStream = new XStream(new DomDriver());
                xStream.processAnnotations(Login.class);
                Login login = (Login) xStream.fromXML(data.toString());
                List<String> categories = login.getData().getCategory().getCategories();
                categorySymbols = login.getData().getCategory().getCategoryIdList();
                TokenRecyclerAdapter adapter = new TokenRecyclerAdapter(TokenNumberActivity.this, categories);
                LinearLayoutManager layoutManager = new LinearLayoutManager(TokenNumberActivity.this);
                tokenRecycler.setHasFixedSize(true);
                tokenRecycler.addItemDecoration(new DividerItemDecoration(TokenNumberActivity.this, DividerItemDecoration.VERTICAL));
                tokenRecycler.setLayoutManager(layoutManager);
                tokenRecycler.setAdapter(adapter);
            }

            @Override
            public void Wsdl2CodeFinishedWithException(Exception ex) {
                dismissProgressDialog();
            }

            @Override
            public void Wsdl2CodeEndedRequest() {

            }
        });
        showProgressDialog();
        try {
            miscFunctions.IssueTokenListAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onItemClick(int position) {
        long currentTimestamp = SystemClock.uptimeMillis();
        if (currentTimestamp - previousTimeStamp > 300000) {
            count = 0;
            previousTimeStamp = currentTimestamp;
            PreferenceConnector.writeLong(this, PreferenceConnector.PREVIOUS_TIME, previousTimeStamp);
        }
        if (count < 2) {
            final String category = categorySymbols.get(position);
            if (category.equalsIgnoreCase("FTS")) {
                CommonMethods.displayConfirmationDialogue(this, getString(R.string.fast_track_msg), new DialogListener() {
                    @Override
                    public void onConfirmation(boolean isConfirmed) {
                        Intent intent = new Intent(TokenNumberActivity.this, TokenDetailsActivity.class);
                        intent.putExtra("category", category);
                        startActivity(intent);
                        count += 1;
                        PreferenceConnector.writeInteger(TokenNumberActivity.this, PreferenceConnector.TICKET_COUNT, count);
                    }
                });
            } else {
                Intent intent = new Intent(this, TokenDetailsActivity.class);
                intent.putExtra("category", category);
                startActivity(intent);
                count += 1;
                PreferenceConnector.writeInteger(this, PreferenceConnector.TICKET_COUNT, count);
            }
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.you_are_permitted));
        }
    }
}
