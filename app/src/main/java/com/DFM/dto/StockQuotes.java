package com.DFM.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Sandeep on 05/11/17.
 */

public class StockQuotes {


    @XStreamAlias("Record")
    private Record record;

    public Record getRecord() {
        return record;
    }

    public void setRecord(Record record) {
        this.record = record;
    }
}
