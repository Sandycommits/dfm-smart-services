package com.DFM.dto;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;

/**
 * Created by Sandeep on 30/10/17.
 */

public class UserCode {

    @XStreamImplicit(itemFieldName = "NINs")
    private ArrayList<NIN> ninList;
    @XStreamImplicit(itemFieldName = "Companies")
    private ArrayList<Company> companyList;
    @XStreamImplicit(itemFieldName = "Brokers")
    private ArrayList<Company> brokersList;

    public ArrayList<NIN> getNinList() {
        return ninList;
    }

    public void setNinList(ArrayList<NIN> ninList) {
        this.ninList = ninList;
    }

    public ArrayList<Company> getCompanyList() {
        return companyList;
    }

    public void setCompanyList(ArrayList<Company> companyList) {
        this.companyList = companyList;
    }

    public ArrayList<Company> getBrokersList() {
        return brokersList;
    }

    public void setBrokersList(ArrayList<Company> brokersList) {
        this.brokersList = brokersList;
    }
}
