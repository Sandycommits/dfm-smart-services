package com.DFM.dto;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;

/**
 * Created by Sandeep on 27/11/17.
 */

public class TradeDetails {

    @XStreamImplicit(itemFieldName = "Record")
    private ArrayList<TradeRecord> records;

    public ArrayList<TradeRecord> getRecords() {
        return records;
    }
}
