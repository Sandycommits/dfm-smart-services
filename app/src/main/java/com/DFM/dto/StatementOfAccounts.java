package com.DFM.dto;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;

/**
 * Created by Sandeep on 29/11/17.
 */

public class StatementOfAccounts {

    @XStreamImplicit(itemFieldName = "Record")
    private ArrayList<AccountRecord> records;

    public ArrayList<AccountRecord> getRecords() {
        return records;
    }
}
