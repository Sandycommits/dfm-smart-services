package com.DFM.dto;

import java.io.Serializable;

/**
 * Created by Sandeep on 15/11/17.
 */

public class BrokerStatement implements Serializable {

    private String BrokerCode, BrokerNameEn, BrokerNameAr, Symbol, SecurityNameEn, SecurityNameAr, Account, Frozen, Pledged, Pending, Currency, Date, shares, fees;

    private long CurrentBalance, Available;
    private double Price;

    public long getCurrentBalance() {
        return CurrentBalance;
    }

    public double getPrice() {
        return Price;
    }

    public String getBrokerCode() {
        return BrokerCode;
    }

    public String getSymbol() {
        return Symbol.trim();
    }

    public String getAccount() {
        return Account;
    }

    public String getBrokerNameEn() {
        return BrokerNameEn;
    }

    public String getFrozen() {
        return Frozen;
    }

    public String getPledged() {
        return Pledged;
    }

    public String getPending() {
        return Pending;
    }

    public String getSecurityNameEn() {
        return SecurityNameEn;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public long getAvailable() {
        return Available;
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getShares() {
        return shares;
    }

    public void setShares(String shares) {
        this.shares = shares;
    }

    public String getCurrency() {
        return Currency;
    }
}
