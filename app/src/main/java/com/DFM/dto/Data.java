package com.DFM.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;

/**
 * Created by Sandeep on 11/10/17.
 */

public class Data {
    private String LoginType;

    private String Session;

    private String UserID;

    private String Description;

    private String EmailCode;

    private String SMSCode;

    private String OfficeClosedEN;

    private String OfficeClosedAR;
    @XStreamImplicit(itemFieldName = "IconView")
    private ArrayList<Icon> icons;
    @XStreamAlias("Broker_Lst")
    private BrokerData brokerData;
    @XStreamAlias("Record")
    private Record record;
    @XStreamAlias("UserCodes")
    private UserCode userCodes;
    @XStreamAlias("StockQuotes")
    private StockQuotes stockQuotes;
    @XStreamAlias("Category")
    private Category category;
    @XStreamAlias("IssueTicket")
    private Ticket ticket;
    @XStreamAlias("BalanceByBroker")
    private GlobalAccount globalAccountFilter;
    @XStreamAlias("BalanceByCompany")
    private GlobalAccount balanceByCompany;
    @XStreamAlias("GlobalAccountBalance")
    private GlobalAccount globalAccount;
    @XStreamAlias("GetiVESTORStatement")
    private IVestor iVestor;
    @XStreamAlias("CashDividendOverview")
    private CashDividend cashDividend;
    @XStreamAlias("CashDividendByCompany")
    private CashDividend dividendByCompany;
    @XStreamAlias("InvestorMovementDetails")
    private MovementDetails movementDetails;
    @XStreamAlias("InvestorTradeDetails")
    private TradeDetails tradeDetails;
    @XStreamAlias("StatementOfAccounts")
    private StatementOfAccounts accounts;

    public String getOfficeClosedEN() {
        return OfficeClosedEN;
    }

    public void setOfficeClosedEN(String officeClosedEN) {
        OfficeClosedEN = officeClosedEN;
    }

    public String getOfficeClosedAR() {
        return OfficeClosedAR;
    }

    public void setOfficeClosedAR(String officeClosedAR) {
        OfficeClosedAR = officeClosedAR;
    }

    public StatementOfAccounts getAccounts() {
        return accounts;
    }

    public TradeDetails getTradeDetails() {
        return tradeDetails;
    }

    public MovementDetails getMovementDetails() {
        return movementDetails;
    }

    public CashDividend getDividendByCompany() {
        return dividendByCompany;
    }

    public CashDividend getCashDividend() {
        return cashDividend;
    }

    public GlobalAccount getGlobalAccountFilter() {
        return globalAccountFilter;
    }

    public GlobalAccount getBalanceByCompany() {
        return balanceByCompany;
    }

    public IVestor getiVestor() {
        return iVestor;
    }

    public GlobalAccount getGlobalAccount() {
        return globalAccount;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public Category getCategory() {
        return category;
    }

    public StockQuotes getStockQuotes() {
        return stockQuotes;
    }

    public Record getRecord() {
        return record;
    }

    public void setRecord(Record record) {
        this.record = record;
    }

    public UserCode getUserCodes() {
        return userCodes;
    }

    public void setUserCodes(UserCode userCodes) {
        this.userCodes = userCodes;
    }

    public BrokerData getBrokerData() {
        return brokerData;
    }

    public void setBrokerData(BrokerData brokerData) {
        this.brokerData = brokerData;
    }

    public ArrayList<Icon> getIcons() {
        return icons;
    }

    public void setIcons(ArrayList<Icon> icons) {
        this.icons = icons;
    }

    public String getEmailCode() {
        return EmailCode;
    }

    public void setEmailCode(String EmailCode) {
        this.EmailCode = EmailCode;
    }

    public String getSMSCode() {
        return SMSCode;
    }

    public void setSMSCode(String SMSCode) {
        this.SMSCode = SMSCode;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getLoginType() {
        return LoginType;
    }

    public void setLoginType(String LoginType) {
        this.LoginType = LoginType;
    }

    public String getSession() {
        return Session;
    }

    public void setSession(String Session) {
        this.Session = Session;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String UserID) {
        this.UserID = UserID;
    }
}
