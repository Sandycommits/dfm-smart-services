package com.DFM.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Sandeep on 15/10/17.
 */
@XStreamAlias("XML")
public class ValidateEmailPhone {

    @XStreamAlias("Status")
    private String status;
    @XStreamAlias("Data")
    private String Data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getData() {
        return Data;
    }

    public void setData(String Data) {
        this.Data = Data;
    }
}
