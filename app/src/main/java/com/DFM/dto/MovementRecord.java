package com.DFM.dto;

import java.io.Serializable;

/**
 * Created by Sandeep on 27/11/17.
 */

public class MovementRecord implements Serializable {

    private String Tran_Date, FRM_MEMBER, TO_MEMBER, FROM_NIN, TO_NIN, FRM_ACCNT, TO_ACCOUNT, SYMBOL1, REGTRF_NUM, FROM_NAME, TO_NAME, FRM_REFER, OriginFrom, OriginStatus;
    private long VOLUME;

    private String FRM_MEMBERNameEn, FRM_MEMBERNameAr, TO_MEMBERNameEn, TO_MEMBERNameAr, TO_REFER, SYMBOL1NameEn, SYMBOL1NameAr, SYMBOL2, SYMBOL2NameEn, SYMBOL2NameAr, FROM_NAME_EN, APPROVL_FG, POSTED_FG, Currency1, Currency2, TO_NAME_EN, T;

    public String getDate() {
        return Tran_Date;
    }

    public String getFromMember() {
        return FRM_MEMBER.trim();
    }

    public String getToMember() {
        return TO_MEMBER.trim();
    }

    public String getFromNIN() {
        return FROM_NIN;
    }

    public String getToNIN() {
        return TO_NIN;
    }

    public String getFromAccount() {
        return FRM_ACCNT;
    }

    public String getToAccount() {
        return TO_ACCOUNT;
    }

    public String getCompany() {
        return SYMBOL1;
    }

    public String getTransaction() {
        return REGTRF_NUM;
    }

    public String getFromName() {
        return FROM_NAME;
    }

    public String getToName() {
        return TO_NAME;
    }

    public String getReference() {
        return FRM_REFER.trim();
    }

    public String getToReference() {
        return TO_REFER.trim();
    }

    public long getQuantity() {
        return VOLUME;
    }

    public String getOrigin() {
        return OriginFrom + "-" + OriginStatus;
    }
}
