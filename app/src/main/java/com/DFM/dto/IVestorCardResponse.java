package com.DFM.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by macadmin on 12/12/17.
 */

public class IVestorCardResponse {

    @SerializedName("HeaderResponse")
    @Expose
    private HeaderResponse headerResponse;

    @SerializedName("FormInfoResponse")
    @Expose
    private FormInfoResponse formInfoResponse;

    @SerializedName("CaseInfoResponse")
    @Expose
    private CaseInfoResponse caseInfoResponse;

    public HeaderResponse getHeaderResponse() {
        return headerResponse;
    }

    public FormInfoResponse getFormInfoResponse() {
        return formInfoResponse;
    }

    public CaseInfoResponse getCaseInfoResponse() {
        return caseInfoResponse;
    }
}
