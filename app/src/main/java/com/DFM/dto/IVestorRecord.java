package com.DFM.dto;

/**
 * Created by Sandeep on 21/11/17.
 */

public class IVestorRecord {

    private long ID;
    private String Nin, AccountNo, ValueDate, Date, Description;
    private Double Debit, Credit, TotalBalance, ClosingBalance, Balance, iVESTORCardBalance;

    public String getDescription() {
        return Description;
    }

    public Double getDebit() {
        return Debit;
    }

    public Double getCredit() {
        return Credit;
    }

    public Double getTotalBalance() {
        return TotalBalance;
    }

    public Double getClosingBalance() {
        return ClosingBalance;
    }

    public String getValueDate() {
        return ValueDate;
    }

    public Double getiVESTORCardBalance() {
        return iVESTORCardBalance;
    }

    public Double getBalance() {
        return Balance;
    }

    public String getDate() {
        return Date;
    }
}
