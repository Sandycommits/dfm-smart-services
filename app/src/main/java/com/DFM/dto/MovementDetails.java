package com.DFM.dto;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;

/**
 * Created by Sandeep on 27/11/17.
 */

public class MovementDetails {

    @XStreamImplicit(itemFieldName = "Record")
    private ArrayList<MovementRecord> records;

    public ArrayList<MovementRecord> getRecords() {
        return records;
    }
}
