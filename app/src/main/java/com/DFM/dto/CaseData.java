package com.DFM.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Created by macadmin on 12/12/17.
 */

public class CaseData {

    @SerializedName("CaseID")
    private String caseID;

    @SerializedName("CaseSubject")
    private String caseSubject;

    @SerializedName("UserID")
    private String userID;

    public void setCaseID(String caseID) {
        this.caseID = caseID;
    }

    public void setCaseSubject(String caseSubject) {
        this.caseSubject = caseSubject;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
