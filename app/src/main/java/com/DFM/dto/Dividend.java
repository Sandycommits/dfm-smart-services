package com.DFM.dto;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sandeep on 23/11/17.
 */

public class Dividend implements Parcelable {

    public static final Creator<Dividend> CREATOR = new Creator<Dividend>() {
        @Override
        public Dividend createFromParcel(Parcel in) {
            return new Dividend(in);
        }

        @Override
        public Dividend[] newArray(int size) {
            return new Dividend[size];
        }
    };
    private String NIN, ACCOUNT, MEMBER, BROKERNAME_EN, BROKERNAME_AR, SYMBOL, COMPANYNAME_EN, COMPANYNAME_AR, CORPACTNUM, ADATE, BASEDVOLUME, CURRENCY, NOTES, N2, CASHDIVIDEND;

    protected Dividend(Parcel in) {
        SYMBOL = in.readString();
    }

    public String getSYMBOL() {
        return SYMBOL;
    }

    public String getBROKERNAME_EN() {
        return BROKERNAME_EN;
    }

    public String getBASEDVOLUME() {
        return BASEDVOLUME;
    }

    public String getNOTES() {
        return NOTES;
    }

    public String getCASHDIVIDEND() {
        return CASHDIVIDEND;
    }

    public String getCURRENCY() {
        return CURRENCY;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
