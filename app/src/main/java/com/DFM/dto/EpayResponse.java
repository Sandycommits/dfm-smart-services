package com.DFM.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by macadmin on 12/26/17.
 */
@XStreamAlias("XML")
public class EpayResponse {

    @XStreamAlias("Status")
    private String status;
    @XStreamAlias("Data")
    private String data;

    public String getStatus() {
        return status;
    }

    public String getData() {
        return data;
    }
}
