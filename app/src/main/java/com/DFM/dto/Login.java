package com.DFM.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Sandeep on 11/10/17.
 */
@XStreamAlias("XML")
public class Login {

    private String userName, password;
    private boolean isRemember;
    @XStreamAlias("Status")
    private String status;
    @XStreamAlias("Data")
    private Data data;
    /*@XStreamAlias("Data")
    private String info;

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }*/

    public boolean isRemember() {
        return isRemember;
    }

    public void setRemember(boolean remember) {
        isRemember = remember;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public com.DFM.dto.Data getData() {
        return data;
    }

    public void setData(com.DFM.dto.Data data) {
        this.data = data;
    }
}
