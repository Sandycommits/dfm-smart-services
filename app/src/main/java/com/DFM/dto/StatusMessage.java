package com.DFM.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by macadmin on 12/12/17.
 */

public class StatusMessage {

    @SerializedName("English")
    @Expose
    private String status;

    public String getStatus() {
        return status;
    }
}
