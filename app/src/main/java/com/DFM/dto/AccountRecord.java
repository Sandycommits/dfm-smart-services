package com.DFM.dto;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sandeep on 29/11/17.
 */

public class AccountRecord implements Parcelable {

    public static final Creator<AccountRecord> CREATOR = new Creator<AccountRecord>() {
        @Override
        public AccountRecord createFromParcel(Parcel in) {
            return new AccountRecord(in);
        }

        @Override
        public AccountRecord[] newArray(int size) {
            return new AccountRecord[size];
        }
    };
    private String BrokerCode, Symbol, BrokerNameEn, BrokerNameAr, SecurityNameEn, SecurityNameAr, TransDate, CompDate, Trans_Desc, Currency, GS7IFN, InVolume, OutVolume, Account;
    private long TotalBalance = 0;
    private int ID, flag;

    protected AccountRecord(Parcel in) {
        Account = in.readString();
        TransDate = in.readString();
        CompDate = in.readString();
        Trans_Desc = in.readString();
    }

    public String getBrokerCode() {
        return BrokerCode;
    }

    public String getSymbol() {
        return Symbol;
    }

    public String getTransDate() {
        return TransDate;
    }

    public String getCompDate() {
        return CompDate;
    }

    public String getTrans_Desc() {
        return Trans_Desc;
    }

    public String getAccount() {
        return Account;
    }

    public String getInVolume() {
        return InVolume;
    }

    public String getOutVolume() {
        return OutVolume;
    }

    public long getTotalBalance() {
        return TotalBalance;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(Account);
        parcel.writeString(TransDate);
        parcel.writeString(CompDate);
        parcel.writeString(Trans_Desc);
    }
}
