package com.DFM.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by macadmin on 12/12/17.
 */

public class Body implements Serializable {

    @SerializedName("AppDate")
    private String appDate;

    @SerializedName("ApplicationDate")
    private String applicationDate;

    @SerializedName("Comments")
    private String comments;

    @SerializedName("CRMTrackingNumber")
    private String crmTrackingNumber;

    @SerializedName("InvestorName")
    private String investorName;

    @SerializedName("InvestorNumber")
    private String investorNumber;

    @SerializedName("TransfereeInvestorName")
    private String toInvestorName;

    @SerializedName("TransfereeInvestorNumber")
    private String toInvestorNumber;

    @SerializedName("ReceiveCashDividendsVia")
    private String dividendsVia;

    @SerializedName("BankName")
    private String bankName;

    @SerializedName("AccountHolderNameInvestor")
    private String holderName;

    @SerializedName("BankAccountNoUAE")
    private String accountNumberUAE;

    @SerializedName("BankAccountNoNONUAE")
    private String accountNumber;

    @SerializedName("IBANNo")
    private String ibanNumber;

    @SerializedName("SWIFTID")
    private String swiftId;

    @SerializedName("Country")
    private String country;

    @SerializedName("City")
    private String city;

    @SerializedName("BranchName")
    private String branchName;

    @SerializedName("RefNo")
    private String refNo;

    @SerializedName("ReqID")
    private int reqID;

    @SerializedName("ServiceType")
    private int serviceType;

    @SerializedName("UserID")
    private int userID;

    @SerializedName("TotalAmount")
    private long totalAmount;

    @SerializedName("MySecurities")
    private List<Security> securities;

    public List<Security> getSecurities() {
        return securities;
    }

    public void setSecurities(List<Security> securities) {
        this.securities = securities;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void setReqID(int reqID) {
        this.reqID = reqID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    public String getCrmTrackingNumber() {
        return crmTrackingNumber;
    }

    public void setCrmTrackingNumber(String crmTrackingNumber) {
        this.crmTrackingNumber = crmTrackingNumber;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getInvestorName() {
        return investorName;
    }

    public void setInvestorName(String investorName) {
        this.investorName = investorName;
    }

    public String getInvestorNumber() {
        return investorNumber;
    }

    public void setInvestorNumber(String investorNumber) {
        this.investorNumber = investorNumber;
    }

    public int getServiceType() {
        return serviceType;
    }

    public void setServiceType(int serviceType) {
        this.serviceType = serviceType;
    }

    public String getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(String applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getDividendsVia() {
        return dividendsVia;
    }

    public void setDividendsVia(String dividendsVia) {
        this.dividendsVia = dividendsVia;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNumberUAE() {
        return accountNumberUAE;
    }

    public void setAccountNumberUAE(String accountNumberUAE) {
        this.accountNumberUAE = accountNumberUAE;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getIbanNumber() {
        return ibanNumber;
    }

    public void setIbanNumber(String ibanNumber) {
        this.ibanNumber = ibanNumber;
    }

    public String getSwiftId() {
        return swiftId;
    }

    public void setSwiftId(String swiftId) {
        this.swiftId = swiftId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getToInvestorName() {
        return toInvestorName;
    }

    public void setToInvestorName(String toInvestorName) {
        this.toInvestorName = toInvestorName;
    }

    public String getToInvestorNumber() {
        return toInvestorNumber;
    }

    public void setToInvestorNumber(String toInvestorNumber) {
        this.toInvestorNumber = toInvestorNumber;
    }

    public void setTotalAmount(long totalAmount) {
        this.totalAmount = totalAmount;
    }
}
