package com.DFM.dto;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;

/**
 * Created by Sandeep on 26/10/17.
 */

public class BrokerData {

    @XStreamImplicit(itemFieldName = "Broker_Info")
    private ArrayList<Broker> brokers;

    public ArrayList<Broker> getBrokers() {
        return brokers;
    }

    public void setBrokers(ArrayList<Broker> brokers) {
        this.brokers = brokers;
    }
}
