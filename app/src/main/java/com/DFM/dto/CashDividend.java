package com.DFM.dto;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;

/**
 * Created by Sandeep on 23/11/17.
 */

public class CashDividend {

    @XStreamImplicit(itemFieldName = "Record")
    private ArrayList<Dividend> records;

    public ArrayList<Dividend> getRecords() {
        return records;
    }
}
