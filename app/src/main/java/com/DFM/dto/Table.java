package com.DFM.dto;

/**
 * Created by Sandeep on 30/10/17.
 */

public class Table {

    private String email, NIN, CITY, COUNTRY, POSTAL, PHONE2, ADDRESS_1, STATUS, EFFECTIVEDATE, PHONE1, FAX, JOINACCOUNT, ENNAME, ARNAME, BIRTHDATE, ACCOUNT, ACCOUNTYPE, CLIENTTYPE, PORTFOLIOACC, OWNERTYPE, BROKERCODE, BROKERNAMEEN, BROKERNAMEAR, PASSPORT, FAMILYBOOK, ADDRESS_2, ADDRESS_3;

    public String getNIN() {
        return NIN;
    }

    public void setNIN(String NIN) {
        this.NIN = NIN;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getENNAME() {
        return ENNAME.trim();
    }

    public void setENNAME(String ENNAME) {
        this.ENNAME = ENNAME;
    }

    public String getPHONE2() {
        return PHONE2;
    }

    public void setPHONE2(String PHONE2) {
        this.PHONE2 = PHONE2;
    }

    public String getADDRESS_1() {
        return ADDRESS_1;
    }

    public void setADDRESS_1(String ADDRESS_1) {
        this.ADDRESS_1 = ADDRESS_1;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }

    public String getCOUNTRY() {
        return COUNTRY;
    }

    public void setCOUNTRY(String COUNTRY) {
        this.COUNTRY = COUNTRY;
    }

    public String getPOSTAL() {
        return POSTAL;
    }

    public void setPOSTAL(String POSTAL) {
        this.POSTAL = POSTAL;
    }
}
