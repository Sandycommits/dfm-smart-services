package com.DFM.dto;

/**
 * Created by Sandeep on 30/10/17.
 */

public class Company {

    private String NIN, Symbol, NameEN, NameAR, Broker;

    public String getBroker() {
        return Broker;
    }

    public void setBroker(String broker) {
        Broker = broker;
    }

    public String getSymbol() {
        return Symbol;
    }

    public void setSymbol(String symbol) {
        Symbol = symbol;
    }

    public String getNIN() {

        return NIN;
    }

    public void setNIN(String NIN) {
        this.NIN = NIN;
    }

    public String getNameEN() {
        return NameEN;
    }

    public void setNameEN(String nameEN) {
        NameEN = nameEN;
    }

    public String getNameAR() {
        return NameAR;
    }

    public void setNameAR(String nameAR) {
        NameAR = nameAR;
    }
}
