package com.DFM.dto;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sandeep on 27/11/17.
 */

public class TradeRecord implements Parcelable {

    public static final Creator<TradeRecord> CREATOR = new Creator<TradeRecord>() {
        @Override
        public TradeRecord createFromParcel(Parcel in) {
            return new TradeRecord(in);
        }

        @Override
        public TradeRecord[] newArray(int size) {
            return new TradeRecord[size];
        }
    };
    private String BrokerCode, BrokerNameEn, BrokerNameAr, Symbol, SecurityNameEn, SecurityNameAr, AccountNo, DateA, DateB, AsOnDate, TCD, TSC, TransactionType, Currency, Name, TransactionDesc, TransactionNo, Available, Frozen, Pledged, Pending, CurrentBalance, Offered, Account, TotalBuyValue, TotalSellValue, TotalBuyVolume, TotalSellVolume;
    private long NoOfSecurity;
    private double SecurityPrice, TotalAmount, LastTradePrice, MarketValue;

    protected TradeRecord(Parcel in) {
        MarketValue = in.readDouble();
        TotalBuyVolume = in.readString();
        TotalSellVolume = in.readString();
        NoOfSecurity = in.readLong();
        DateA = in.readString();
        TransactionNo = in.readString();
        SecurityPrice = in.readDouble();
        AccountNo = in.readString();
        AsOnDate = in.readString();
        Available = in.readString();
        CurrentBalance = in.readString();
        Frozen = in.readString();
        Pending = in.readString();
        Offered = in.readString();
        Account = in.readString();
        TransactionType = in.readString();
    }

    public String getBrokerCode() {
        return BrokerCode.trim();
    }

    public String getSymbol() {
        return Symbol.trim();
    }

    public String getAccountNo() {
        return AccountNo;
    }

    public String getDateA() {
        return DateA;
    }

    public String getAsOnDate() {
        return AsOnDate;
    }

    public String getTransactionType() {
        return TransactionType;
    }

    public String getAvailable() {
        return Available;
    }

    public String getFrozen() {
        return Frozen;
    }

    public String getPending() {
        return Pending;
    }

    public String getOffered() {
        return Offered;
    }

    public long getNoOfSecurity() {
        return NoOfSecurity;
    }

    public String getAccount() {
        return Account.trim();
    }

    public String getTotalBuyVolume() {
        return TotalBuyVolume;
    }

    public String getTotalSellVolume() {
        return TotalSellVolume;
    }

    public double getSecurityPrice() {
        return SecurityPrice;
    }

    public double getTotalAmount() {
        return TotalAmount;
    }

    public String getTotalBuyValue() {
        return TotalBuyValue;
    }

    public String getTotalSellValue() {
        return TotalSellValue;
    }

    public double getMarketValue() {
        return MarketValue;
    }

    public String getCurrentBalance() {
        return CurrentBalance;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeDouble(MarketValue);
        parcel.writeString(TotalBuyVolume);
        parcel.writeString(TotalSellVolume);
        parcel.writeLong(NoOfSecurity);
        parcel.writeString(DateA);
        parcel.writeString(TransactionNo);
        parcel.writeDouble(SecurityPrice);
        parcel.writeString(AccountNo);
        parcel.writeString(AsOnDate);
        parcel.writeString(Available);
        parcel.writeString(CurrentBalance);
        parcel.writeString(Frozen);
        parcel.writeString(Pending);
        parcel.writeString(Offered);
        parcel.writeString(Account);
        parcel.writeString(TransactionType);
    }
}
