package com.DFM.dto;

/**
 * Created by Sandeep on 17/10/17.
 */

public class Icon {

    private String title, icon, hidden, enabled, titleAR, titleID, DisableMsgEN, DisableMsgAR;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getHidden() {
        return hidden;
    }

    public void setHidden(String hidden) {
        this.hidden = hidden;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getTitleAR() {
        return titleAR;
    }

    public String getDisableMsgEN() {
        return DisableMsgEN;
    }

    public String getDisableMsgAR() {
        return DisableMsgAR;
    }
}
