package com.DFM.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Created by macadmin on 12/12/17.
 */

public class Header {

    @SerializedName("FormOperation")
    private int formOperation;

    @SerializedName("HasAttahments")
    private boolean hasAttachments;

    @SerializedName("RefNo")
    private String refNo;

    @SerializedName("RequestID")
    private int requestID;

    @SerializedName("sessionID")
    private String sessionID;

    @SerializedName("UserID")
    private int userID;

    public void setFormOperation(int formOperation) {
        this.formOperation = formOperation;
    }

    public void setHasAttachments(boolean hasAttachments) {
        this.hasAttachments = hasAttachments;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public void setRequestID(int requestID) {
        this.requestID = requestID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}
