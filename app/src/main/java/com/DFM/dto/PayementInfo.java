package com.DFM.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by macadmin on 12/12/17.
 */

public class PayementInfo {

    @SerializedName("PaymentAmount")
    @Expose
    private long paymentAmount;

    public long getPaymentAmount() {
        return paymentAmount;
    }
}
