package com.DFM.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by macadmin on 12/12/17.
 */

public class HeaderResponse {

    @SerializedName("ReqStatus_MSG")
    @Expose
    private StatusMessage statusMessage;

    public StatusMessage getStatusMessage() {
        return statusMessage;
    }
}
