package com.DFM.dto;

import com.DFM.utils.CommonMethods;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by macadmin on 12/14/17.
 */

public class Case implements Serializable {

    @SerializedName("RequestName")
    private String requestName = "";

    @SerializedName("TrackingNumber")
    private String trackingNumber;

    @SerializedName("RequestDate")
    private String requestDate = "";

    @SerializedName("RequestStatus")
    private String requestStatus = "";

    @SerializedName("RequestCaseSubject")
    private String requestCaseSubject = "";

    @SerializedName("RequestCaseReason")
    private String requestCaseReason = "";

    @SerializedName("RequestCaseRemarks")
    private String requestCaseRemarks = "";

    @SerializedName("RequestExpectedDate")
    private String requestExpectedDate = "";

    @SerializedName("RefNO")
    private String referenceNumber;

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public String getRequestName() {
        return requestName;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public String getRequestDate() {
        return CommonMethods.getRequiredDateFormat(requestDate);
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public String getRequestCaseSubject() {
        return requestCaseSubject;
    }

    public String getRequestCaseReason() {
        return requestCaseReason;
    }

    public String getRequestCaseRemarks() {
        return requestCaseRemarks;
    }

    public String getRequestExpectedDate() {
        return requestExpectedDate;
    }
}
