package com.DFM.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by macadmin on 12/12/17.
 */

public class IVestorCardRequest {

    @SerializedName("Attachments")
    private List<Attachment> attachments;

    @SerializedName("Body")
    private Body body;

    @SerializedName("CaseData")
    private CaseData caseData;

    @SerializedName("Header")
    private Header header;

    @SerializedName("MySecurities")
    private List<Security> securities;

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public void setCaseData(CaseData caseData) {
        this.caseData = caseData;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public void setSecurities(List<Security> securities) {
        this.securities = securities;
    }
}
