package com.DFM.dto;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;

/**
 * Created by Sandeep on 15/11/17.
 */

public class GlobalAccount {

    @XStreamImplicit(itemFieldName = "Record")
    private ArrayList<BrokerStatement> brokers;

    public ArrayList<BrokerStatement> getBrokers() {
        return brokers;
    }
}
