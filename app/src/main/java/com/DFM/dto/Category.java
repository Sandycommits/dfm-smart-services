package com.DFM.dto;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;

/**
 * Created by Sandeep on 08/11/17.
 */

public class Category {

    @XStreamImplicit(itemFieldName = "CategoryID")
    private ArrayList<String> categoryIdList;

    @XStreamImplicit(itemFieldName = "CategoryTitle")
    private ArrayList<String> categories;

    @XStreamImplicit(itemFieldName = "CategoryTitleAR")
    private ArrayList<String> categoriesAR;

    public ArrayList<String> getCategoriesAR() {
        return categoriesAR;
    }

    public ArrayList<String> getCategoryIdList() {
        return categoryIdList;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }
}
