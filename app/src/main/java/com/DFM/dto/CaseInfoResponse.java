package com.DFM.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by macadmin on 1/31/18.
 */

public class CaseInfoResponse {

    @SerializedName("CaseID")
    @Expose
    private String trackingNumber;

    public String getTrackingNumber() {
        return trackingNumber;
    }
}
