package com.DFM.dto;

/**
 * Created by Sandeep on 26/10/17.
 */

public class Broker {

    private String Broker_name_ar, Broker_name_en, Broker_website, Broker_lat, Broker_lon;

    public String getBroker_name_ar() {
        return Broker_name_ar;
    }

    public void setBroker_name_ar(String broker_name_ar) {
        Broker_name_ar = broker_name_ar;
    }

    public String getBroker_name_en() {
        return Broker_name_en;
    }

    public void setBroker_name_en(String broker_name_en) {
        Broker_name_en = broker_name_en;
    }

    public String getBroker_website() {
        return Broker_website;
    }

    public void setBroker_website(String broker_website) {
        Broker_website = broker_website;
    }

    public String getBroker_lat() {
        return Broker_lat;
    }

    public void setBroker_lat(String broker_lat) {
        Broker_lat = broker_lat;
    }

    public String getBroker_lon() {
        return Broker_lon;
    }

    public void setBroker_lon(String broker_lon) {
        Broker_lon = broker_lon;
    }
}
