package com.DFM.dto;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;

/**
 * Created by Sandeep on 21/11/17.
 */

public class IVestor {

    @XStreamImplicit(itemFieldName = "Record")
    private ArrayList<IVestorRecord> records;

    public ArrayList<IVestorRecord> getRecords() {
        return records;
    }
}
