package com.DFM.dto;

/**
 * Created by macadmin on 1/7/18.
 */

public class ComboBox {

    private String option, value;

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
