package com.DFM.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Sandeep on 30/10/17.
 */

public class Record {


    @XStreamAlias("Table")

    private Table table;

    private String current_close, Previous_close, Current_Price;
    private String symbol;

    public String getCurrent_Price() {
        return Current_Price;
    }

    public void setCurrent_Price(String current_Price) {
        Current_Price = current_Price;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }
}
