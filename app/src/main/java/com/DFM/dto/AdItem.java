package com.DFM.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Sandeep on 18/10/17.
 */
@XStreamAlias("XML")
public class AdItem {
    @XStreamAlias("STATUS")
    private String status;
    @XStreamAlias("DATA")
    private String data;
    @XStreamAlias("Error_Message")
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
