package com.DFM.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by macadmin on 12/12/17.
 */

public class FormInfoResponse {

    @SerializedName("PaymentInfo")
    @Expose
    private PayementInfo payementInfo;

    @SerializedName("RefNo")
    @Expose
    private String referenceNumber;

    @SerializedName("FormData")
    @Expose
    private Body formData;

    @SerializedName("FormURL")
    @Expose
    private String url;

    public Body getFormData() {
        return formData;
    }

    public PayementInfo getPayementInfo() {
        return payementInfo;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public String getUrl() {
        return url;
    }
}
