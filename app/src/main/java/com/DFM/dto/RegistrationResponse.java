package com.DFM.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandeep on 16/10/17.
 */

public class RegistrationResponse {

    @SerializedName("Status_Code")
    @Expose
    private String statusCode;

    @SerializedName("Status_MSG_En")
    @Expose
    private String messageEn;

    @SerializedName("Status_MSG_Ar")
    @Expose
    private String messageAr;

    public String getStatusCode() {
        return statusCode;
    }

    public String getMessageEn() {
        return messageEn;
    }

    public String getMessageAr() {
        return messageAr;
    }
}
