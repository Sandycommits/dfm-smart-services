package com.DFM.dto;

import java.io.Serializable;

/**
 * Created by Sandeep on 09/11/17.
 */

public class Ticket implements Serializable {

    private String TotalTicketWaiting, approx_wait_time, avg_wait_time, res_satus_msg, resp_status, ticketID, ticketNum, title, verificationCode;

    public String getTotalTicketWaiting() {
        return TotalTicketWaiting;
    }

    public String getTicketNum() {
        return ticketNum;
    }

    public String getTitle() {
        return title;
    }
}
