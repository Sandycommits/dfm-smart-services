package com.DFM.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandeep on 31/10/17.
 */

public class Resource {

    @SerializedName("r_path")
    @Expose
    private String postfix;

    public String getPostfix() {
        return postfix;
    }
}
