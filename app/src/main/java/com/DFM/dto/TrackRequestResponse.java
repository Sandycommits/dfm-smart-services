package com.DFM.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by macadmin on 12/14/17.
 */

public class TrackRequestResponse {

    @SerializedName("Status_MSG_En")
    private String status;

    @SerializedName("Cases")
    private List<Case> cases;

    public String getStatus() {
        return status;
    }

    public List<Case> getCases() {
        return cases;
    }
}
