package com.DFM.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Created by macadmin on 12/25/17.
 */

public class Security {

    @SerializedName("SecFees")
    private String fees;

    @SerializedName("SecClosingDate")
    private String closingDate;

    @SerializedName("SecPrice")
    private String price;

    @SerializedName("SecNumber")
    private String shares;

    @SerializedName("SecCompanyName")
    private String company;

    @SerializedName("SecRow")
    private String row;

    @SerializedName("SecMax")
    private String available;

    @SerializedName("SecSymbol1")
    private String symbol;

    @SerializedName("SecCurrency1")
    private String currency;

    @SerializedName("TransactionNumber")
    private String transactionNumber = "";

    @SerializedName("DublicateValidation")
    private String duplicateValidation = "";

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(String closingDate) {
        this.closingDate = closingDate;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getShares() {
        return shares;
    }

    public void setShares(String shares) {
        this.shares = shares;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public String getDuplicateValidation() {
        return duplicateValidation;
    }

    public void setDuplicateValidation(String duplicateValidation) {
        this.duplicateValidation = duplicateValidation;
    }
}
