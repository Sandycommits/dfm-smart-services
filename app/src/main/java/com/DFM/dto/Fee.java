package com.DFM.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Created by macadmin on 12/21/17.
 */

public class Fee {

    @SerializedName("FeePercentageField")
    private String percentage;

    @SerializedName("MinimumFeeAmountField")
    private String minimumAmount;

    public String getPercentage() {
        return percentage;
    }

    public String getMinimumAmount() {
        return minimumAmount;
    }
}
