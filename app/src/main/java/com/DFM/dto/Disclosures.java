package com.DFM.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandeep on 31/10/17.
 */

public class Disclosures {

    @SerializedName("root")
    @Expose
    private List<Disclosure> disclosures;

    public List<Disclosure> getDisclosures() {
        return disclosures;
    }
}
