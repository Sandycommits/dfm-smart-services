package com.DFM.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandeep on 31/10/17.
 */

public class Disclosure {

    @SerializedName("headline")
    @Expose
    private String headline;

    @SerializedName("issuer_symbol")
    @Expose
    private String issuer;

    @SerializedName("publication_date")
    @Expose
    private String date;

    @SerializedName("resources")
    @Expose
    private List<Resource> resources;

    public String getHeadline() {
        return headline;
    }

    public String getIssuer() {
        return issuer;
    }

    public String getDate() {
        return date;
    }

    public List<Resource> getResources() {
        return resources;
    }
}
