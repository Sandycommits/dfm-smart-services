package com.DFM.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Created by macadmin on 12/12/17.
 */

public class Attachment {

    @SerializedName("DocumentName")
    private String documentName;

    @SerializedName("FileBytes")
    private String fileBytes;

    @SerializedName("FileName")
    private String fileName;

    @SerializedName("FileType")
    private String fileType;

    private int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public void setFileBytes(String fileBytes) {
        this.fileBytes = fileBytes;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
