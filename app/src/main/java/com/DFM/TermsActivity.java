package com.DFM;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.DFM.fonts.HelveticaNeueButton;
import com.DFM.utils.PreferenceConnector;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TermsActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.termsView)
    WebView termsView;
    @BindView(R.id.agree)
    HelveticaNeueButton agree;
    @BindView(R.id.disagree)
    HelveticaNeueButton disagree;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.terms_title));

        loadWebView();
    }

    private void loadWebView() {

        WebSettings settings = termsView.getSettings();
        settings.setJavaScriptEnabled(true);
        float fontSize = getResources().getDimension(R.dimen.webview_text_size);
        settings.setDefaultFontSize((int) fontSize);
        if (language.equalsIgnoreCase("ar")) {
            termsView.loadUrl("file:///android_asset/terms_ar.html");
        } else {
            termsView.loadUrl("file:///android_asset/terms.html");
        }
    }

    @OnClick({R.id.agree, R.id.disagree})
    public void onViewClicks(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.agree:
                PreferenceConnector.writeBoolean(this, PreferenceConnector.TERMS, true);
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                this.finish();
                break;
            case R.id.disagree:
                onBackPressed();
                break;
            default:
                break;

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
