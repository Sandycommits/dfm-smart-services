package com.DFM;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.DFM.dto.AccountRecord;
import com.DFM.fonts.HelveticaNeueEditText;
import com.DFM.utils.CommonMethods;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccountDetailsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.accountNo)
    HelveticaNeueEditText accountNo;
    @BindView(R.id.transactionDate)
    HelveticaNeueEditText transactionDate;
    @BindView(R.id.completionDate)
    HelveticaNeueEditText completionDate;
    @BindView(R.id.transactionDescription)
    HelveticaNeueEditText transactionDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_details);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.details));

        loadData();
    }

    private void loadData() {

        Intent intent = getIntent();
        AccountRecord record = intent.getParcelableExtra("account_record");

        accountNo.setText(record.getAccount());
        transactionDate.setText(CommonMethods.getDateInFormat(record.getTransDate()));
        completionDate.setText(CommonMethods.getDateInFormat(record.getCompDate()));
        transactionDescription.setText(record.getTrans_Desc());
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
