package com.DFM;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.DFM.adapters.CustomBubbleAdapter;
import com.DFM.dto.Broker;
import com.DFM.dto.BrokerData;
import com.DFM.dto.Login;
import com.DFM.fragments.BrokerDialogFragment;
import com.DFM.interfaces.OnBrokerSelectionListener;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.PreferenceConnector;
import com.Wsdl2Code.WebServices.MiscFunctions.IWsdl2CodeEvents;
import com.Wsdl2Code.WebServices.MiscFunctions.MiscFunctions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LocationActivity extends BaseActivity implements OnMapReadyCallback, OnBrokerSelectionListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private GoogleMap mMap;
    private BrokerData brokerData;
    private Marker marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.our_location));
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        loadMapMenu();
        loadData();
    }

    @Override
    public void onViewClicks(View view) {

    }

    private void loadData() {

        MiscFunctions miscFunctions = new MiscFunctions(new IWsdl2CodeEvents() {
            @Override
            public void Wsdl2CodeStartedRequest() {

            }

            @Override
            public void Wsdl2CodeFinished(String methodName, Object data) {
                dismissProgressDialog();
                try {
                    xStream.processAnnotations(Login.class);
                    Login login = (Login) xStream.fromXML(data.toString());

                    brokerData = login.getData().getBrokerData();
                    CommonMethods.storeObject(LocationActivity.this, brokerData, PreferenceConnector.BROKER_DATA);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void Wsdl2CodeFinishedWithException(Exception ex) {
                dismissProgressDialog();
            }

            @Override
            public void Wsdl2CodeEndedRequest() {

            }
        });
        showProgressDialog();
        try {
            miscFunctions.BrokersListAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadMapMenu() {

        ImageView mapMenu = new ImageView(this);
        mapMenu.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_list_bulleted));
        FloatingActionButton floatingButton = new FloatingActionButton.Builder(this)
                .setContentView(mapMenu)
                .setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.blue_action_background))
                .build();

        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                FloatingActionButton.LayoutParams.WRAP_CONTENT, FloatingActionButton.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, getResources().getDimensionPixelSize(R.dimen.regular_margin), getResources().getDimensionPixelSize(R.dimen.regular_margin));
        floatingButton.setPosition(FloatingActionButton.POSITION_BOTTOM_RIGHT, params);

        SubActionButton.Builder rLSubBuilder = new SubActionButton.Builder(this);
        rLSubBuilder.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.blue_action_background));
        ImageView markers = new ImageView(this);
        ImageView location = new ImageView(this);
        ImageView search = new ImageView(this);

        markers.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_markers));
        location.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_map_location));
        search.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_broker_dis));


        final FloatingActionMenu rightLowerMenu = new FloatingActionMenu.Builder(this)
                .addSubActionView(rLSubBuilder.setContentView(markers).build())
                .addSubActionView(rLSubBuilder.setContentView(location).build())
                .addSubActionView(rLSubBuilder.setContentView(search).build())
                .attachTo(floatingButton)
                .build();
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rightLowerMenu.close(false);
                if (brokerData != null)
                    showDialog();
            }
        });

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rightLowerMenu.close(true);
                pointToDFM();
            }
        });

        markers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rightLowerMenu.close(true);
                if (brokerData != null)
                    displayMarkers();
            }
        });

    }

    private void displayMarkers() {
        for (Broker broker : brokerData.getBrokers()) {
            double latitude = CommonMethods.getDecimalValue(broker.getBroker_lat());
            double longitude = CommonMethods.getDecimalValue(broker.getBroker_lon());
            LatLng latLng = new LatLng(latitude, longitude);

            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14.0f));
//            mMap.setInfoWindowAdapter(new CustomBubbleAdapter(this, broker.getBroker_name_en() + " - " + broker.getBroker_name_ar(), broker.getBroker_website()));

            Bitmap bitmap = CommonMethods.getBitmap(this, R.drawable.ic_marker);
            marker = mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromBitmap(bitmap)));
            marker.setTag(broker);
//            marker = null;
//            addBubble(latLng, broker.getBroker_name_en() + " - " + broker.getBroker_name_ar(), broker.getBroker_website());
        }
        LatLng latLng = new LatLng(25.2263722, 55.2865452);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10.0f));
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Broker broker = (Broker) marker.getTag();
                mMap.setInfoWindowAdapter(new CustomBubbleAdapter(LocationActivity.this, broker.getBroker_name_en() + " - " + broker.getBroker_name_ar(), broker.getBroker_website()));
                return false;
            }
        });
    }

    void showDialog() {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = BrokerDialogFragment.newInstance("", "");
        newFragment.show(ft, "dialog");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in DFM and move the camera
        LatLng latLng = new LatLng(25.2263722, 55.2865452);
//        mMap.addMarker(new MarkerOptions().position(latLng).title(getString(R.string.dfm)));
        addBubble(latLng, getString(R.string.dfm), getString(R.string.world_class));
    }

    void pointToDFM() {
        // Add a marker in DFM and move the camera
        LatLng latLng = new LatLng(25.2263722, 55.2865452);
        addBubble(latLng, getString(R.string.dfm), getString(R.string.world_class));
    }

    private void addBubble(LatLng latLng, String title, String link) {
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14.0f));
        mMap.setInfoWindowAdapter(new CustomBubbleAdapter(this, title, link));
        Bitmap bitmap = CommonMethods.getBitmap(this, R.drawable.ic_marker);
        if (marker == null) {
            marker = mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromBitmap(bitmap))
            );
        } else {
            marker.setPosition(latLng);
            marker.showInfoWindow();
        }
    }

    @Override
    public void onBrokerSelected(Broker broker) {
        CommonMethods.getLog(broker.getBroker_name_en());
        double latitude = CommonMethods.getDecimalValue(broker.getBroker_lat());
        double longitude = CommonMethods.getDecimalValue(broker.getBroker_lon());
        LatLng latLng = new LatLng(latitude, longitude);
        addBubble(latLng, broker.getBroker_name_en() + " - " + broker.getBroker_name_ar(), broker.getBroker_website());
    }
}
