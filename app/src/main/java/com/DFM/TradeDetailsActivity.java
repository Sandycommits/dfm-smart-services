package com.DFM;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.DFM.dto.TradeRecord;
import com.DFM.fonts.HelveticaNeueEditText;
import com.DFM.utils.CommonMethods;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TradeDetailsActivity extends AppCompatActivity {

    @BindView(R.id.marketValue)
    HelveticaNeueEditText marketValue;
    @BindView(R.id.totalBuy)
    HelveticaNeueEditText totalBuy;
    @BindView(R.id.totalSell)
    HelveticaNeueEditText totalSell;
    @BindView(R.id.totalNoTraded)
    HelveticaNeueEditText totalNoTraded;
    @BindView(R.id.transactionType)
    HelveticaNeueEditText transactionType;
    @BindView(R.id.transDate)
    HelveticaNeueEditText transactionDate;
    @BindView(R.id.transactionNumber)
    HelveticaNeueEditText transactionNumber;
    @BindView(R.id.securityPrice)
    HelveticaNeueEditText securityPrice;
    @BindView(R.id.noOfSecurity)
    HelveticaNeueEditText noOfSecurity;
    @BindView(R.id.accountNo)
    HelveticaNeueEditText accountNo;
    @BindView(R.id.availableBalance)
    HelveticaNeueEditText availableBalance;
    @BindView(R.id.available)
    HelveticaNeueEditText available;
    @BindView(R.id.balance)
    HelveticaNeueEditText balance;
    @BindView(R.id.frozen)
    HelveticaNeueEditText frozen;
    @BindView(R.id.pending)
    HelveticaNeueEditText pending;
    @BindView(R.id.offered)
    HelveticaNeueEditText offered;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    DecimalFormat formatter;
    DecimalFormat sharesFormatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trade_details);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.investor_trade_details));

        NumberFormat nFormatter = NumberFormat.getNumberInstance(Locale.US);
        formatter = (DecimalFormat) nFormatter;
        formatter.applyPattern("#,###,###.###");
        formatter.setMaximumFractionDigits(3);
        formatter.setMinimumFractionDigits(3);

        NumberFormat sFormatter = NumberFormat.getNumberInstance(Locale.US);
        sharesFormatter = (DecimalFormat) sFormatter;
        sharesFormatter.applyPattern("#,###,###");
        loadDate();
    }

    private void loadDate() {
        Intent intent = getIntent();
        TradeRecord record = intent.getParcelableExtra("trade_record");
        marketValue.setText(record.getMarketValue() + "");
        totalBuy.setText(record.getTotalBuyVolume().equals("") ? "0.0" : record.getTotalBuyValue());
        totalSell.setText(record.getTotalSellVolume().equals("") ? "0.0" : record.getTotalSellVolume());
        totalNoTraded.setText(record.getNoOfSecurity() + "");
        transactionType.setText(record.getTransactionType());
        transactionDate.setText(CommonMethods.getDateInFormat(record.getDateA()));
        transactionNumber.setText(record.getAccount());
        securityPrice.setText(formatter.format(record.getSecurityPrice()));
        noOfSecurity.setText(sharesFormatter.format(record.getNoOfSecurity()));
        accountNo.setText(record.getAccountNo());
        availableBalance.setText(record.getAsOnDate().substring(0, 10));
        available.setText(record.getCurrentBalance());
        balance.setText(record.getCurrentBalance());
        frozen.setText(record.getFrozen());
        pending.setText(record.getPending());
        offered.setText(record.getOffered());
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
