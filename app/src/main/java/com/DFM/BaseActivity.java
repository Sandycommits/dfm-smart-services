package com.DFM;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.DFM.retrofit.APIClient;
import com.DFM.retrofit.APIInterface;
import com.DFM.utils.ProgressDialog;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.mapper.MapperWrapper;

/**
 * Created by Sandeep on 11/10/17.
 */

public abstract class BaseActivity extends AppCompatActivity {


    public static final int CONNECTION_OK = 200;
    public APIInterface apiService;
    public XStream xStream;
    public String language;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);//retrofit instance
        Configuration config = getResources().getConfiguration();
        language = config.locale.getLanguage();
        apiService = APIClient.getClientInstance().create(APIInterface.class);
        xStream = new XStream(new DomDriver()) {
            // to enable ignoring of unknown elements
            @Override
            protected MapperWrapper wrapMapper(MapperWrapper next) {
                return new MapperWrapper(next) {
                    @Override
                    public boolean shouldSerializeMember(Class definedIn, String fieldName) {
                        if (definedIn == Object.class) {
                            try {
                                return this.realClass(fieldName) != null;
                            } catch (Exception e) {
                                return false;
                            }
                        }
                        return super.shouldSerializeMember(definedIn, fieldName);
                    }
                };
            }
        };
//        xStream.processAnnotations(Login.class);
    }

    /**
     * Displays the progress dialog
     */
    protected void showProgressDialog() {
        ProgressDialog.getInstance().displayProgressDialog(this);
    }

    protected void dismissProgressDialog() {
        ProgressDialog.getInstance().dismissProgressDialog();
    }

    public abstract void onViewClicks(View view);
}
