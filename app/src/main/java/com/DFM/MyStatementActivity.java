package com.DFM;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.DFM.adapters.StatementRecyclerAdapter;
import com.DFM.dto.BrokerStatement;
import com.DFM.dto.GlobalAccount;
import com.DFM.dto.IVestorRecord;
import com.DFM.dto.Login;
import com.DFM.dto.NIN;
import com.DFM.dto.UserCode;
import com.DFM.fonts.HelveticaNeueBoldTextView;
import com.DFM.interfaces.OnItemClickListener;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.PreferenceConnector;
import com.Wsdl2Code.WebServices.Enquiry.Enquiry;
import com.Wsdl2Code.WebServices.Enquiry.IWsdl2CodeEvents;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyStatementActivity extends BaseActivity implements OnItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.statementRecycler)
    RecyclerView statementRecycler;
    @BindView(R.id.totalValue)
    HelveticaNeueBoldTextView totalPortfolio;
    @BindView(R.id.iVestorBalance)
    HelveticaNeueBoldTextView iVestorBalance;
    DecimalFormat formatter;
    private double totalValue;
    private int[] drawables = new int[]{R.drawable.ic_statement_1, R.drawable.ic_statement_2, R.drawable.ic_statement_3, R.drawable.ic_statement_4, R.drawable.ic_statement_5, R.drawable.ic_statement_6};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_statement);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.my_statement));

        NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
        formatter = (DecimalFormat) nf;
        formatter.applyPattern("#,###,###");
        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);
        loadItems();
        setGlobalAccountBalance();
    }

    private void setiVestorCardBalance() {

        Enquiry enquiry = new Enquiry(new IWsdl2CodeEvents() {
            @Override
            public void Wsdl2CodeStartedRequest() {

            }

            @Override
            public void Wsdl2CodeFinished(String methodName, Object data) {
                dismissProgressDialog();
                Login login = (Login) xStream.fromXML(data.toString());
                if (login.getStatus().equalsIgnoreCase("valid")) {
                    ArrayList<IVestorRecord> records = login.getData().getiVestor().getRecords();
                    if (records != null && records.size() > 0) {
                        iVestorBalance.setText(formatter.format(records.get(0).getiVESTORCardBalance()));
                    }
                }
            }

            @Override
            public void Wsdl2CodeFinishedWithException(Exception ex) {
                dismissProgressDialog();
            }

            @Override
            public void Wsdl2CodeEndedRequest() {

            }
        });
        showProgressDialog();
        Login login = (Login) CommonMethods.retrieveObject(this, PreferenceConnector.LOGIN, new Login());
        UserCode userCode = (UserCode) CommonMethods.retrieveObject(this, PreferenceConnector.USER_CODES, new UserCode());
        NIN nin = CommonMethods.getRootNin(userCode);
        try {
            enquiry.GetiVESTORStatementAsync(login.getData().getSession(), nin.getNIN(), CommonMethods.get20YearsDate(), CommonMethods.getCurrentDateOnly());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewClicks(View view) {

    }

    private void setGlobalAccountBalance() {

        Enquiry enquiry = new Enquiry(new IWsdl2CodeEvents() {
            @Override
            public void Wsdl2CodeStartedRequest() {

            }

            @Override
            public void Wsdl2CodeFinished(String methodName, Object data) {
                try {
                    dismissProgressDialog();
                    xStream.processAnnotations(Login.class);
                    Login login = (Login) xStream.fromXML(data.toString());
                    if (login.getStatus().equalsIgnoreCase("valid")) {
                        GlobalAccount globalAccount = login.getData().getGlobalAccount();
                        loadData(globalAccount);
                    } else {
                        CommonMethods.displayOKDialogue(MyStatementActivity.this, getString(R.string.session_expired));
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void Wsdl2CodeFinishedWithException(Exception ex) {
                dismissProgressDialog();
            }

            @Override
            public void Wsdl2CodeEndedRequest() {

            }
        });
        showProgressDialog();
        Login login = (Login) CommonMethods.retrieveObject(this, PreferenceConnector.LOGIN, new Login());
        UserCode userCode = (UserCode) CommonMethods.retrieveObject(this, PreferenceConnector.USER_CODES, new UserCode());
        NIN nin = CommonMethods.getRootNin(userCode);
        try {
            enquiry.GlobalAccountBalanceAsync(login.getData().getSession(), nin.getNIN(), CommonMethods.getCurrentDateOnly(), "0");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadData(GlobalAccount globalAccount) {
        ArrayList<BrokerStatement> brokers = globalAccount.getBrokers();
        for (BrokerStatement broker : brokers) {
            totalValue += broker.getCurrentBalance() * broker.getPrice();
        }
        totalPortfolio.setText(formatter.format(totalValue));
        setiVestorCardBalance();
    }

    private void loadItems() {
        String[] categories = getResources().getStringArray(R.array.statement_list);
        StatementRecyclerAdapter adapter = new StatementRecyclerAdapter(this, Arrays.asList(categories), language, drawables);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        statementRecycler.setHasFixedSize(true);
//        statementRecycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        statementRecycler.setLayoutManager(layoutManager);
        statementRecycler.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onItemClick(int position) {
        if (CommonMethods.isOnline(this)) {
            Intent intent;
            switch (position) {
                case 0:
                    intent = new Intent(this, GlobalAccountActivity.class);
                    startActivity(intent);
                    break;
                case 1:
                    intent = new Intent(this, IVestorActivity.class);
                    startActivity(intent);
                    break;
                case 2:
                    intent = new Intent(this, CashDividendActivity.class);
                    startActivity(intent);
                    break;
                case 3:
                    intent = new Intent(this, InvestorMovementActivity.class);
                    startActivity(intent);
                    break;
                case 4:
                    intent = new Intent(this, InvestorTradeActivity.class);
                    startActivity(intent);
                    break;
                case 5:
                    intent = new Intent(this, StatementsAccountActivity.class);
                    startActivity(intent);
                    break;
                default:
                    break;
            }
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
        }
    }
}
