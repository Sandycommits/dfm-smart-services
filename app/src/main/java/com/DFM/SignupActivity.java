package com.DFM;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Patterns;
import android.view.View;
import android.widget.LinearLayout;

import com.DFM.dto.Data;
import com.DFM.dto.Login;
import com.DFM.dto.Register;
import com.DFM.dto.ValidateEmailPhone;
import com.DFM.fonts.HelveticaNeueButton;
import com.DFM.fonts.HelveticaNeueEditText;
import com.DFM.utils.CommonMethods;
import com.DFM.utils.Constants;
import com.Wsdl2Code.WebServices.Authenticate.Authenticate;
import com.Wsdl2Code.WebServices.Authenticate.IWsdl2CodeEvents;
import com.Wsdl2Code.WebServices.Enquiry.Enquiry;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignupActivity extends BaseActivity {

    @BindView(R.id.yes)
    HelveticaNeueButton yes;
    @BindView(R.id.no)
    HelveticaNeueButton no;
    @BindView(R.id.register)
    HelveticaNeueButton register;

    @BindView(R.id.investorNumber)
    HelveticaNeueEditText investorNumber;
    @BindView(R.id.firstName)
    HelveticaNeueEditText firstName;
    @BindView(R.id.lastName)
    HelveticaNeueEditText lastName;
    @BindView(R.id.email)
    HelveticaNeueEditText email;
    @BindView(R.id.password)
    HelveticaNeueEditText password;
    @BindView(R.id.confirmPassword)
    HelveticaNeueEditText confirmPassword;
    @BindView(R.id.phone)
    HelveticaNeueEditText phone;

    @BindView(R.id.ninLayout)
    LinearLayout ninLayout;
    @BindView(R.id.namesLayout)
    LinearLayout namesLayout;

//    private String nanFormat = "^\\s*[a-zA-Z]{2}(?:\\s*\\d\\s*){6}[a-zA-Z]?\\s*$";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
    }


    @Override
    @OnClick({R.id.yes, R.id.no, R.id.register})
    public void onViewClicks(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.yes:
                ninLayout.setVisibility(View.VISIBLE);
                namesLayout.setVisibility(View.GONE);
                yes.setBackgroundResource(R.drawable.yes_shape_active);
                no.setBackgroundResource(R.drawable.no_shape_inactive);
                yes.setTextColor(Color.WHITE);
                no.setTextColor(ContextCompat.getColor(this, R.color.light_orange));
                break;
            case R.id.no:
                ninLayout.setVisibility(View.GONE);
                namesLayout.setVisibility(View.VISIBLE);
                yes.setBackgroundResource(R.drawable.yes_shape_inactive);
                yes.setTextColor(ContextCompat.getColor(this, R.color.light_orange));
                no.setBackgroundResource(R.drawable.no_shape_active);
                no.setTextColor(Color.WHITE);
                break;
            case R.id.register:
                if (yes.getCurrentTextColor() == Color.WHITE) {
                    investorRegistration();
                } else {
                    normalRegistration();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        this.finish();
    }

    private void normalRegistration() {
        String emailString = email.getText().toString();
        String phoneString = phone.getText().toString();
        String nameOne = firstName.getText().toString();
        String nameTwo = lastName.getText().toString();
        if (!nameOne.isEmpty()) {
            if (!nameTwo.isEmpty()) {
                if (Patterns.EMAIL_ADDRESS.matcher(emailString).matches()) {
                    String passwordOne = password.getText().toString();
                    String passwordTwo = confirmPassword.getText().toString();
                    if (!passwordOne.isEmpty()) {
                        if (!passwordTwo.isEmpty()) {
                            if (passwordOne.equals(passwordTwo)) {
                                if (!phoneString.isEmpty()) {
                                    Register register = new Register();
                                    register.setNAN("");
                                    register.setFirstName(nameOne);
                                    register.setLastName(nameTwo);
                                    register.setEmail(emailString);
                                    register.setPassword(passwordOne);
                                    register.setMobile(phoneString);
                                    if (CommonMethods.isOnline(this)) {
                                        validatePhoneEmail(register, "en");
                                    } else {
                                        CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
                                    }
                                } else {
                                    CommonMethods.displayOKDialogue(this, getString(R.string.phone_validation));
                                }
                            } else {
                                CommonMethods.displayOKDialogue(this, getString(R.string.matching_password));
                            }
                        } else {
                            CommonMethods.displayOKDialogue(this, getString(R.string.re_enter_password));
                        }
                    } else {
                        CommonMethods.displayOKDialogue(this, getString(R.string.choose_password));
                    }
                } else {
                    CommonMethods.displayOKDialogue(this, getString(R.string.email_validation));
                }
            } else {
                CommonMethods.displayOKDialogue(this, getString(R.string.last_name_validation));
            }
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.first_name_validation));
        }
    }

    private void investorRegistration() {
        String emailString = email.getText().toString().trim();
        String phoneString = phone.getText().toString();
        String nanString = investorNumber.getText().toString();
//        if (nanString.matches(nanFormat)) {
            if (Patterns.EMAIL_ADDRESS.matcher(emailString).matches()) {
                String passwordOne = password.getText().toString();
                String passwordTwo = confirmPassword.getText().toString();
                if (!passwordOne.isEmpty()) {
                    if (!passwordTwo.isEmpty()) {
                        if (passwordOne.equals(passwordTwo)) {
                            if (!phoneString.isEmpty()) {
                                Register register = new Register();
                                register.setNAN(nanString);
                                register.setEmail(emailString);
                                register.setPassword(passwordOne);
                                register.setMobile(phoneString);
                                if (CommonMethods.isOnline(this)) {
                                    validatePhoneEmail(register, "en");
                                } else {
                                    CommonMethods.displayOKDialogue(this, getString(R.string.no_internet));
                                }
                            } else {
                                CommonMethods.displayOKDialogue(this, getString(R.string.phone_validation));
                            }
                        } else {
                            CommonMethods.displayOKDialogue(this, getString(R.string.matching_password));
                        }
                    } else {
                        CommonMethods.displayOKDialogue(this, getString(R.string.re_enter_password));
                    }
                } else {
                    CommonMethods.displayOKDialogue(this, getString(R.string.choose_password));
                }
            } else {
                CommonMethods.displayOKDialogue(this, getString(R.string.email_validation));
            }
//        } else {
//            CommonMethods.displayOKDialogue(this, getString(R.string.nan_validation));
//        }
    }

    private void validatePhoneEmail(final Register register, String language) {


        Authenticate authenticate = new Authenticate(new IWsdl2CodeEvents() {
            @Override
            public void Wsdl2CodeStartedRequest() {

            }

            @Override
            public void Wsdl2CodeFinished(String methodName, Object data) {
                dismissProgressDialog();
                String response = data.toString();
                String display = "";
                XStream xStream = new XStream(new DomDriver());
                if (response.contains("Description")) {
                    xStream.processAnnotations(Login.class);
                    Login login = (Login) xStream.fromXML(response);
                    Data description = login.getData();
                    display = description.getDescription();
                } else {
                    xStream.processAnnotations(ValidateEmailPhone.class);
                    ValidateEmailPhone validate = (ValidateEmailPhone) xStream.fromXML(response);
                    display = validate.getData();
                }
                if (display.equalsIgnoreCase("Success")) {
                    sendCodes(register);
                } else {
                    CommonMethods.displayOKDialogue(SignupActivity.this, display);
                }
            }

            @Override
            public void Wsdl2CodeFinishedWithException(Exception ex) {
                dismissProgressDialog();
            }

            @Override
            public void Wsdl2CodeEndedRequest() {

            }
        });
        showProgressDialog();
        try {
            authenticate.ValidateEmailPhoneAsync(register.getEmail(), register.getMobile(), "", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void sendCodes(final Register register) {
        Enquiry enquiry = new Enquiry(new com.Wsdl2Code.WebServices.Enquiry.IWsdl2CodeEvents() {
            @Override
            public void Wsdl2CodeStartedRequest() {

            }

            @Override
            public void Wsdl2CodeFinished(String methodName, Object data) {
                dismissProgressDialog();
                XStream xStream = new XStream(new DomDriver());
                xStream.processAnnotations(Login.class);
                Login login = (Login) xStream.fromXML(data.toString());
                final Data codeData = login.getData();
                register.setEmailCode(codeData.getEmailCode());
                String mobile = register.getMobile();
                final Intent intent = new Intent(SignupActivity.this, OTPActivity.class);
                if (mobile.substring(0, 5).contains("971")) {
                    Enquiry mobileEnquiry = new Enquiry(new com.Wsdl2Code.WebServices.Enquiry.IWsdl2CodeEvents() {
                        @Override
                        public void Wsdl2CodeStartedRequest() {

                        }

                        @Override
                        public void Wsdl2CodeFinished(String methodName, Object Data) {
                            dismissProgressDialog();
                            register.setSmsCode(codeData.getSMSCode());
                            intent.putExtra("register", register);
                            startActivity(intent);
                        }

                        @Override
                        public void Wsdl2CodeFinishedWithException(Exception ex) {
                            dismissProgressDialog();
                        }

                        @Override
                        public void Wsdl2CodeEndedRequest() {

                        }
                    });
                    try {
                        mobileEnquiry.Send_SMSAsync(register.getMobile(), getString(R.string.sms_template) + " " + codeData.getSMSCode(), "DFMSmartApp");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    dismissProgressDialog();
                    register.setSmsCode("");
                    intent.putExtra("register", register);
                    startActivity(intent);
                }
                SignupActivity.this.finish();
            }

            @Override
            public void Wsdl2CodeFinishedWithException(Exception ex) {
                dismissProgressDialog();
            }

            @Override
            public void Wsdl2CodeEndedRequest() {

            }
        });
        try {
            showProgressDialog();
            enquiry.Send_EmailAsync(register.getEmail(), "eservices@dfm.ae", "", getString(R.string.activation_code_subject), "en", Constants.ACTIVATION_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
