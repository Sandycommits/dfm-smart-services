package com.DFM;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;

import com.DFM.adapters.DisclosureRecyclerAdapter;
import com.DFM.dto.Disclosure;
import com.DFM.dto.Disclosures;
import com.DFM.fonts.HelveticaNeueEditText;
import com.DFM.interfaces.OnItemClickListener;
import com.DFM.retrofit.APIInterface;
import com.DFM.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DisclosuresActivity extends BaseActivity implements OnItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.search)
    HelveticaNeueEditText search;
    @BindView(R.id.disclosureRecycler)
    RecyclerView disclosureRecycler;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private Disclosures disclosures;
    private DisclosureRecyclerAdapter adapter;
    private APIInterface apiService;
    private int count;
    private List<Disclosure> disclosureList;
    private boolean loading = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disclosures);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.disclosures));

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://feeds.dfm.ae/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        apiService = retrofit.create(APIInterface.class);
        disclosureList = new ArrayList<Disclosure>();
        getDisclosuresData(0);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().isEmpty()) {
                    loadData();
                } else {
                    filter(editable.toString());
                }
            }
        });
    }

    @Override
    public void onViewClicks(View view) {

    }

    private void getDisclosuresData(final int counter) {

        if (counter < 200) {
            showProgressDialog();
            apiService.getDisclosures("directfn2c8475_efsah", language, "Disclosure", "20", "MMM dd, yyyy hh:mm tt", "json", "true", counter + "").enqueue(new Callback<Disclosures>() {
                @Override
                public void onResponse(Call<Disclosures> call, Response<Disclosures> response) {
                    dismissProgressDialog();
                    disclosures = response.body();
                    if (counter == 0) {
                        disclosureList = disclosures.getDisclosures();
                        loadData();
                    } else {
                        disclosureList.addAll(disclosures.getDisclosures());
                        adapter.notifyDataSetChanged();
                    }
                    loading = true;
                }

                @Override
                public void onFailure(Call<Disclosures> call, Throwable t) {
                    dismissProgressDialog();
                }
            });
        }
    }

    private void filter(String text) {
        List<Disclosure> temp = new ArrayList();
        String filteredString;
        for (Disclosure disclosure : disclosureList) {
            filteredString = disclosure.getHeadline() + " " + disclosure.getIssuer();
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (filteredString.toLowerCase().contains(text.toLowerCase())) {
                temp.add(disclosure);
            }
        }
        //update recyclerview
        adapter.updateList(temp);
    }

    private void loadData() {
        adapter = new DisclosureRecyclerAdapter(this, disclosureList, language);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        disclosureRecycler.setHasFixedSize(true);
        disclosureRecycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        disclosureRecycler.setLayoutManager(layoutManager);
        disclosureRecycler.setAdapter(adapter);
        disclosureRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            count += 20;
                            getDisclosuresData(count);
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onItemClick(int position) {
        Uri uri = Uri.parse(Constants.DISCLOSURE_PDF_PREFIX + adapter.getItem(position).getResources().get(0).getPostfix());
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(browserIntent);
    }
}
