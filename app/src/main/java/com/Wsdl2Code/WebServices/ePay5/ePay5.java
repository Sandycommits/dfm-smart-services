package com.Wsdl2Code.WebServices.ePay5;

//------------------------------------------------------------------------------
// <wsdl2code-generated>
//    This code was generated by http://www.wsdl2code.com version  2.6
//
// Date Of Creation: 12/25/2017 1:30:01 PM
//    Please dont change this code, regeneration will override your changes
//</wsdl2code-generated>
//
//------------------------------------------------------------------------------
//
//This source code was auto-generated by Wsdl2Code  Version
//

import android.os.AsyncTask;

import com.DFM.utils.Constants;
import com.Wsdl2Code.WebServices.ePay5.WS_Enums.SoapProtocolVersion;

import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.List;

public class ePay5 {

    public String NAMESPACE = "http://dfm.ae/";
    public String url = Constants.BASE_URL + "ePay5.asmx";
    public int timeOut = Constants.TIME_OUT;
    public IWsdl2CodeEvents eventHandler;
    public SoapProtocolVersion soapVersion;

    public ePay5() {
    }

    public ePay5(IWsdl2CodeEvents eventHandler) {
        this.eventHandler = eventHandler;
    }

    public ePay5(IWsdl2CodeEvents eventHandler, String url) {
        this.eventHandler = eventHandler;
        this.url = url;
    }

    public ePay5(IWsdl2CodeEvents eventHandler, String url, int timeOutInSeconds) {
        this.eventHandler = eventHandler;
        this.url = url;
        this.setTimeOut(timeOutInSeconds);
    }

    public void setTimeOut(int seconds) {
        this.timeOut = seconds * 1000;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void ePay5EncryptionAsync(String sessionID, String text) throws Exception {
        if (this.eventHandler == null)
            throw new Exception("Async Methods Requires IWsdl2CodeEvents");
        ePay5EncryptionAsync(sessionID, text, null);
    }

    public void ePay5EncryptionAsync(final String sessionID, final String text, final List<HeaderProperty> headers) throws Exception {

        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                eventHandler.Wsdl2CodeStartedRequest();
            }

            @Override
            protected String doInBackground(Void... params) {
                return ePay5Encryption(sessionID, text, headers);
            }

            @Override
            protected void onPostExecute(String result) {
                eventHandler.Wsdl2CodeEndedRequest();
                if (result != null) {
                    eventHandler.Wsdl2CodeFinished("ePay5Encryption", result);
                }
            }
        }.execute();
    }

    public String ePay5Encryption(String sessionID, String text) {
        return ePay5Encryption(sessionID, text, null);
    }

    public String ePay5Encryption(String sessionID, String text, List<HeaderProperty> headers) {
        SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        soapEnvelope.implicitTypes = true;
        soapEnvelope.dotNet = true;
        SoapObject soapReq = new SoapObject("http://dfm.ae/", "ePay5Encryption");
        soapReq.addProperty("SessionID", sessionID);
        soapReq.addProperty("text", text);
        soapEnvelope.setOutputSoapObject(soapReq);
        HttpTransportSE httpTransport = new HttpTransportSE(url, timeOut);
        try {
            if (headers != null) {
                httpTransport.call("http://dfm.ae/ePay5Encryption", soapEnvelope, headers);
            } else {
                httpTransport.call("http://dfm.ae/ePay5Encryption", soapEnvelope);
            }
            Object retObj = soapEnvelope.bodyIn;
            if (retObj instanceof SoapFault) {
                SoapFault fault = (SoapFault) retObj;
                Exception ex = new Exception(fault.faultstring);
                if (eventHandler != null)
                    eventHandler.Wsdl2CodeFinishedWithException(ex);
            } else {
                SoapObject result = (SoapObject) retObj;
                if (result.getPropertyCount() > 0) {
                    Object obj = result.getProperty(0);
                    if (obj != null && obj.getClass().equals(SoapPrimitive.class)) {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        String resultVariable = j.toString();
                        return resultVariable;
                    } else if (obj != null && obj instanceof String) {
                        String resultVariable = (String) obj;
                        return resultVariable;
                    }
                }
            }
        } catch (Exception e) {
            if (eventHandler != null)
                eventHandler.Wsdl2CodeFinishedWithException(e);
            e.printStackTrace();
        }
        return "";
    }

    public void ePay5EncryptionNewAsync(String sessionID, String text) throws Exception {
        if (this.eventHandler == null)
            throw new Exception("Async Methods Requires IWsdl2CodeEvents");
        ePay5EncryptionNewAsync(sessionID, text, null);
    }

    public void ePay5EncryptionNewAsync(final String sessionID, final String text, final List<HeaderProperty> headers) throws Exception {

        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                eventHandler.Wsdl2CodeStartedRequest();
            }

            ;

            @Override
            protected String doInBackground(Void... params) {
                return ePay5EncryptionNew(sessionID, text, headers);
            }

            @Override
            protected void onPostExecute(String result) {
                eventHandler.Wsdl2CodeEndedRequest();
                if (result != null) {
                    eventHandler.Wsdl2CodeFinished("ePay5EncryptionNew", result);
                }
            }
        }.execute();
    }

    public String ePay5EncryptionNew(String sessionID, String text) {
        return ePay5EncryptionNew(sessionID, text, null);
    }

    public String ePay5EncryptionNew(String sessionID, String text, List<HeaderProperty> headers) {
        SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        soapEnvelope.implicitTypes = true;
        soapEnvelope.dotNet = true;
        SoapObject soapReq = new SoapObject("http://dfm.ae/", "ePay5EncryptionNew");
        soapReq.addProperty("SessionID", sessionID);
        soapReq.addProperty("text", text);
        soapEnvelope.setOutputSoapObject(soapReq);
        HttpTransportSE httpTransport = new HttpTransportSE(url, timeOut);
        try {
            if (headers != null) {
                httpTransport.call("http://dfm.ae/ePay5EncryptionNew", soapEnvelope, headers);
            } else {
                httpTransport.call("http://dfm.ae/ePay5EncryptionNew", soapEnvelope);
            }
            Object retObj = soapEnvelope.bodyIn;
            if (retObj instanceof SoapFault) {
                SoapFault fault = (SoapFault) retObj;
                Exception ex = new Exception(fault.faultstring);
                if (eventHandler != null)
                    eventHandler.Wsdl2CodeFinishedWithException(ex);
            } else {
                SoapObject result = (SoapObject) retObj;
                if (result.getPropertyCount() > 0) {
                    Object obj = result.getProperty(0);
                    if (obj != null && obj.getClass().equals(SoapPrimitive.class)) {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        String resultVariable = j.toString();
                        return resultVariable;
                    } else if (obj != null && obj instanceof String) {
                        String resultVariable = (String) obj;
                        return resultVariable;
                    }
                }
            }
        } catch (Exception e) {
            if (eventHandler != null)
                eventHandler.Wsdl2CodeFinishedWithException(e);
            e.printStackTrace();
        }
        return "";
    }

    public void ePay5DecryptionAsync(String sessionID, String text, String vector) throws Exception {
        if (this.eventHandler == null)
            throw new Exception("Async Methods Requires IWsdl2CodeEvents");
        ePay5DecryptionAsync(sessionID, text, vector, null);
    }

    public void ePay5DecryptionAsync(final String sessionID, final String text, final String vector, final List<HeaderProperty> headers) throws Exception {

        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                eventHandler.Wsdl2CodeStartedRequest();
            }

            ;

            @Override
            protected String doInBackground(Void... params) {
                return ePay5Decryption(sessionID, text, vector, headers);
            }

            @Override
            protected void onPostExecute(String result) {
                eventHandler.Wsdl2CodeEndedRequest();
                if (result != null) {
                    eventHandler.Wsdl2CodeFinished("ePay5Decryption", result);
                }
            }
        }.execute();
    }

    public String ePay5Decryption(String sessionID, String text, String vector) {
        return ePay5Decryption(sessionID, text, vector, null);
    }

    public String ePay5Decryption(String sessionID, String text, String vector, List<HeaderProperty> headers) {
        SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        soapEnvelope.implicitTypes = true;
        soapEnvelope.dotNet = true;
        SoapObject soapReq = new SoapObject("http://dfm.ae/", "ePay5Decryption");
        soapReq.addProperty("SessionID", sessionID);
        soapReq.addProperty("text", text);
        soapReq.addProperty("Vector", vector);
        soapEnvelope.setOutputSoapObject(soapReq);
        HttpTransportSE httpTransport = new HttpTransportSE(url, timeOut);
        try {
            if (headers != null) {
                httpTransport.call("http://dfm.ae/ePay5Decryption", soapEnvelope, headers);
            } else {
                httpTransport.call("http://dfm.ae/ePay5Decryption", soapEnvelope);
            }
            Object retObj = soapEnvelope.bodyIn;
            if (retObj instanceof SoapFault) {
                SoapFault fault = (SoapFault) retObj;
                Exception ex = new Exception(fault.faultstring);
                if (eventHandler != null)
                    eventHandler.Wsdl2CodeFinishedWithException(ex);
            } else {
                SoapObject result = (SoapObject) retObj;
                if (result.getPropertyCount() > 0) {
                    Object obj = result.getProperty(0);
                    if (obj != null && obj.getClass().equals(SoapPrimitive.class)) {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        String resultVariable = j.toString();
                        return resultVariable;
                    } else if (obj != null && obj instanceof String) {
                        String resultVariable = (String) obj;
                        return resultVariable;
                    }
                }
            }
        } catch (Exception e) {
            if (eventHandler != null)
                eventHandler.Wsdl2CodeFinishedWithException(e);
            e.printStackTrace();
        }
        return "";
    }

}
